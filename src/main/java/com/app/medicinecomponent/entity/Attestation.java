package com.app.medicinecomponent.entity;

import com.app.medicinecomponent.entity.template.AbcEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update attestation set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Attestation extends AbcEntity {

    @Column(nullable = false)
    private Date startDate;

    @Column(nullable = false)
    private Date endDate;

    @Column(nullable = false,columnDefinition = "text")
    private String description;

    @Column(nullable = false)
    private Boolean closed=false;

}
