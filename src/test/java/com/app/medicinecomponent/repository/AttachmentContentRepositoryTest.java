package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Attachment;
import com.app.medicinecomponent.entity.AttachmentContent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.io.UnsupportedEncodingException;

@ContextConfiguration(classes = {AttachmentContentRepository.class, AttachmentRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class AttachmentContentRepositoryTest {
    @Autowired
    private AttachmentContentRepository attachmentContentRepository;

    @Autowired
    private AttachmentRepository attachmentRepository;

    @AfterEach
    void tearDown() {
        attachmentRepository.deleteAll();
        attachmentContentRepository.deleteAll();
    }

    @Test
    void testFindByAttachmentId() throws UnsupportedEncodingException {
        Attachment attachment = new Attachment();
        attachment.setContentType("text/plain");
        attachment.setDeleted(false);
        attachment.setName("Name");
        attachment.setSize(3L);
        this.attachmentRepository.save(attachment);

        AttachmentContent attachmentContent = new AttachmentContent();
        attachmentContent.setAttachment(attachment);
        attachmentContent.setBytes("AAAAAAAA".getBytes("UTF-8"));
        attachmentContent.setDeleted(false);
        this.attachmentContentRepository.save(attachmentContent);

        Assertions.assertEquals(this.attachmentContentRepository.findByAttachmentId(attachment.getId()).get(), attachmentContent);
    }
}

