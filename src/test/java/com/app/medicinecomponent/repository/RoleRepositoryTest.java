package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Role;
import com.app.medicinecomponent.entity.enums.PermissionEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ContextConfiguration(classes = {RoleRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class RoleRepositoryTest {

    @Autowired
    RoleRepository roleRepository;

    @AfterEach
    void tearDown() {
        roleRepository.deleteAll();
    }

    @Test
    void findByName() {
        // given
        Role admin = new Role("ADMIN", new HashSet<>(Arrays.asList(PermissionEnum.values())));
        roleRepository.save(admin);
        Role doctor = new Role("DOCTOR", new HashSet<>(Arrays.asList(PermissionEnum.REQUEST_REGISTRATION, PermissionEnum.UPLOAD_ATTACHMENT)));
        roleRepository.save(doctor);

        //when
        Role admin1 = roleRepository.findByName("ADMIN").get();
        Role doctor1 = roleRepository.findByName("DOCTOR").get();


        //then
        assertNotNull(admin1);
        assertNotNull(doctor1);
        assertEquals(admin1.getName(), "ADMIN");
        assertEquals(doctor1.getName(), "DOCTOR");
    }
}