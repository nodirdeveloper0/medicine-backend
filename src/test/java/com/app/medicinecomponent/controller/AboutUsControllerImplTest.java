package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.AboutUsReqDTO;
import com.app.medicinecomponent.payload.AboutUsResDTO;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.service.AboutUsServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AboutUsControllerImpl.class})
class AboutUsControllerImplTest {

    @Autowired
    private AboutUsControllerImpl aboutUsController;

    @MockBean
    private AboutUsServiceImpl aboutUsService;

    AboutUsResDTO aboutUsResDTO = new AboutUsResDTO();
    AboutUsReqDTO aboutUsReqDTO = new AboutUsReqDTO();

    @BeforeEach
    void setUp() {
        aboutUsResDTO.setId(UUID.randomUUID());
        aboutUsResDTO.setDescription("This is a description");

        aboutUsReqDTO.setDescription("This is a description");
    }

    @Test
    void add() throws Exception {
        when(this.aboutUsService.add(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, aboutUsResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(aboutUsReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/about-us")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.aboutUsController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));

    }

    @Test
    void getOne() throws Exception {
        when(this.aboutUsService.getOne(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, aboutUsResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/about-us/{id}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.aboutUsController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAll() throws Exception {
        AboutUsResDTO aboutUsResDTO = new AboutUsResDTO(UUID.randomUUID(), "This is a description");
        when(this.aboutUsService.getOne(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, aboutUsResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/about-us");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.aboutUsController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void edit() throws Exception {
        when(this.aboutUsService.edit(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, aboutUsResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(aboutUsReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.put("/api/about-us/{id}", UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions =
                MockMvcBuilders.standaloneSetup(this.aboutUsController)
                        .build()
                        .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void delete() throws Exception {
        when(this.aboutUsService.delete(ArgumentMatchers.any())).thenReturn(new ApiResult<>("About us is successfully deleted", null, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/about-us/{id}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.aboutUsController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

}