package com.app.medicinecomponent.payload.clinicResponse;

import com.app.medicinecomponent.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private String name;
    private Long code;
    private Address address;
    private Boolean active;
}
