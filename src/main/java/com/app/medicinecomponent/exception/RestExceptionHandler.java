package com.app.medicinecomponent.exception;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.ErrorData;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

public class RestExceptionHandler {

    @ExceptionHandler(value = RestException.class)
    public ResponseEntity<?> exceptionHandling(RestException restException){
//        restException.printStackTrace();
        return ResponseEntity.status(restException.getStatus()).body(ApiResult.errorResponse(restException.getMessage()));
    }

    @ExceptionHandler(value = TokenExpiredException.class)
    public ResponseEntity<?> exceptionHandling(){
        return ResponseEntity.status(498).body(ApiResult.errorResponse("Token expired"));
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> exceptionHandling(Exception e){
//        e.printStackTrace();
        return ResponseEntity.status(500).body(ApiResult.errorResponse("Internal server error"));
    }


    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<?> exceptionHandling(MethodArgumentNotValidException e){
        List<ErrorData> errorData = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(objectError -> errorData.add(
                        new ErrorData(objectError.getDefaultMessage(), 400)
                )
        );
        return ResponseEntity.status(400).body(ApiResult.errorResponse(errorData));
    }
}
