package com.app.medicinecomponent.payload;

import com.app.medicinecomponent.entity.enums.CertificateTypeEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CertificateResDTO {
    private UUID id;
    private Integer serialNumber;
    private UserResDTO toUser;
    private Date fromDate;
    private Date toDate;
    private QualificationCategory qualificationCategory;
    private JobTitleResDTO jobTitle;
    private AttestationResDTO attestation;
    private CertificateTypeEnum certificateTypeEnum;
    private String description;
}
