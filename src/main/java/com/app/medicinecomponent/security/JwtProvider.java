package com.app.medicinecomponent.security;


import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.exception.TokenExpiredException;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {

    @Value("${token.SecretKey}")
    private String tokenSecretKey;

    @Value("${token.ExpireTime}")
    private long tokenExpireTime;

    public String generateJwtTokenFromId(Long cardNumber) {
        Date expireDate = new Date(System.currentTimeMillis() + tokenExpireTime);
        return "Bearer " + Jwts
                .builder()
                .setSubject(cardNumber.toString())
                .setIssuedAt(new Date())
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, tokenSecretKey)
                .compact();
    }

    public String getIdFromToken(String token) {
        try {
            return Jwts
                    .parser()
                    .setSigningKey(tokenSecretKey)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
        } catch (ExpiredJwtException e) {
            throw new TokenExpiredException();
        } catch (Exception ex) {
            throw new RestException(HttpStatus.UNAUTHORIZED, "Unauthorized");
        }
    }


    public void validateToken(String token) {
//        try {
            Jwts
                    .parser()
                    .setSigningKey(tokenSecretKey)
                    .parseClaimsJws(token);
//            return true;
//          }
//        catch (ExpiredJwtException e) {
//            System.err.println("Expired Jwt token");
//        } catch (MalformedJwtException e) {
//            System.err.println("Malformed Jwt token");
//        } catch (SignatureException e) {
//            System.err.println("Invalid Jwt token");
//        } catch (UnsupportedJwtException e) {
//            System.err.println("Unsupported Jwt token");
//        } catch (IllegalArgumentException e) {
//            System.err.println("Empty Jwt token");
//        }
//        return false;
    }


}
