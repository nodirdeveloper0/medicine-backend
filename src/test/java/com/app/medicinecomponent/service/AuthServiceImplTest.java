package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.SignInReqDTO;
import com.app.medicinecomponent.payload.SignUpReqDTO;
import com.app.medicinecomponent.payload.accountResponse.*;
import com.app.medicinecomponent.payload.residentResponse.ResidentInfoFromCityManagement;
import com.app.medicinecomponent.payload.residentResponse.Result;
import com.app.medicinecomponent.security.JwtProvider;
import com.app.medicinecomponent.security.hmac.HMACUtilServiceImpl;
import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.enums.CertificateTypeEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.payload.accountResponse.CertificateResponseFromAccountService;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeResponseFromAccountService;
import com.app.medicinecomponent.payload.accountResponse.Embedded;
import com.app.medicinecomponent.payload.accountResponse.Links;
import com.app.medicinecomponent.repository.*;
import com.app.medicinecomponent.repository.AttachmentRepository;
import com.app.medicinecomponent.repository.CertificateRepository;
import com.app.medicinecomponent.repository.ClinicRepository;
import com.app.medicinecomponent.repository.JobTitleRepository;
import com.app.medicinecomponent.repository.RoleRepository;
import com.app.medicinecomponent.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.intercept.RunAsImplAuthenticationProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.jaas.DefaultJaasAuthenticationProvider;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;

@ContextConfiguration(classes = {AuthServiceImpl.class})
@ExtendWith(SpringExtension.class)
class AuthServiceImplTest {
    @MockBean
    private AttachmentRepository attachmentRepository;

    @Autowired
    private AuthServiceImpl authServiceImpl;

    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private CertificateRepository certificateRepository;

    @MockBean
    private ClinicRepository clinicRepository;

    @MockBean
    private ExternalAPIsService externalAPIsService;

    @MockBean
    private JobTitleRepository jobTitleRepository;

    @MockBean
    private JwtProvider jwtProvider;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserRepository userRepository;

    User user = new User();
    Role role = new Role();
    JobTitle jobTitle = new JobTitle();
    Clinic clinic = new Clinic();
    Address address = new Address();
    Attachment attachment = new Attachment();
    Attestation attestation = new Attestation();
    Certificate certificate = new Certificate();


    @BeforeEach
    void setUp() {
        attachment.setContentType("text/plain");
        attachment.setCreatedAt(mock(Timestamp.class));
        attachment.setCreatedBy(UUID.randomUUID());
        attachment.setDeleted(true);
        attachment.setId(UUID.randomUUID());
        attachment.setName("Name");
        attachment.setSize(3L);
        attachment.setUpdatedAt(mock(Timestamp.class));
        attachment.setUpdatedBy(UUID.randomUUID());

        address.setCreatedAt(mock(Timestamp.class));
        address.setCreatedBy(UUID.randomUUID());
        address.setDeleted(true);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setId(UUID.randomUUID());
        address.setStreet("Street");
        address.setUpdatedAt(mock(Timestamp.class));
        address.setUpdatedBy(UUID.randomUUID());

        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setCreatedAt(mock(Timestamp.class));
        clinic.setCreatedBy(UUID.randomUUID());
        clinic.setDeleted(true);
        clinic.setId(UUID.randomUUID());
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinic.setUpdatedAt(mock(Timestamp.class));
        clinic.setUpdatedBy(UUID.randomUUID());

        jobTitle.setCreatedAt(mock(Timestamp.class));
        jobTitle.setCreatedBy(UUID.randomUUID());
        jobTitle.setDeleted(true);
        jobTitle.setId(UUID.randomUUID());
        jobTitle.setName("Name");
        jobTitle.setUpdatedAt(mock(Timestamp.class));
        jobTitle.setUpdatedBy(UUID.randomUUID());

        role.setCreatedAt(mock(Timestamp.class));
        role.setCreatedBy(UUID.randomUUID());
        role.setDeleted(true);
        role.setId(UUID.randomUUID());
        role.setName("Name");
        role.setPermissionEnums(new HashSet<>());
        role.setUpdatedAt(mock(Timestamp.class));
        role.setUpdatedBy(UUID.randomUUID());

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setActive(true);
        user.setAvatarImg(attachment);
        user.setClinic(clinic);
        user.setCreatedAt(mock(Timestamp.class));
        user.setCreatedBy(UUID.randomUUID());
        user.setCredentialsNonExpired(true);
        user.setDeleted(true);
        user.setEnabled(true);
        user.setFirstname("Jane");
        user.setId(UUID.randomUUID());
        user.setJobTitle(jobTitle);
        user.setLastname("Doe");
        user.setPassword("iloveyou");
        user.setQualificationCategory(QualificationCategory.SPECIALIST);
        user.setResidentCardNumber(1L);
        user.setRole(role);
        user.setUpdatedAt(mock(Timestamp.class));
        user.setUpdatedBy(UUID.randomUUID());
        user.setWorkingExperienceInMonth(1);

        attestation.setClosed(true);
        attestation.setCreatedAt(mock(Timestamp.class));
        attestation.setCreatedBy(UUID.randomUUID());
        attestation.setDeleted(true);
        attestation.setDescription("The characteristics of someone or something");
        attestation.setEndDate(mock(Date.class));
        attestation.setId(UUID.randomUUID());
        attestation.setStartDate(mock(Date.class));
        attestation.setUpdatedAt(mock(Timestamp.class));
        attestation.setUpdatedBy(UUID.randomUUID());

        certificate.setAttestation(attestation);
        certificate.setCertificateTypeEnum(CertificateTypeEnum.CERTIFICATE_OF_REGISTRATION);
        certificate.setCreatedAt(mock(Timestamp.class));
        certificate.setCreatedBy(UUID.randomUUID());
        certificate.setDeleted(true);
        certificate.setDescription("The characteristics of someone or something");
        certificate.setFromDate(mock(Date.class));
        certificate.setId(UUID.randomUUID());
        certificate.setJobTitle(jobTitle);
        certificate.setQualificationCategory(QualificationCategory.SPECIALIST);
        certificate.setSerialNumber(10);
        certificate.setToDate(mock(Date.class));
        certificate.setToUser(user);
        certificate.setUpdatedAt(mock(Timestamp.class));
        certificate.setUpdatedBy(UUID.randomUUID());
    }

    @Test
    void testLoadUserByUsername2() throws UsernameNotFoundException {
        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        assertSame(user, this.authServiceImpl.loadUserByUsername("42"));
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
    }

    @Test
    void testLoadUserByUsername01() {
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.authServiceImpl.loadUserByUsername("123"));
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
    }

    @Test
    void testLoadById() {
        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findById((UUID) any())).thenReturn(ofResult);
        assertSame(user, this.authServiceImpl.loadById(UUID.randomUUID()));
        verify(this.userRepository).findById((UUID) any());
    }

    @Test
    void testLoadById01() {
        when(this.userRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.authServiceImpl.loadById(UUID.randomUUID()));
        verify(this.userRepository).findById((UUID) any());
    }

    @Test
    void testSignUp() {
        when(this.userRepository.existsByResidentCardNumber((Long) any())).thenReturn(true);
        assertThrows(RestException.class, () -> this.authServiceImpl.signUp(new SignUpReqDTO()));
        verify(this.userRepository).existsByResidentCardNumber((Long) any());
    }

    @Test
    void testSignUp4() {
        when(this.userRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);

        ResidentInfoFromCityManagement residentInfoFromCityManagement = new ResidentInfoFromCityManagement();
        residentInfoFromCityManagement.setSuccess(false);
        when(this.externalAPIsService.getResidentInfoFromCityManagement((Long) any()))
                .thenReturn(residentInfoFromCityManagement);
        assertThrows(RestException.class, () -> this.authServiceImpl.signUp(new SignUpReqDTO()));
        verify(this.userRepository).existsByResidentCardNumber((Long) any());
        verify(this.externalAPIsService).getResidentInfoFromCityManagement((Long) any());
    }

    @Test
    void testSignUp7() {
        when(this.userRepository.save((User) any())).thenReturn(user);
        when(this.userRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);

        Optional<Role> ofResult = Optional.of(role);
        when(this.roleRepository.findByName((String) any())).thenReturn(ofResult);
        when(this.passwordEncoder.encode((CharSequence) any())).thenReturn("secret");

        Optional<JobTitle> ofResult1 = Optional.of(jobTitle);
        when(this.jobTitleRepository.findById((UUID) any())).thenReturn(ofResult1);

        Result result = new Result();
        result.setActive(true);

        ResidentInfoFromCityManagement residentInfoFromCityManagement = new ResidentInfoFromCityManagement(result, true);
        residentInfoFromCityManagement.setSuccess(true);
        when(this.externalAPIsService
                .saveCertificateType((CertificateTypeReq) any()))
                .thenReturn(new CertificateTypeResponseFromAccountService());
        when(this.externalAPIsService.getResidentInfoFromCityManagement((Long) any()))
                .thenReturn(residentInfoFromCityManagement);

        Optional<Clinic> ofResult2 = Optional.of(clinic);
        when(this.clinicRepository.findById((UUID) any())).thenReturn(ofResult2);

        when(this.certificateRepository.countAll()).thenReturn(3);
        when(this.certificateRepository.save((Certificate) any())).thenReturn(certificate);
        assertThrows(RestException.class, () -> this.authServiceImpl.signUp(new SignUpReqDTO()));
        verify(this.userRepository).existsByResidentCardNumber((Long) any());
        verify(this.userRepository).save((User) any());
        verify(this.roleRepository, atLeast(1)).findByName((String) any());
        verify(this.passwordEncoder).encode((CharSequence) any());
        verify(this.jobTitleRepository).findById((UUID) any());
        verify(this.externalAPIsService)
                .saveCertificateType((CertificateTypeReq) any());
        verify(this.externalAPIsService).getResidentInfoFromCityManagement((Long) any());
        verify(this.clinicRepository).findById((UUID) any());
        verify(this.certificateRepository).countAll();
        verify(this.certificateRepository).save((Certificate) any());
    }

    @Test
    void testSignIn3() {
        ArrayList<AuthenticationProvider> authenticationProviderList = new ArrayList<>();
        authenticationProviderList.add(new RunAsImplAuthenticationProvider());
        ProviderManager authenticationManager = new ProviderManager(authenticationProviderList);
        UserRepository userRepository = mock(UserRepository.class);
        ExternalAPIsServiceImpl externalAPIs = new ExternalAPIsServiceImpl(new HMACUtilServiceImpl());
        Argon2PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        RoleRepository roleRepository = mock(RoleRepository.class);
        ClinicRepository clinicRepository = mock(ClinicRepository.class);
        JobTitleRepository jobTitleRepository = mock(JobTitleRepository.class);
        AuthServiceImpl authServiceImpl = new AuthServiceImpl(userRepository, externalAPIs, passwordEncoder, roleRepository,
                clinicRepository, jobTitleRepository, authenticationManager, new JwtProvider(),
                mock(CertificateRepository.class), mock(AttachmentRepository.class));
        assertThrows(RestException.class, () -> authServiceImpl.signIn(new SignInReqDTO("42", "iloveyou")));
    }

    @Test
    void testSignIn4() {
        UserRepository userRepository = mock(UserRepository.class);
        ExternalAPIsServiceImpl externalAPIs = new ExternalAPIsServiceImpl(new HMACUtilServiceImpl());
        Argon2PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        RoleRepository roleRepository = mock(RoleRepository.class);
        ClinicRepository clinicRepository = mock(ClinicRepository.class);
        JobTitleRepository jobTitleRepository = mock(JobTitleRepository.class);
        AuthServiceImpl authServiceImpl = new AuthServiceImpl(userRepository, externalAPIs, passwordEncoder, roleRepository,
                clinicRepository, jobTitleRepository, null, new JwtProvider(), mock(CertificateRepository.class),
                mock(AttachmentRepository.class));
        assertThrows(RestException.class, () -> authServiceImpl.signIn(new SignInReqDTO("42", "iloveyou")));
    }

    @Test
    void testSignIn5() {
        ArrayList<AuthenticationProvider> authenticationProviderList = new ArrayList<>();
        authenticationProviderList.add(new DefaultJaasAuthenticationProvider());
        ProviderManager authenticationManager = new ProviderManager(authenticationProviderList);
        UserRepository userRepository = mock(UserRepository.class);
        ExternalAPIsServiceImpl externalAPIs = new ExternalAPIsServiceImpl(new HMACUtilServiceImpl());
        Argon2PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        RoleRepository roleRepository = mock(RoleRepository.class);
        ClinicRepository clinicRepository = mock(ClinicRepository.class);
        JobTitleRepository jobTitleRepository = mock(JobTitleRepository.class);
        AuthServiceImpl authServiceImpl = new AuthServiceImpl(userRepository, externalAPIs, passwordEncoder, roleRepository,
                clinicRepository, jobTitleRepository, authenticationManager, new JwtProvider(),
                mock(CertificateRepository.class), mock(AttachmentRepository.class));
        assertThrows(RestException.class, () -> authServiceImpl.signIn(new SignInReqDTO("42", "iloveyou")));
    }

    @Test
    void testSignIn6() {
        ArrayList<AuthenticationProvider> authenticationProviderList = new ArrayList<>();
        authenticationProviderList.add(new RunAsImplAuthenticationProvider());
        ProviderManager authenticationManager = new ProviderManager(authenticationProviderList);
        UserRepository userRepository = mock(UserRepository.class);
        ExternalAPIsServiceImpl externalAPIs = new ExternalAPIsServiceImpl(new HMACUtilServiceImpl());
        Argon2PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        RoleRepository roleRepository = mock(RoleRepository.class);
        ClinicRepository clinicRepository = mock(ClinicRepository.class);
        JobTitleRepository jobTitleRepository = mock(JobTitleRepository.class);
        assertThrows(RestException.class,
                () -> (new AuthServiceImpl(userRepository, externalAPIs, passwordEncoder, roleRepository, clinicRepository,
                        jobTitleRepository, authenticationManager, new JwtProvider(), mock(CertificateRepository.class),
                        mock(AttachmentRepository.class))).signIn(null));
    }


    @Test
    void testCreateCertificate() {
        CertificateResponseFromAccountService certificateResponseFromAccountService = new CertificateResponseFromAccountService();
        certificateResponseFromAccountService.description = "The characteristics of someone or something";
        certificateResponseFromAccountService.expiredAt = mock(Date.class);
        certificateResponseFromAccountService.cardNumber = 10;
        certificateResponseFromAccountService.firstName = "Jane";
        certificateResponseFromAccountService.lastName = "Doe";
        certificateResponseFromAccountService._embedded = new Embedded();
        certificateResponseFromAccountService._links = new Links();
        when(this.externalAPIsService
                .saveCertificate((CertificateReq) any()))
                .thenReturn(certificateResponseFromAccountService);

        this.authServiceImpl.createCertificate(user, certificate, "42");
        verify(this.externalAPIsService)
                .saveCertificate((CertificateReq) any());
    }
}

//1425->509