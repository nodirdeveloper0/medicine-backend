package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.entity.Attachment;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RequestMapping(RestConstant.ATTACHMENT_CONTROLLER)
@Tag(name = "Attachment(Image) operations",description = "Attachment")
public interface AttachmentController {

    @Operation(summary = "Uploading photo,image (doctor)")
    @PostMapping("/upload")
    ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException;

    @Operation(summary = "Get attachment's info")
    @GetMapping("/info/{id}")
    ApiResult<Attachment> getById(@PathVariable UUID id);

    @Operation(summary = "Downloading attachment (any user)")
    @GetMapping("/download/{id}")
    ApiResult<?> getFile(@PathVariable UUID id, HttpServletResponse response) throws IOException;

}