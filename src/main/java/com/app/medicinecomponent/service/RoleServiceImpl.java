package com.app.medicinecomponent.service;

import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.entity.Role;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RoleResDTO;
import com.app.medicinecomponent.repository.RoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public ApiResult<RoleResDTO> getOne(UUID id) {
        Role role = roleRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Role is not found!"));
        return ApiResult.successResponse(CustomMapper.roleToDTO(role));
    }

    @Override
    public ApiResult<List<RoleResDTO>> getAll() {
        List<Role> roleList = roleRepository.findAll();
        return ApiResult.successResponse(CustomMapper.roleListToDTO(roleList));
    }
}
