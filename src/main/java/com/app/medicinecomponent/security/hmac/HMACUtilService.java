package com.app.medicinecomponent.security.hmac;

public interface HMACUtilService {

    String calculateHASH(String keyId,String timestamp,String action,String secretKey);

    boolean hasAccess(String keyId, String timestamp,String action,String signature);
}
