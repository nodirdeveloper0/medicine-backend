package com.app.medicinecomponent.payload.accountResponse;

import java.sql.Date;

public class CertificateResponseFromAccountService {
    public String description;
    public Date expiredAt;
    public int cardNumber;
    public String firstName;
    public String lastName;
    public Embedded _embedded;
    public Links _links;


}
