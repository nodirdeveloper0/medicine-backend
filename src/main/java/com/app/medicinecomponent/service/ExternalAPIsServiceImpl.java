package com.app.medicinecomponent.service;

import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.security.hmac.HMACUtilServiceImpl;
import com.app.medicinecomponent.payload.accountResponse.CertificateReq;
import com.app.medicinecomponent.payload.accountResponse.CertificateResponseFromAccountService;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeReq;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeResponseFromAccountService;
import com.app.medicinecomponent.payload.clinicResponse.ClinicInfoFromCityManagement;
import com.app.medicinecomponent.payload.residentResponse.ResidentInfoFromCityManagement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Date;

@Service
public class ExternalAPIsServiceImpl implements ExternalAPIsService {
    private final HMACUtilServiceImpl hmacUtilServiceImpl;

    public ExternalAPIsServiceImpl(HMACUtilServiceImpl hmacUtilServiceImpl) {
        this.hmacUtilServiceImpl = hmacUtilServiceImpl;
    }

    @Value("${cityManagementBasePath}")
    private String cityManagementBasePath;

    @Value("${hmacSignatureSecretKey}")
    private String hmacSignatureSecretKey = "medicineKey";

    @Value("${hmacSignatureKeyId}")
    private String hmacSignatureKeyId = "MEDICINE";

    @Value("${hmacExpireTime}")
    private Long hmacExpireTime = 6000000L;

    @Value("${cityManagementResidentInfoPath}")
    private String cityManagementResidentInfoPath;

    @Value("${cityManagementClinicInfoPath}")
    private String cityManagementClinicInfoPath;

    @Value("${accountServiceBasePath}")
    private String accountServiceBasePath;

    @Value("${accountServiceCertificateTypePath}")
    private String accountServiceCertificateTypePath;

    @Value("${accountServiceCertificatePath}")
    private String accountServiceCertificatePath;

    @Override
    public ResidentInfoFromCityManagement getResidentInfoFromCityManagement(Long residentCardNumber) {
        String action = "get_resident_info";
        String path = cityManagementBasePath + cityManagementResidentInfoPath + residentCardNumber;
        HttpHeaders headers = httpHeadersMakerMethod(action);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<?> request = new HttpEntity<>(headers);
        RestTemplate restTemplate = new RestTemplate();
        ResidentInfoFromCityManagement response;

        try {
            response = restTemplate.exchange(
                    URI.create(path),
                    HttpMethod.GET,
                    request, ResidentInfoFromCityManagement.class).getBody();
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, " Resident info not found from City Management");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "External api error!");
        }
        return response;
    }

    @Override
    public ClinicInfoFromCityManagement getClinicInfoFromCityManagement(Long cardNumber) {
        String action = "get_clinic_info";
        String path = cityManagementBasePath + "/api/v1/legalEntity/" + cardNumber;
        HttpHeaders headers = httpHeadersMakerMethod(action);
        HttpEntity<?> request = new HttpEntity<>(headers);
        RestTemplate restTemplate = new RestTemplate();
        ClinicInfoFromCityManagement response;

        try {
            response = restTemplate.exchange(
                    URI.create(path),
                    HttpMethod.GET,
                    request, ClinicInfoFromCityManagement.class).getBody();
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Clinic info not found from City Management");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "External api error");
        }
        return response;

    }

    @Override
    public CertificateTypeResponseFromAccountService saveCertificateType(CertificateTypeReq certificateTypeReq) {
        String action = "save_certificate_type";
        String path = accountServiceBasePath + accountServiceCertificateTypePath;
        HttpHeaders headers = httpHeadersMakerMethod(action);
        HttpEntity<CertificateTypeReq> request = new HttpEntity<>(certificateTypeReq, headers);
        RestTemplate restTemplate = new RestTemplate();
        CertificateTypeResponseFromAccountService response;
        try {
            response = restTemplate.exchange(
                    URI.create(path),
                    HttpMethod.POST,
                    request,
                    CertificateTypeResponseFromAccountService.class).getBody();

        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "CertificateType info not found from Account service");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.SERVICE_UNAVAILABLE, "External Api error");
        }
        return response;
    }

    @Override
    public CertificateResponseFromAccountService saveCertificate(CertificateReq certificateReq) {
        String action = "save_certificate";
        String path = accountServiceBasePath + accountServiceCertificatePath;
        HttpHeaders headers = httpHeadersMakerMethod(action);
        HttpEntity<CertificateReq> request = new HttpEntity<>(certificateReq, headers);
        RestTemplate restTemplate = new RestTemplate();
        CertificateResponseFromAccountService response;
        try {
            response = restTemplate.exchange(
                    URI.create(path),
                    HttpMethod.POST,
                    request,
                    CertificateResponseFromAccountService.class).getBody();
        } catch (HttpClientErrorException e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.NOT_FOUND, "Certicate info not found from Account service");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.SERVICE_UNAVAILABLE, "External Api error");
        }
        return response;
    }


//  @Override
//    public ClinicInfoFromCityManagement getClinicInfoFromCityManagement(Long cardNumber) {
//        final String action = "get_clinic_info";
////        final String path = cityManagementBasePath + "/api/v1/resident/card/" + cardNumber;
//        final String path = "http://citymanagementbackend-env-1.eba-3swwhqnr.us-east-2.elasticbeanstalk.com" + "/api/v1/resident/card/" + cardNumber;
//        HttpHeaders headers = httpHeadersMakerMethod(action);
//        HttpEntity<?> request = new HttpEntity<>(headers);
//        RestTemplate restTemplate = new RestTemplate();
//        ClinicInfoFromCityManagement response;
//
//        try {
//            response = restTemplate.exchange(
//                    URI.create(path),
//                    HttpMethod.GET,
//                    request, ClinicInfoFromCityManagement.class).getBody();
//        } catch (HttpClientErrorException e) {
//            e.printStackTrace();
//            throw new RestException(HttpStatus.NOT_FOUND, "Clinic info not found from City Management");
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RestException(HttpStatus.NOT_FOUND, "External api error");
//        }
//        return response;
//
//    }

//    public  ResidentCertificateFromCitizenAccount checkResidentCertificateFromCitizenAccount(Long cardNumber){
//        String action = "get_certificate_info";
//        String path = "";
//        HttpHeaders headers = httpHeadersMakerMethod(action);
//        HttpEntity<?> request = new HttpEntity<>(headers);
//        RestTemplate restTemplate = new RestTemplate();
//        ResidentInfoFromCityManagement response;
//
//        try {
//            response = restTemplate.exchange(
//                    URI.create(path),
//                    HttpMethod.GET,
//                    request,
//                    ResidentCertificateFromCitizenAccount.class).getBody();
//
//        }catch (HttpClientErrorException e) {
//            e.printStackTrace();
//            throw new RestException(HttpStatus.NOT_FOUND, "Certificate info not found from City Management");
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RestException(HttpStatus.NOT_FOUND, "External api error");
//        }
//        return response;about_us
//    }

    private HttpHeaders httpHeadersMakerMethod(String action) {
        String timestamp = String.valueOf(new Date().getTime() + hmacExpireTime);
        String signature = hmacUtilServiceImpl.calculateHASH(hmacSignatureKeyId, timestamp, action, hmacSignatureSecretKey);
        HttpHeaders headers = new HttpHeaders();
        headers.set("sm-keyId", hmacSignatureKeyId);
        headers.set("sm-timestamp", timestamp);
        headers.set("sm-action", action);
        headers.set("sm-signature", signature);
        return headers;
    }

//    public static void main(String[] args) {
//        String now = new Date().getTime() + ""+86400;
//        System.out.println(now);
////        String s = hmac.calculateHASH2("MEDICINE", String.valueOf(now), "get_clinic_info", "medicineKey");
//    }
}
