package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Attachment;
import com.app.medicinecomponent.entity.AttachmentContent;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.repository.AttachmentContentRepository;
import com.app.medicinecomponent.repository.AttachmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.Optional;
import java.util.Scanner;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {AttachmentServiceImpl.class})
@ExtendWith(SpringExtension.class)
class AttachmentServiceImplTest {
    @MockBean
    private AttachmentContentRepository attachmentContentRepository;

    @MockBean
    private AttachmentRepository attachmentRepository;

    @Autowired
    private AttachmentServiceImpl attachmentServiceImpl;

    Attachment attachment = new Attachment();
    AttachmentContent attachmentContent = new AttachmentContent();

    @BeforeEach
    void setUp() throws UnsupportedEncodingException {
        attachment.setContentType("text/plain");
        attachment.setCreatedAt(mock(Timestamp.class));
        attachment.setCreatedBy(UUID.randomUUID());
        attachment.setDeleted(true);
        attachment.setId(UUID.randomUUID());
        attachment.setName("Name");
        attachment.setSize(3L);
        attachment.setUpdatedAt(mock(Timestamp.class));
        attachment.setUpdatedBy(UUID.randomUUID());


        attachmentContent.setAttachment(attachment);
        attachmentContent.setBytes("AAAAAAAA".getBytes("UTF-8"));
        attachmentContent.setCreatedAt(mock(Timestamp.class));
        attachmentContent.setCreatedBy(UUID.randomUUID());
        attachmentContent.setDeleted(true);
        attachmentContent.setId(UUID.randomUUID());
        attachmentContent.setUpdatedAt(mock(Timestamp.class));
        attachmentContent.setUpdatedBy(UUID.randomUUID());
    }

    @Test
    void testUpload3() throws IOException {
        when(this.attachmentRepository.save((Attachment) any())).thenReturn(attachment);
        when(this.attachmentContentRepository.save((AttachmentContent) any())).thenReturn(attachmentContent);
        DefaultMultipartHttpServletRequest defaultMultipartHttpServletRequest = mock(
                DefaultMultipartHttpServletRequest.class);
        when(defaultMultipartHttpServletRequest.getFile((String) any()))
                .thenReturn(new MockMultipartFile("Name", new ByteArrayInputStream("AAAAAAAA".getBytes("UTF-8"))));
        when(defaultMultipartHttpServletRequest.getFileNames()).thenReturn(new Scanner("foo"));
        ApiResult<?> actualUploadResult = this.attachmentServiceImpl.upload(defaultMultipartHttpServletRequest);
        assertNull(actualUploadResult.getData());
        assertTrue(actualUploadResult.isSuccess());
        assertNull(actualUploadResult.getMessage());
        assertNull(actualUploadResult.getErrors());
        verify(this.attachmentRepository).save((Attachment) any());
        verify(this.attachmentContentRepository).save((AttachmentContent) any());
        verify(defaultMultipartHttpServletRequest).getFileNames();
        verify(defaultMultipartHttpServletRequest).getFile((String) any());
    }

    @Test
    void testUpload4() throws IOException {
        when(this.attachmentRepository.save((Attachment) any())).thenReturn(attachment);
        when(this.attachmentContentRepository.save((AttachmentContent) any()))
                .thenThrow(new RestException(HttpStatus.CONTINUE, "An error occurred"));
        DefaultMultipartHttpServletRequest defaultMultipartHttpServletRequest = mock(
                DefaultMultipartHttpServletRequest.class);
        when(defaultMultipartHttpServletRequest.getFile((String) any()))
                .thenReturn(new MockMultipartFile("Name", new ByteArrayInputStream("AAAAAAAA".getBytes("UTF-8"))));
        when(defaultMultipartHttpServletRequest.getFileNames()).thenReturn(new Scanner("foo"));
        assertThrows(RestException.class, () -> this.attachmentServiceImpl.upload(defaultMultipartHttpServletRequest));
        verify(this.attachmentRepository).save((Attachment) any());
        verify(this.attachmentContentRepository).save((AttachmentContent) any());
        verify(defaultMultipartHttpServletRequest).getFileNames();
        verify(defaultMultipartHttpServletRequest).getFile((String) any());
    }

    @Test
    void testUpload5() throws IOException {
        when(this.attachmentContentRepository.save((AttachmentContent) any())).thenReturn(attachmentContent);
        DefaultMultipartHttpServletRequest defaultMultipartHttpServletRequest = mock(
                DefaultMultipartHttpServletRequest.class);
        when(defaultMultipartHttpServletRequest.getFile((String) any())).thenReturn(null);
        when(defaultMultipartHttpServletRequest.getFileNames()).thenReturn(new Scanner("foo"));
        ApiResult<?> actualUploadResult = this.attachmentServiceImpl.upload(defaultMultipartHttpServletRequest);
        assertNull(actualUploadResult.getData());
        assertTrue(actualUploadResult.isSuccess());
        assertEquals(" Cannot uploaded the attachment", actualUploadResult.getMessage());
        assertNull(actualUploadResult.getErrors());
        verify(defaultMultipartHttpServletRequest).getFileNames();
        verify(defaultMultipartHttpServletRequest).getFile((String) any());
    }

    @Test
    void testGetById() {
        Optional<Attachment> ofResult = Optional.of(attachment);
        when(this.attachmentRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<Attachment> actualById = this.attachmentServiceImpl.getById(UUID.randomUUID());
        assertSame(attachment, actualById.getData());
        assertTrue(actualById.isSuccess());
        assertNull(actualById.getErrors());
        assertNull(actualById.getMessage());
        verify(this.attachmentRepository).findById((UUID) any());
    }

    @Test
    void testGetById3() {
        when(this.attachmentRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.attachmentServiceImpl.getById(UUID.randomUUID()));
        verify(this.attachmentRepository).findById((UUID) any());
    }

    @Test
    void testGetFile5() throws IOException {
        Optional<Attachment> ofResult = Optional.of(attachment);
        when(this.attachmentRepository.findById((UUID) any())).thenReturn(ofResult);
        Optional<AttachmentContent> attachmentContentOptional = Optional.of(attachmentContent);
        when(this.attachmentContentRepository.findByAttachmentId((UUID) any())).thenReturn(attachmentContentOptional);
        UUID id = UUID.randomUUID();
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        ApiResult<?> actualFile = this.attachmentServiceImpl.getFile(id, mockHttpServletResponse);
        assertNull(actualFile.getData());
        assertTrue(actualFile.isSuccess());
        assertEquals("Attachment successfully got", actualFile.getMessage());
        assertNull(actualFile.getErrors());
        verify(this.attachmentRepository).findById((UUID) any());
        verify(this.attachmentContentRepository).findByAttachmentId((UUID) any());
        assertNull(mockHttpServletResponse.getRedirectedUrl());
        assertEquals("text/plain", mockHttpServletResponse.getContentType());
    }

    @Test
    void testGetFile01() {
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        when(this.attachmentRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.attachmentServiceImpl.getFile(UUID.randomUUID(), mockHttpServletResponse));
        verify(this.attachmentRepository).findById((UUID) any());
    }

    @Test
    void testGetFile03() throws IOException {
        Optional<Attachment> ofResult = Optional.of(attachment);
        when(this.attachmentRepository.findById((UUID) any())).thenReturn(ofResult);
        Optional<AttachmentContent> attachmentContentOptional = Optional.of(attachmentContent);
        when(this.attachmentContentRepository.findByAttachmentId((UUID) any())).thenReturn(Optional.empty());
        UUID id = UUID.randomUUID();
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        assertThrows(RestException.class, () -> this.attachmentServiceImpl.getFile(id, mockHttpServletResponse));
        verify(this.attachmentRepository).findById((UUID) any());
        verify(this.attachmentContentRepository).findByAttachmentId((UUID) any());
    }
}

