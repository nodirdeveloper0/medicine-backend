package com.app.medicinecomponent.service;

import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.ClinicReqDTO;
import com.app.medicinecomponent.payload.ClinicResDTO;
import com.app.medicinecomponent.payload.clinicResponse.ClinicInfoFromCityManagement;
import com.app.medicinecomponent.entity.Clinic;
import com.app.medicinecomponent.repository.AddressRepository;
import com.app.medicinecomponent.repository.ClinicRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ClinicServiceImpl implements ClinicService {
    private final ClinicRepository clinicRepository;
    private final ExternalAPIsServiceImpl externalAPIs;
    private final AddressRepository addressRepository;

    public ClinicServiceImpl(ClinicRepository clinicRepository, ExternalAPIsServiceImpl externalAPIs, AddressRepository addressRepository) {
        this.clinicRepository = clinicRepository;
        this.externalAPIs = externalAPIs;
        this.addressRepository = addressRepository;
    }

    @Override
    public ApiResult<ClinicResDTO> add(ClinicReqDTO clinicReqDTO) {
        if (clinicRepository.existsByClinicLegalCardNumber(clinicReqDTO.getClinicLegalCardNumber()))
            throw new RestException(HttpStatus.CONFLICT, "Clinic already exist with such card number");

        ClinicInfoFromCityManagement clinicInfo = externalAPIs.getClinicInfoFromCityManagement(clinicReqDTO.getClinicLegalCardNumber());

        if (!clinicInfo.getSuccess())
            throw new RestException(HttpStatus.NOT_FOUND, "Clinic not found in receiving Clinic info from City Management");

        Clinic clinic = new Clinic(
                clinicInfo.getResult().getName(),
                clinicInfo.getResult().getCode(),
                clinicReqDTO.getPhoneNumber(),
                clinicInfo.getResult().getAddress(),
                clinicInfo.getResult().getActive()
        );
        addressRepository.save(clinicInfo.getResult().getAddress());
        clinicRepository.save(clinic);
        return ApiResult.successResponse(CustomMapper.clinicToDTO(clinic));
    }

    @Override
    public ApiResult<ClinicResDTO> getOne(UUID id) {
        Clinic clinic = clinicRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Clinic with such id not found"));

//        ClinicInfoFromCityManagement clinicInfo = externalAPIs.getClinicInfoFromCityManagement(clinic.getClinicLegalCardNumber());
//        if (!clinicInfo.getSuccess())
//            throw new RestException(HttpStatus.BAD_REQUEST, "Clinic not found in smart city with such id");

        return ApiResult.successResponse(CustomMapper.clinicToDTO(clinic));
    }

    @Override
    public ApiResult<List<ClinicResDTO>> getAll() {
        List<Clinic> clinicList = clinicRepository.findAll();
//        for (Clinic clinic : clinicList) {
//            ClinicInfoFromCityManagement info = externalAPIs.getClinicInfoFromCityManagement(clinic.getClinicLegalCardNumber());
//            if (!info.getSuccess()) {
//                clinicList.remove(clinic);
//            }
//        }
        return ApiResult.successResponse(CustomMapper.clinicListToDTO(clinicList));
    }

    @Override
    public ApiResult<ClinicResDTO> getOneByLegalCardNumber(Long cardNumber) {
        Clinic clinic = clinicRepository.findByClinicLegalCardNumber(cardNumber).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Clinic is not found!"));
//        ClinicInfoFromCityManagement clinicInfo = externalAPIs.getClinicInfoFromCityManagement(clinic.getClinicLegalCardNumber());
//        if (!clinicInfo.getSuccess())
//            throw new RestException(HttpStatus.BAD_REQUEST, "Clinic not found in smart city with such id");
        return ApiResult.successResponse(CustomMapper.clinicToDTO(clinic));

    }

}
