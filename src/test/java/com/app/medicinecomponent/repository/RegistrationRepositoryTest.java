package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Attestation;
import com.app.medicinecomponent.entity.Registration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.sql.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@ContextConfiguration(classes = {RegistrationRepository.class, AttestationRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class RegistrationRepositoryTest {
    @Autowired
    private RegistrationRepository registrationRepository;
    @Autowired
    private AttestationRepository attestationRepository;

    Registration registration = new Registration();

    @BeforeEach
    void setUp() {
        Attestation attestation = new Attestation();
        attestation.setClosed(true);
        attestation.setDeleted(false);
        attestation.setDescription("The characteristics of someone or something");
        attestation.setEndDate(mock(Date.class));
        attestation.setStartDate(mock(Date.class));
        attestationRepository.save(attestation);

        registration.setAttestation(attestation);
        registration.setDeleted(false);
        registration.setResidentCardNumber(1L);
        registrationRepository.save(registration);
        registrationRepository.save(registration);
    }

    @AfterEach
    void tearDown() {
        attestationRepository.deleteAll();
        registrationRepository.deleteAll();
    }

    @Test
    void testFindAllResidentCardNumber() {
        UUID id = registration.getAttestation().getId();
        assertNotNull(this.registrationRepository.findAllResidentCardNumber(id));
        assertEquals(this.registrationRepository.findAllResidentCardNumber(id).size(), 1);
    }

    @Test
    void testExistsByResidentCardNumber() {
        assertTrue(this.registrationRepository.existsByResidentCardNumber(1L));
    }
}

