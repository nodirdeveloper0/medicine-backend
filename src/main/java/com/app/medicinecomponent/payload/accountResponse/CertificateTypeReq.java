package com.app.medicinecomponent.payload.accountResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CertificateTypeReq {
    private String type;
    private String description;
    private Object serviceName;
}
