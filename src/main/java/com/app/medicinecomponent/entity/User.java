package com.app.medicinecomponent.entity;

import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.entity.template.AbcEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
@SQLDelete(sql = "update users set deleted=true where id=?")
@Where(clause = "deleted=false")
public class User extends AbcEntity implements UserDetails {

    @Column(nullable = false)
    private String firstname;

    @Column(nullable = false)
    private String lastname;

    @Column(nullable = false, unique = true)
    private Long residentCardNumber;

    @Column(nullable = false)
    private String password;

    private Integer workingExperienceInMonth;

    @Enumerated(EnumType.STRING)
    private QualificationCategory qualificationCategory;

    @OneToOne(fetch = FetchType.LAZY)
    private JobTitle jobTitle;

    @ManyToOne(fetch = FetchType.LAZY)
    private Clinic clinic;

    @ManyToOne(fetch = FetchType.LAZY)
    private Role role;

    @OneToOne
    private Attachment avatarImg;

    private boolean active;

    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return role.getPermissionEnums().stream().map(permissionEnum ->
                new SimpleGrantedAuthority(permissionEnum.name())
        ).collect(Collectors.toSet());
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.residentCardNumber.toString();
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public User(String firstname, String lastname, String password, Role role, Long residentCardNumber) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.role = role;
        this.residentCardNumber = residentCardNumber;
    }

    public User(String firstname, String lastname, Long residentCardNumber, String password, Integer workingExperienceInMonth, QualificationCategory qualificationCategory, JobTitle jobTitle, boolean active, Clinic clinic, Role role, Attachment avatarImg) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.residentCardNumber = residentCardNumber;
        this.password = password;
        this.workingExperienceInMonth = workingExperienceInMonth;
        this.qualificationCategory = qualificationCategory;
        this.jobTitle = jobTitle;
        this.active = active;
        this.clinic = clinic;
        this.role = role;
        this.avatarImg = avatarImg;
    }
}
