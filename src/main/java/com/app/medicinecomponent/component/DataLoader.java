package com.app.medicinecomponent.component;

import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.enums.DiseaseInfoStatusEnums;
import com.app.medicinecomponent.entity.enums.PermissionEnum;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.clinicResponse.ClinicInfoFromCityManagement;
import com.app.medicinecomponent.repository.*;
import com.app.medicinecomponent.security.hmac.HMACUtilService;
import com.app.medicinecomponent.service.ExternalAPIsService;
import com.app.medicinecomponent.utils.RestConstant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class DataLoader implements CommandLineRunner {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JobTitleRepository jobTitleRepository;
    private final AttestationRepository attestationRepository;
    private final DiseaseInfoRepository diseaseInfoRepository;
    private final ExternalAPIsService externalAPIsService;
    private final AddressRepository addressRepository;
    private final ClinicRepository clinicRepository;
    private final HMACUtilService hmacUtilService;

    public DataLoader(UserRepository userRepository, RoleRepository repository, PasswordEncoder passwordEncoder, JobTitleRepository jobTitleRepository, AttestationRepository attestationRepository, DiseaseInfoRepository diseaseInfoRepository, ExternalAPIsService externalAPIsService, AddressRepository addressRepository, ClinicRepository clinicRepository, HMACUtilService hmacUtilService) {
        this.userRepository = userRepository;
        this.roleRepository = repository;
        this.passwordEncoder = passwordEncoder;
        this.jobTitleRepository = jobTitleRepository;
        this.attestationRepository = attestationRepository;
        this.diseaseInfoRepository = diseaseInfoRepository;
        this.externalAPIsService = externalAPIsService;
        this.addressRepository = addressRepository;
        this.clinicRepository = clinicRepository;
        this.hmacUtilService = hmacUtilService;
    }

    @Value("${spring.sql.init.mode}")
    private String dataLoaderMode;

    @Override
    public void run(String... args) throws Exception {
        if (dataLoaderMode.equals("always")) {
            //ROLE ADDED
            Role superAdmin = roleRepository.save(new Role(
                    RestConstant.ADMIN,
                    new HashSet<>(Arrays.asList(PermissionEnum.values()))
            ));
            Role doctor = roleRepository.save(new Role(
                    RestConstant.DOCTOR,
                    new HashSet<>(Arrays.asList(PermissionEnum.REQUEST_REGISTRATION, PermissionEnum.UPLOAD_ATTACHMENT))
            ));

            //JOB TITLE ADDED
            JobTitle surgeon = jobTitleRepository.save(new JobTitle("SURGEON"));
            JobTitle therapist = jobTitleRepository.save(new JobTitle("THERAPIST"));
            JobTitle pediatrician = jobTitleRepository.save(new JobTitle("PEDIATRICIAN"));
            JobTitle anesthesiologist = jobTitleRepository.save(new JobTitle("ANESTHESIOLOGIST"));
            JobTitle cardiologist = jobTitleRepository.save(new JobTitle("CARDIOLOGIST"));
            JobTitle dermatologist = jobTitleRepository.save(new JobTitle("DERMATOLOGIST"));
            JobTitle neurologist = jobTitleRepository.save(new JobTitle("NEUROLOGIST"));
            JobTitle urologist = jobTitleRepository.save(new JobTitle("UROLOGIST"));
            JobTitle pulmonologist = jobTitleRepository.save(new JobTitle("PULMONOLOGIST"));
            JobTitle orthopedic = jobTitleRepository.save(new JobTitle("ORTHOPEDIC"));

            //ADMIN ADDED
            userRepository.save(new User(
                    "admin",
                    "admin",
                    110000000L,
                    passwordEncoder.encode("admin"),
                    0,
                    null,
                    null,
                    true,
                    null,
                    superAdmin,
                    null
            ));

            //CLINIC ADDED
            ClinicInfoFromCityManagement clinicInfo = externalAPIsService.getClinicInfoFromCityManagement(54942823L);
            if (!clinicInfo.getSuccess())
                throw new RestException(HttpStatus.NOT_FOUND, "Clinic not found in receiving Clinic info from City Management");
            Clinic clinic = new Clinic(
                    clinicInfo.getResult().getName(),
                    clinicInfo.getResult().getCode(),
                    "+998971234567",
                    clinicInfo.getResult().getAddress(),
                    clinicInfo.getResult().getActive()
            );
            addressRepository.save(clinicInfo.getResult().getAddress());
            clinicRepository.save(clinic);


            //DISEASE INFO ADDED
            List<String> clinicIdsInString = clinicRepository.findAllIdsAsString();
            List<UUID> clinicIdsInUUID = clinicIdsInString.stream().map(UUID::fromString).collect(Collectors.toList());
            DiseaseInfo diseaseInfo = new DiseaseInfo(
                    "Sam",
                    "Robert",
                    47034917L,
                    "i don't feel well.My temperature seems to have risen",
                    DiseaseInfoStatusEnums.NEW,
                    clinicRepository.findById(clinicIdsInUUID.get((int) (Math.random() * clinicIdsInUUID.size()))).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Clinic is not found")),
                    new Timestamp(System.currentTimeMillis()),
                    null
            );
            diseaseInfoRepository.save(diseaseInfo);
        }
    }

    public static void main(String[] args) {

        long hmacExpireTime = 6000000L;
        String timestamp = String.valueOf(new Date().getTime() + hmacExpireTime);
        System.out.println("Timestamp -> " + timestamp);

        /* CLINIC MODULE  ->to  Medicine   */
//        String s = Main.calculateHASH("CLINIC", "clinicKey", "get_all_clinic", String.valueOf(now));
//        String s = Main.calculateHASH("CLINIC", "clinicKey", "get_one_clinic", String.valueOf(now));
//        String s = Main.calculateHASH("CLINIC", "clinicKey", "get_one_user", String.valueOf(now));

        /* ACCOUNT_SERVICE MODULE ->to  Medicine  */
//        String s = Main.calculateHASH("ACCOUNT_SERVICE", "citizenAccountKey", "send_disease_info", String.valueOf(now));

        /* MEDICINE ->to  City Management */
//        String s = Main.calculateHASH("MEDICINE", "medicineKey", "get_resident_info", String.valueOf(now));
//        String s = Main.calculateHASH("MEDICINE", "medicineKey", "get_clinic_info", String.valueOf(now));

        /* MEDICINE  ->to   Account Service */
        String signature = DataLoader.calculateHASH("MEDICINE", "certificate", timestamp, "medicineKey");
        System.out.println("Signature -> " + signature);


    }

    public static String calculateHASH(String keyId, String action, String timestamp, String secretKey) {
        String data = "keyId=" + keyId + ";timestamp=" + timestamp + ";action=" + action;
        try {
            SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            return new String(Base64.getEncoder().encode(rawHmac));
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }
}
