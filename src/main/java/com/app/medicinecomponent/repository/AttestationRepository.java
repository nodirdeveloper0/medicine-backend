package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Attestation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface AttestationRepository extends JpaRepository<Attestation, UUID> {

    Optional<Attestation> findByIdAndClosedFalse(UUID id);
//    boolean existsByStartDateAndClosedFalse(Date startDate);

    @Query(value = "select a.* from Attestation a where a.closed = false",nativeQuery = true)
    List<Attestation> findAllByClosedFalse();

    @Query(value = "select *\n" +
            "from attestation a where (:startDate between a.start_date and a.end_date\n" +
            "or :endDate between a.start_date and a.end_date\n" +
            "or a.start_date between :startDate and :endDate\n" +
            "or a.end_date between :startDate and :endDate) and closed=false", nativeQuery = true)
    List<Attestation> findAllByExistsDate(@Param("startDate") Date startDate,
                                          @Param("endDate") Date endDate);

    @Query(value = "select *\n" +
            "from attestation a where a.id != :id and (:startDate between a.start_date and a.end_date\n" +
            "or :endDate between a.start_date and a.end_date\n" +
            "or a.start_date between :startDate and :endDate\n" +
            "or a.end_date between :startDate and :endDate) and closed=false", nativeQuery = true)
    List<Attestation> findAllByExistsDateIdNot(@Param("id") UUID id,
                                               @Param("startDate") Date startDate,
                                          @Param("endDate") Date endDate);




}
