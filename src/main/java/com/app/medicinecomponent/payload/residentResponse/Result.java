package com.app.medicinecomponent.payload.residentResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private Long cardNumber;
    private String firstName;
    private String lastName;
    private String gender;
    private String photoId;
    private Boolean active;
}
