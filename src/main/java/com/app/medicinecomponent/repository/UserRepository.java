package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Role;
import com.app.medicinecomponent.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByIdAndActiveTrue(UUID id);

    Optional<User> findByResidentCardNumberAndActiveTrue(Long residentCardNumber);

    Optional<User> findByResidentCardNumber(Long residentCardNumber);

    List<User> findAllByResidentCardNumberInAndActiveTrue(Collection<Long> residentCardNumber);

    List<User> findAllByClinicId(UUID clinic_id);

    List<User> findAllByRole(Role role);

    boolean existsByResidentCardNumber(Long residentCardNumber);
}
