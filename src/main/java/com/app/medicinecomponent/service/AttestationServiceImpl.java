package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Attestation;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.AttestationReqDTO;
import com.app.medicinecomponent.payload.AttestationResDTO;
import com.app.medicinecomponent.repository.AttestationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AttestationServiceImpl implements AttestationService {
    private final AttestationRepository attestationRepository;

    public AttestationServiceImpl(AttestationRepository attestationRepository) {
        this.attestationRepository = attestationRepository;
    }

    @Override
    public ApiResult<AttestationResDTO> add(AttestationReqDTO attestationReqDTO) {
        /* this method checks given startDate shouldn't be in the past */
//        validateDateOrThrow(attestationReqDTO.getStartDate());

        /*This method checks given startDate and endDate shouldn't be in the interval of other dates */
        validateConflictDatesOrThrowExistInAdd(attestationReqDTO.getStartDate(), attestationReqDTO.getEndDate());

        Attestation attestation = new Attestation(
                attestationReqDTO.getStartDate(),
                attestationReqDTO.getEndDate(),
                attestationReqDTO.getDescription(),
                false
        );
        attestationRepository.save(attestation);
        return ApiResult.successResponse(CustomMapper.attestationToDTO(attestation));
    }

    @Override
    public ApiResult<AttestationResDTO> edit(AttestationReqDTO attestationReqDTO, UUID id) {

        /* this method checks given startTime shouldn't be in the past */
        validateDateOrThrow(attestationReqDTO.getStartDate());

        /* This method checks given startDate and endDate shouldn't be in the interval of other dates without self */
        validateConflictDatesOrThrowExistInEdit(id, attestationReqDTO.getStartDate(), attestationReqDTO.getEndDate());

        Attestation attestation = attestationRepository.findByIdAndClosedFalse(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Attestation is not found! or you can't edit closed attestation"));
        if (attestation.getStartDate().after(new Date(System.currentTimeMillis()))) {
            attestation.setStartDate(attestationReqDTO.getStartDate());
        }
        attestation.setEndDate(attestation.getEndDate());
        attestation.setDescription(attestationReqDTO.getDescription());
        attestationRepository.save(attestation);
        return ApiResult.successResponse(CustomMapper.attestationToDTO(attestation));
    }


    @Override
    public ApiResult<AttestationResDTO> getOne(UUID id) {
        Attestation attestation = attestationRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Attestation is not found!"));
        return ApiResult.successResponse(CustomMapper.attestationToDTO(attestation));
    }

    @Override
    public ApiResult<List<AttestationResDTO>> getAll() {
        List<Attestation> attestationList = attestationRepository.findAll();
        return ApiResult.successResponse(CustomMapper.attestationListToDTO(attestationList));
    }

    @Override
    public ApiResult<List<AttestationResDTO>> getAllCloseFalse() {
        List<Attestation> allByClosedFalse = attestationRepository.findAllByClosedFalse();
        return ApiResult.successResponse(CustomMapper.attestationListToDTO(allByClosedFalse));
    }

    /**
     * This method checks given startDate shouldn't be in the past
     *
     * @param startDate:Date
     */
    public static void validateDateOrThrow(Date startDate) {
        if (startDate.after(new Date(System.currentTimeMillis())))
            throw new RestException(HttpStatus.BAD_REQUEST, "Attestation start date shouldn't be in the past!");
    }

    /*
     * This method checks given startDate and endDate shouldn't be in the interval of other dates
     *
     * @param startDate:Date
     * @param endDate:Date
     */
    public void validateConflictDatesOrThrowExistInAdd(Date startDate, Date endDate) {
        List<Attestation> conflictDates = attestationRepository.findAllByExistsDate(
                startDate,
                endDate);
        if (!conflictDates.isEmpty())
            throw new RestException(
                    HttpStatus.CONFLICT,
                    "Two attestations shouldn't be at same time!",
                    conflictDates.stream().map(CustomMapper::attestationToDTO).collect(Collectors.toList()));
    }


    /**
     * This method checks given startDate and endDate shouldn't be in the interval of other dates without self
     *
     * @param id:UUID
     * @param startDate:Date
     * @param endDate:Date
     */
    public void validateConflictDatesOrThrowExistInEdit(UUID id, Date startDate, Date endDate) {
        List<Attestation> conflictDatesIdNot = attestationRepository.findAllByExistsDateIdNot(
                id,
                startDate,
                endDate);
        if (!conflictDatesIdNot.isEmpty())
            throw new RestException(
                    HttpStatus.CONFLICT,
                    "Attestation already exists  on this date",
                    conflictDatesIdNot.stream().map(CustomMapper::attestationToDTO).collect(Collectors.toList()));
    }


}
