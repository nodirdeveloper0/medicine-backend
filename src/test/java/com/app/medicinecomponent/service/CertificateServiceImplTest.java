package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.enums.CertificateTypeEnum;
import com.app.medicinecomponent.entity.enums.PermissionEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.payload.accountResponse.*;
import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.Address;
import com.app.medicinecomponent.entity.Attachment;
import com.app.medicinecomponent.entity.Attestation;
import com.app.medicinecomponent.entity.Certificate;
import com.app.medicinecomponent.entity.Clinic;
import com.app.medicinecomponent.entity.JobTitle;
import com.app.medicinecomponent.entity.Role;
import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.payload.CertificateReqDTO;
import com.app.medicinecomponent.payload.accountResponse.*;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeResponseFromAccountService;
import com.app.medicinecomponent.repository.*;
import com.app.medicinecomponent.repository.AttestationRepository;
import com.app.medicinecomponent.repository.CertificateRepository;
import com.app.medicinecomponent.repository.JobTitleRepository;
import com.app.medicinecomponent.repository.RegistrationRepository;
import com.app.medicinecomponent.repository.UserRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {CertificateServiceImpl.class})
@ExtendWith(SpringExtension.class)
class CertificateServiceImplTest {
    @MockBean
    private AttestationRepository attestationRepository;

    @MockBean
    private CertificateRepository certificateRepository;

    @Autowired
    private CertificateServiceImpl certificateServiceImpl;

    @MockBean
    private ExternalAPIsService externalAPIsService;

    @MockBean
    private JobTitleRepository jobTitleRepository;

    @MockBean
    private RegistrationRepository registrationRepository;

    @MockBean
    private UserRepository userRepository;

    User user = new User();
    Role role = new Role();
    JobTitle jobTitle = new JobTitle();
    Clinic clinic = new Clinic();
    Address address = new Address();
    Attachment attachment = new Attachment();
    Attestation attestation = new Attestation();
    Certificate certificate = new Certificate();


    @BeforeEach
    void setUp() {

        attachment.setContentType("text/plain");
        attachment.setCreatedAt(mock(Timestamp.class));
        attachment.setCreatedBy(UUID.randomUUID());
        attachment.setDeleted(true);
        attachment.setId(UUID.randomUUID());
        attachment.setName("Name");
        attachment.setSize(3L);
        attachment.setUpdatedAt(mock(Timestamp.class));
        attachment.setUpdatedBy(UUID.randomUUID());


        address.setCreatedAt(mock(Timestamp.class));
        address.setCreatedBy(UUID.randomUUID());
        address.setDeleted(true);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setId(UUID.randomUUID());
        address.setStreet("Street");
        address.setUpdatedAt(mock(Timestamp.class));
        address.setUpdatedBy(UUID.randomUUID());


        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setCreatedAt(mock(Timestamp.class));
        clinic.setCreatedBy(UUID.randomUUID());
        clinic.setDeleted(true);
        clinic.setId(UUID.randomUUID());
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinic.setUpdatedAt(mock(Timestamp.class));
        clinic.setUpdatedBy(UUID.randomUUID());


        jobTitle.setCreatedAt(mock(Timestamp.class));
        jobTitle.setCreatedBy(UUID.randomUUID());
        jobTitle.setDeleted(true);
        jobTitle.setId(UUID.randomUUID());
        jobTitle.setName("Name");
        jobTitle.setUpdatedAt(mock(Timestamp.class));
        jobTitle.setUpdatedBy(UUID.randomUUID());


        role.setCreatedAt(mock(Timestamp.class));
        role.setCreatedBy(UUID.randomUUID());
        role.setDeleted(true);
        role.setId(UUID.randomUUID());
        role.setName("Name");
        role.setPermissionEnums(new HashSet<>());
        role.setUpdatedAt(mock(Timestamp.class));
        role.setUpdatedBy(UUID.randomUUID());


        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setActive(true);
        user.setAvatarImg(attachment);
        user.setClinic(clinic);
        user.setCreatedAt(mock(Timestamp.class));
        user.setCreatedBy(UUID.randomUUID());
        user.setCredentialsNonExpired(true);
        user.setDeleted(true);
        user.setEnabled(true);
        user.setFirstname("Jane");
        user.setId(UUID.randomUUID());
        user.setJobTitle(jobTitle);
        user.setLastname("Doe");
        user.setPassword("iloveyou");
        user.setQualificationCategory(QualificationCategory.SPECIALIST);
        user.setResidentCardNumber(1L);
        user.setRole(role);
        user.setUpdatedAt(mock(Timestamp.class));
        user.setUpdatedBy(UUID.randomUUID());
        user.setWorkingExperienceInMonth(1);

        attestation.setClosed(true);
        attestation.setCreatedAt(mock(Timestamp.class));
        attestation.setCreatedBy(UUID.randomUUID());
        attestation.setDeleted(true);
        attestation.setDescription("The characteristics of someone or something");
        attestation.setEndDate(mock(Date.class));
        attestation.setId(UUID.randomUUID());
        attestation.setStartDate(mock(Date.class));
        attestation.setUpdatedAt(mock(Timestamp.class));
        attestation.setUpdatedBy(UUID.randomUUID());

        Date date = mock(Date.class);
        when(date.getTime()).thenReturn(10L);

        certificate.setAttestation(attestation);
        certificate.setCertificateTypeEnum(CertificateTypeEnum.CERTIFICATE_OF_REGISTRATION);
        certificate.setCreatedAt(mock(Timestamp.class));
        certificate.setCreatedBy(UUID.randomUUID());
        certificate.setDeleted(true);
        certificate.setDescription("The characteristics of someone or something");
        certificate.setFromDate(mock(Date.class));
        certificate.setId(UUID.randomUUID());
        certificate.setJobTitle(jobTitle);
        certificate.setQualificationCategory(QualificationCategory.SPECIALIST);
        certificate.setSerialNumber(10);
        certificate.setToDate(mock(Date.class));
        certificate.setToUser(user);
        certificate.setUpdatedAt(mock(Timestamp.class));
        certificate.setUpdatedBy(UUID.randomUUID());
    }


    @Test
    void testAdd3() {

        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findById((UUID) any())).thenReturn(ofResult);

        Optional<JobTitle> ofResult1 = Optional.of(jobTitle);
        when(this.jobTitleRepository.findById((UUID) any())).thenReturn(ofResult1);
        when(this.externalAPIsService
                .saveCertificateType((CertificateTypeReq) any()))
                .thenReturn(new CertificateTypeResponseFromAccountService());

        Date date = mock(Date.class);
        when(date.getTime()).thenReturn(10L);

        Optional<Certificate> ofResult2 = Optional.of(certificate);
        when(this.certificateRepository.save((Certificate) any()))
                .thenThrow(new RestException(HttpStatus.CONTINUE, "An error occurred"));
        when(this.certificateRepository.countAll()).thenReturn(3);
        when(this.certificateRepository.findByFromDateAndToUserId((UUID) any())).thenReturn(ofResult2);

        Optional<Attestation> ofResult3 = Optional.of(attestation);
        when(this.attestationRepository.findById((UUID) any())).thenReturn(ofResult3);

        Date date1 = mock(Date.class);
        when(date1.after((Date) any())).thenReturn(true);
        Date date2 = mock(Date.class);
        when(date2.after((Date) any())).thenReturn(true);
        UUID toUserId = UUID.randomUUID();
        assertThrows(RestException.class,
                () -> this.certificateServiceImpl.add(new CertificateReqDTO(toUserId, date1, date2, UUID.randomUUID())));
        verify(this.userRepository).findById((UUID) any());
        verify(this.jobTitleRepository).findById((UUID) any());
        verify(this.certificateRepository).countAll();
        verify(this.certificateRepository).save((Certificate) any());
        verify(this.certificateRepository, atLeast(1)).findByFromDateAndToUserId((UUID) any());
//        verify(date).getTime();
        verify(this.attestationRepository).findById((UUID) any());
        verify(date1).after((java.util.Date) any());
        verify(date2).after((java.util.Date) any());
    }

    @Test
    void testGetOne() {
        UUID randomUUIDResult = UUID.randomUUID();
        attestation.setId(randomUUIDResult);

        UUID randomUUIDResult1 = UUID.randomUUID();
        jobTitle.setId(randomUUIDResult1);

        UUID randomUUIDResult2 = UUID.randomUUID();
        attachment.setId(randomUUIDResult2);

        UUID randomUUIDResult3 = UUID.randomUUID();
        role.setId(randomUUIDResult3);

        HashSet<PermissionEnum> permissionEnumSet = new HashSet<>();
        role.setPermissionEnums(permissionEnumSet);

        UUID randomUUIDResult4 = UUID.randomUUID();
        user.setId(randomUUIDResult4);

        UUID randomUUIDResult5 = UUID.randomUUID();
        certificate.setId(randomUUIDResult5);

        Optional<Certificate> ofResult = Optional.of(certificate);
        when(this.certificateRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<CertificateResDTO> actualOne = this.certificateServiceImpl.getOne(UUID.randomUUID());
        assertTrue(actualOne.isSuccess());
        assertNull(actualOne.getErrors());
        assertNull(actualOne.getMessage());

        CertificateResDTO data = actualOne.getData();
        assertEquals(10, data.getSerialNumber().intValue());
        Assertions.assertEquals(QualificationCategory.SPECIALIST, data.getQualificationCategory());
        assertSame(randomUUIDResult5, data.getId());
        Assertions.assertEquals(CertificateTypeEnum.CERTIFICATE_OF_REGISTRATION, data.getCertificateTypeEnum());
        assertEquals("The characteristics of someone or something", data.getDescription());

        AttestationResDTO attestation1 = data.getAttestation();
        assertEquals("The characteristics of someone or something", attestation1.getDescription());
        assertTrue(attestation1.getClosed());

        UserResDTO toUser = data.getToUser();
        assertEquals(1, toUser.getWorkingExperienceInMonth().intValue());
        assertEquals(1L, toUser.getResidentCardNumber().longValue());
        assertTrue(toUser.isActive());

        JobTitleResDTO jobTitle2 = data.getJobTitle();
        assertEquals("Name", jobTitle2.getName());
        assertSame(randomUUIDResult1, jobTitle2.getId());
        Assertions.assertEquals(QualificationCategory.SPECIALIST, toUser.getQualificationCategory());
        assertSame(randomUUIDResult4, toUser.getId());
        assertEquals("Doe", toUser.getLastname());
        assertEquals("Jane", toUser.getFirstname());
        assertSame(randomUUIDResult2, toUser.getAvatarImgId());
        assertSame(randomUUIDResult, attestation1.getId());

        RoleResDTO role1 = toUser.getRole();
        assertSame(randomUUIDResult3, role1.getId());
        assertSame(permissionEnumSet, role1.getPermissionEnumSet());
        assertEquals("Name", role1.getName());
        verify(this.certificateRepository).findById((UUID) any());
    }

    @Test
    void testGetAll2() {
        ArrayList<Certificate> certificateList = new ArrayList<>();
        certificateList.add(certificate);
        when(this.certificateRepository.findAll()).thenReturn(certificateList);
        ApiResult<List<CertificateResDTO>> actualAll = this.certificateServiceImpl.getAll();
        assertEquals(1, actualAll.getData().size());
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getMessage());
        assertNull(actualAll.getErrors());
        verify(this.certificateRepository).findAll();
    }

    @Test
    void testGetAllByUserId2() {
        ArrayList<Certificate> certificateList = new ArrayList<>();
        certificateList.add(certificate);
        when(this.certificateRepository.findAllByToUserId((UUID) any())).thenReturn(certificateList);
        ApiResult<List<CertificateResDTO>> actualAllByUserId = this.certificateServiceImpl
                .getAllByUserId(UUID.randomUUID());
        assertEquals(1, actualAllByUserId.getData().size());
        assertTrue(actualAllByUserId.isSuccess());
        assertNull(actualAllByUserId.getMessage());
        assertNull(actualAllByUserId.getErrors());
        verify(this.certificateRepository).findAllByToUserId((UUID) any());
    }

    @Test
    void testGetAllByUserId3() {
        when(this.certificateRepository.findAllByToUserId((UUID) any()))
                .thenThrow(new RestException(HttpStatus.CONTINUE, "An error occurred"));
        assertThrows(RestException.class, () -> this.certificateServiceImpl.getAllByUserId(UUID.randomUUID()));
        verify(this.certificateRepository).findAllByToUserId((UUID) any());
    }

    @Test
    void testDefineQualificationCategory6() {
        Assertions.assertEquals(QualificationCategory.CATEGORY_1, CertificateServiceImpl.defineQualificationCategory(user));

        user.setQualificationCategory(QualificationCategory.CATEGORY_1);
        Assertions.assertEquals(QualificationCategory.CATEGORY_2, CertificateServiceImpl.defineQualificationCategory(user));

        user.setQualificationCategory(QualificationCategory.CATEGORY_2);
        Assertions.assertEquals(QualificationCategory.CATEGORY_3, CertificateServiceImpl.defineQualificationCategory(user));
    }

    @Test
    void testDefineDescriptionByQualificationCategory() {
        assertEquals(
                "This certificate was issued to confirm that JANE DOE's qualification level had risen to the" + " CATEGORY_1",
                CertificateServiceImpl.defineDescriptionByQualificationCategory(user));

        user.setQualificationCategory(QualificationCategory.CATEGORY_1);
        assertEquals(
                "This certificate was issued to confirm that JANE DOE's qualification level had risen to the" + " CATEGORY_2",
                CertificateServiceImpl.defineDescriptionByQualificationCategory(user));


        user.setQualificationCategory(QualificationCategory.CATEGORY_2);
        assertEquals(
                "This certificate was issued to confirm that JANE DOE's qualification level had risen to the" + " CATEGORY_3",
                CertificateServiceImpl.defineDescriptionByQualificationCategory(user));
    }

    @Test
    void testCreateCertificateType5() {
        Links links = new Links();
        links.setSelf(new Self("MEDICINE"));
        Root root = mock(Root.class);
        when(root.get_links()).thenReturn(links);

        CertificateTypeResponseFromAccountService certificateTypeResponseFromAccountService = new CertificateTypeResponseFromAccountService();
        certificateTypeResponseFromAccountService.setRoot(root);
        when(this.externalAPIsService
                .saveCertificateType((CertificateTypeReq) any()))
                .thenReturn(certificateTypeResponseFromAccountService);

        assertEquals("MEDICINE", this.certificateServiceImpl.createCertificateType(certificate));
        verify(this.externalAPIsService)
                .saveCertificateType((CertificateTypeReq) any());
        verify(root, atLeast(1)).get_links();
    }

    @Test
    void testCreateCertificate() {
        CertificateResponseFromAccountService certificateResponseFromAccountService = new CertificateResponseFromAccountService();
        certificateResponseFromAccountService.description = "The characteristics of someone or something";
        certificateResponseFromAccountService.expiredAt = mock(Date.class);
        certificateResponseFromAccountService.cardNumber = 10;
        certificateResponseFromAccountService.firstName = "Jane";
        certificateResponseFromAccountService.lastName = "Doe";
        certificateResponseFromAccountService._embedded = new Embedded();
        certificateResponseFromAccountService._links = new Links();
        when(this.externalAPIsService
                .saveCertificate((CertificateReq) any()))
                .thenReturn(certificateResponseFromAccountService);

        this.certificateServiceImpl.createCertificate(certificate, user, "42");
        verify(this.externalAPIsService)
                .saveCertificate((CertificateReq) any());
    }
}

