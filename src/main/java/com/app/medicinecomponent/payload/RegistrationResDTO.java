package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationResDTO {
    private UUID id;
    private Long residentCardNumber;
    private AttestationResDTO attestation;
}
