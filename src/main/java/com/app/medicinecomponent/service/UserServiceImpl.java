package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Clinic;
import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.CustomPage;
import com.app.medicinecomponent.payload.UserResDTO;
import com.app.medicinecomponent.repository.ClinicRepository;
import com.app.medicinecomponent.repository.UserRepository;
import com.app.medicinecomponent.utils.RestConstant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ClinicRepository clinicRepository;

    public UserServiceImpl(UserRepository userRepository, ClinicRepository clinicRepository) {
        this.userRepository = userRepository;
        this.clinicRepository = clinicRepository;
    }

    @Override
    public ApiResult<UserResDTO> getOne(UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User is not found or not active for system"));
        return ApiResult.successResponse(CustomMapper.userToResDTO(user));
    }

    @Override
    public ApiResult<UserResDTO> getOneByCardNumber(Long cardNumber) {
        User user = userRepository.findByResidentCardNumber(cardNumber).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User is not found or not active for system"));
        return ApiResult.successResponse(CustomMapper.userToResDTO(user));
    }


    @Override
    public ApiResult<CustomPage<UserResDTO>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> userPage = userRepository.findAll(pageable);
        List<User> newUserList = new ArrayList<>();
        for (User user : userPage.getContent()) {
            if (user.getRole().getName().equals(RestConstant.DOCTOR)) {
                newUserList.add(user);
            }
        }
        Page<User> newUserPage = new PageImpl<>(newUserList);
        CustomPage<UserResDTO> userResDTOCustomPage = userToCustomPage(newUserPage);
        return ApiResult.successResponse(userResDTOCustomPage);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            userRepository.deleteById(id);
            return ApiResult.successResponse("User successfully deleted!");
        } catch (Exception e) {
            throw new RestException(HttpStatus.NOT_FOUND, "User is not found!");
        }
    }

    @Override
    public ApiResult<List<UserResDTO>> getAllByClinicId(UUID clinicId) {
        List<User> users = userRepository.findAllByClinicId(clinicId);
        users.removeIf(user -> user.getRole().getName().equals(RestConstant.ADMIN));
        return ApiResult.successResponse(CustomMapper.userListToDTO(users));
    }


    @Override
    public ApiResult<List<UserResDTO>> getAllByClinicCardNumber(Long clinicCardNumber) {
        Clinic clinic = clinicRepository.findByClinicLegalCardNumber(clinicCardNumber).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "CLinic is not exist!"));
        List<User> allByClinicId = userRepository.findAllByClinicId(clinic.getId());
        allByClinicId.removeIf(user -> user.getRole().getName().equals(RestConstant.ADMIN));
        return ApiResult.successResponse(CustomMapper.userListToDTO(allByClinicId));
    }

    @Override
    public ApiResult<?> activateUser(Long cardNumber) {
        User user = userRepository.findByResidentCardNumberAndActiveTrue(cardNumber).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User is not found for activating!"));
        user.setActive(true);
        userRepository.save(user);
        return ApiResult.successResponse("User is activated successfully saved");
    }

    @Override
    public ApiResult<?> deactivateUser(Long cardNumber) {
        User user = userRepository.findByResidentCardNumberAndActiveTrue(cardNumber).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User is not found for deactivating!"));
        user.setActive(false);
        userRepository.save(user);
        return ApiResult.successResponse("User is deactivated successfully saved");
    }



    private CustomPage<UserResDTO> userToCustomPage(Page<User> usersPage) {
        return new CustomPage<UserResDTO>(
                usersPage.getContent().stream().map(CustomMapper::userToResDTO).collect(Collectors.toList()),
                usersPage.getNumberOfElements(),
                usersPage.getNumber(),
                usersPage.getTotalElements(),
                usersPage.getTotalPages(),
                usersPage.getSize()
        );
    }

}
