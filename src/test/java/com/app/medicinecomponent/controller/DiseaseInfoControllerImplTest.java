package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.entity.enums.DiseaseInfoStatusEnums;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.service.DiseaseInfoServiceImpl;
import com.app.medicinecomponent.payload.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DiseaseInfoControllerImpl.class})
class DiseaseInfoControllerImplTest {

    @Autowired
    private DiseaseInfoControllerImpl diseaseInfoController;

    @MockBean
    private DiseaseInfoServiceImpl diseaseInfoService;

    DiseaseInfoReqDTO diseaseInfoReqDTO = new DiseaseInfoReqDTO();
    DiseaseInfoResDto diseaseInfoResDto = new DiseaseInfoResDto();

    @BeforeEach
    void setUp() {
        AddressResDTO addressResDTO = new AddressResDTO();
        addressResDTO.setId(UUID.randomUUID());
        addressResDTO.setHomeNumber("12");
        addressResDTO.setStreet("Oybek");
        addressResDTO.setDistrict("Yunusobod");

        ClinicResDTO clinicResDTO = new ClinicResDTO();
        clinicResDTO.setId(UUID.randomUUID());
        clinicResDTO.setName("AKFA");
        clinicResDTO.setClinicLegalCardNumber(123L);
        clinicResDTO.setPhoneNumber("+998981234567");
        clinicResDTO.setAddress(addressResDTO);
        clinicResDTO.setActive(true);

        diseaseInfoReqDTO.setFirstName("John");
        diseaseInfoReqDTO.setLastName("Doe");
        diseaseInfoReqDTO.setCardNumber(1L);
        diseaseInfoReqDTO.setDescription("This is description");

        diseaseInfoResDto.setId(UUID.randomUUID());
        diseaseInfoResDto.setFirstName("John");
        diseaseInfoResDto.setLastName("Doe");
        diseaseInfoResDto.setCardNumber(1L);
        diseaseInfoResDto.setClinicResDTO(clinicResDTO);
        diseaseInfoResDto.setReceivedTime(mock(Timestamp.class));
        diseaseInfoResDto.setSentTime(mock(Timestamp.class));
        diseaseInfoResDto.setDescription("This is description");
        diseaseInfoResDto.setStatus(DiseaseInfoStatusEnums.NEW);

    }

    @Test
    void add() throws Exception {
        when(this.diseaseInfoService.add((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, diseaseInfoResDto, null, true));
        String content = (new ObjectMapper().writeValueAsString(diseaseInfoReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/disease-info")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.diseaseInfoController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getOne() throws Exception {
        when(this.diseaseInfoService.getOne((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, diseaseInfoResDto, null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/disease-info/{id}",UUID.randomUUID());
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.diseaseInfoController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAll() throws Exception {
        List<DiseaseInfoResDto> diseaseInfoResDtoList = new ArrayList<>(Collections.singletonList(diseaseInfoResDto));
        when(this.diseaseInfoService.getAll()).thenReturn(new ApiResult<>(null, diseaseInfoResDtoList, null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/disease-info");
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.diseaseInfoController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));


    }
}