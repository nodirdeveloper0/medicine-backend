package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttestationResDTO {

    private UUID id;
    private Date startDate;
    private Date endDate;
    private String description;
    private Boolean closed;
}
