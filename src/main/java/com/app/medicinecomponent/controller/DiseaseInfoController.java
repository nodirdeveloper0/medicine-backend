package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.DiseaseInfoReqDTO;
import com.app.medicinecomponent.payload.DiseaseInfoResDto;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RequestMapping(RestConstant.DISEASE_INFO)
@Tag(name = "DiseaseInfo operations", description = "DiseaseInfo")
public interface DiseaseInfoController {

    @Operation(summary = "Send disease info via medicine module to random clinic (Account service modul)")
    @PostMapping
    ApiResult<DiseaseInfoResDto> add(@RequestBody @Valid DiseaseInfoReqDTO diseaseInfoReqDTO);

    @PreAuthorize(value = "hasAuthority('VIEW_DISEASE_INFO')")
    @Operation(summary = "Get one disease Info (admin)")
    @GetMapping("/{id}")
    ApiResult<DiseaseInfoResDto> getOne(@PathVariable UUID id);

    @PreAuthorize(value = "hasAuthority('VIEW_DISEASE_INFO')")
    @Operation(summary = "Get all disease Info (admin)")
    @GetMapping
    ApiResult<List<DiseaseInfoResDto>> getAll();
}
