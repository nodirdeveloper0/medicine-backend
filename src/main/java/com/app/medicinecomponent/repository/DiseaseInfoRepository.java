package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.DiseaseInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
@Repository
public interface DiseaseInfoRepository extends JpaRepository<DiseaseInfo, UUID> {

    @Query(value = "select *\n" +
            "from disease_info where status='NEW'",nativeQuery = true)
    List<DiseaseInfo> findAllByStatusNew();
}
