package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttestationReqDTO {

    @NotNull(message = "Attestation start date shouldn't be empty!")
    private Date startDate;

    @NotNull(message = "Attestation end date shouldn't be empty!")
    private Date endDate;

    @NotBlank(message = "Description shouldn't be empty!")
    private String description;
}
