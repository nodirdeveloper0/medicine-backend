package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.CustomPage;
import com.app.medicinecomponent.payload.UserResDTO;

import java.util.List;
import java.util.UUID;

public interface UserService {
    ApiResult<UserResDTO> getOne(UUID id);

    ApiResult<CustomPage<UserResDTO>> getAll(int page, int size);

    ApiResult<?> delete(UUID id);

    ApiResult<List<UserResDTO>> getAllByClinicId(UUID clinicId);

    ApiResult<UserResDTO> getOneByCardNumber(Long cardNumber);

    ApiResult<?> activateUser(Long cardNumber);

    ApiResult<?> deactivateUser(Long cardNumber);

    ApiResult<List<UserResDTO>> getAllByClinicCardNumber(Long clinicCardNumber);

}
