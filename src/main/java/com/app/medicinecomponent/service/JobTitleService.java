package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.JobTitleReqDTO;
import com.app.medicinecomponent.payload.JobTitleResDTO;

import java.util.List;
import java.util.UUID;

public interface JobTitleService {

    ApiResult<JobTitleResDTO> add(JobTitleReqDTO jobTitleReqDTO);

    ApiResult<JobTitleResDTO> getOne(UUID id);

    ApiResult<List<JobTitleResDTO>> getAll();

    ApiResult<JobTitleResDTO> edit(JobTitleReqDTO jobTitleReqDTO, UUID id);

}
