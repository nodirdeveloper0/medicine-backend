package com.app.medicinecomponent.entity;


import com.app.medicinecomponent.entity.enums.DiseaseInfoStatusEnums;
import com.app.medicinecomponent.entity.template.AbcEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update disease_info set deleted=true where id=?")
@Where(clause = "deleted=false")
public class DiseaseInfo extends AbcEntity {

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private Long cardNumber;

    @Column(nullable = false,columnDefinition = "text")
    private String description;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DiseaseInfoStatusEnums status;

    @ManyToOne(fetch = FetchType.LAZY)
    private Clinic clinic;

    @Column(nullable = false)
    private Timestamp receivedTime;

    private Timestamp sentTime;
}
