package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.service.RegistrationServiceImpl;
import com.app.medicinecomponent.payload.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.HashSet;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {RegistrationControllerImpl.class})
class RegistrationControllerImplTest {

    @Autowired
    private RegistrationControllerImpl registrationController;

    @MockBean
    private RegistrationServiceImpl registrationService;


    RegistrationResDTO registrationResDTO = new RegistrationResDTO();
    RegistrationReqDTO registrationReqDTO = new RegistrationReqDTO();
    UserResDTO userResDTO = new UserResDTO();

    @BeforeEach
    void setUp() {
        AddressResDTO addressResDTO = new AddressResDTO();
        addressResDTO.setId(UUID.randomUUID());
        addressResDTO.setHomeNumber("12");
        addressResDTO.setStreet("Oybek");
        addressResDTO.setDistrict("Yunusobod");

        ClinicResDTO clinicResDTO = new ClinicResDTO();
        clinicResDTO.setId(UUID.randomUUID());
        clinicResDTO.setName("AKFA");
        clinicResDTO.setClinicLegalCardNumber(123L);
        clinicResDTO.setPhoneNumber("+998981234567");
        clinicResDTO.setAddress(addressResDTO);
        clinicResDTO.setActive(true);

        JobTitleResDTO jobTitleResDTO = new JobTitleResDTO();
        jobTitleResDTO.setId(UUID.randomUUID());
        jobTitleResDTO.setName("DENTIST");

        RoleResDTO roleResDTO = new RoleResDTO();
        roleResDTO.setId(UUID.randomUUID());
        roleResDTO.setName("DOCTOR");
        roleResDTO.setPermissionEnumSet(new HashSet<>());

        userResDTO.setId(UUID.randomUUID());
        userResDTO.setFirstname("Jane");
        userResDTO.setLastname("Doe");
        userResDTO.setResidentCardNumber(1L);
        userResDTO.setWorkingExperienceInMonth(1);
        userResDTO.setQualificationCategory(QualificationCategory.SPECIALIST);
        userResDTO.setJobTitle(jobTitleResDTO);
        userResDTO.setClinic(clinicResDTO);
        userResDTO.setRole(roleResDTO);
        userResDTO.setActive(true);
        userResDTO.setAvatarImgUrl("This is path for avatar img");
        userResDTO.setAvatarImgId(UUID.randomUUID());


        AttestationResDTO attestationResDTO = new AttestationResDTO();
        attestationResDTO.setId(UUID.randomUUID());
        attestationResDTO.setStartDate(mock(java.sql.Date.class));
        attestationResDTO.setEndDate(mock(java.sql.Date.class));
        attestationResDTO.setDescription("The characteristics of someone or something");
        attestationResDTO.setClosed(true);

        registrationResDTO.setId(UUID.randomUUID());
        registrationResDTO.setResidentCardNumber(1L);
        registrationResDTO.setAttestation(attestationResDTO);

        registrationReqDTO.setResidentCardNumber(1L);
        registrationReqDTO.setAttestationId(UUID.randomUUID());
    }

    @Test
    void add() throws Exception {
        when(this.registrationService.add(ArgumentMatchers.any(),ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, registrationResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(registrationReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/registration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.registrationController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAllRegisteredUserByAttestationId() throws Exception {

        RegisteredUsersResDTO registeredUsersResDTO = new RegisteredUsersResDTO();
        registeredUsersResDTO.setAttestationId(UUID.randomUUID());
        registeredUsersResDTO.setUserList(Collections.singletonList(userResDTO));

        when(this.registrationService.getAllRegisteredUserByAttestationId((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null,registeredUsersResDTO , null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/registration/attestation-id/{attestationId}",UUID.randomUUID());
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.registrationController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));


    }
}