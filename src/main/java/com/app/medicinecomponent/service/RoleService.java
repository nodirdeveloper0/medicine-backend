package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RoleResDTO;

import java.util.List;
import java.util.UUID;

public interface RoleService {
    ApiResult<RoleResDTO> getOne(UUID id);

    ApiResult<List<RoleResDTO>> getAll();
}
