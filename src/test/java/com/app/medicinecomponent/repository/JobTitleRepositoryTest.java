package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.JobTitle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.sql.Timestamp;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@ContextConfiguration(classes = {JobTitleRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class JobTitleRepositoryTest {
    @Autowired
    private JobTitleRepository jobTitleRepository;

    JobTitle jobTitle = new JobTitle();

    @BeforeEach
    void setUp() {
        jobTitle.setCreatedAt(mock(Timestamp.class));
        jobTitle.setCreatedBy(UUID.randomUUID());
        jobTitle.setDeleted(false);
        jobTitle.setId(UUID.randomUUID());
        jobTitle.setName("DENTIST");
        jobTitle.setUpdatedAt(mock(Timestamp.class));
        jobTitle.setUpdatedBy(UUID.randomUUID());
        jobTitleRepository.save(jobTitle);
    }

    @AfterEach
    void tearDown() {
        jobTitleRepository.delete(jobTitle);
    }

    @Test
    void existsByName() {
        assertTrue(jobTitleRepository.existsByName("DENTIST"));
        assertFalse(jobTitleRepository.existsByName("DEN"));
    }

    @Test
    void existsByNameAndIdNot() {
        UUID uuid = UUID.randomUUID();
        jobTitle.setId(uuid);
        assertTrue(jobTitleRepository.existsByNameAndIdNot("DENTIST",uuid));
    }
}