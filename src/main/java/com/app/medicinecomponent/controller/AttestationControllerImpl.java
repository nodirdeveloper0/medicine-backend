package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.AttestationReqDTO;
import com.app.medicinecomponent.payload.AttestationResDTO;
import com.app.medicinecomponent.service.AttestationService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class AttestationControllerImpl implements AttestationController {
    private final AttestationService attestationService;

    public AttestationControllerImpl(AttestationService attestationService) {
        this.attestationService = attestationService;
    }

    @Override
    public ApiResult<AttestationResDTO> add(AttestationReqDTO attestationReqDTO) {
        return attestationService.add(attestationReqDTO);
    }

    @Override
    public ApiResult<AttestationResDTO> edit(AttestationReqDTO attestationReqDTO, UUID id) {
        return attestationService.edit(attestationReqDTO, id);
    }

    @Override
    public ApiResult<AttestationResDTO> getOne(UUID id) {
        return attestationService.getOne(id);
    }

    @Override
    public ApiResult<List<AttestationResDTO>> getAll() {
        return attestationService.getAll();
    }

    @Override
    public ApiResult<List<AttestationResDTO>> getAllCloseFalse() {
        return attestationService.getAllCloseFalse();
    }
}
