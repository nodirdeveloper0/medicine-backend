package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Attachment;
import com.app.medicinecomponent.payload.ApiResult;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public interface AttachmentService {

    ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException;

    ApiResult<Attachment> getById(UUID id);

    ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException;

}
