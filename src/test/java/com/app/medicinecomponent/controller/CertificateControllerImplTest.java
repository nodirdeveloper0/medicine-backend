package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.entity.enums.CertificateTypeEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.service.CertificateServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.sql.Date;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CertificateControllerImpl.class})
class CertificateControllerImplTest {

    @Autowired
    private CertificateControllerImpl certificateController;

    @MockBean
    private CertificateServiceImpl certificateService;

    CertificateResDTO certificateResDTO = new CertificateResDTO();
    CertificateReqDTO certificateReqDTO = new CertificateReqDTO();

    @BeforeEach
    void setUp() {
        AddressResDTO addressResDTO = new AddressResDTO();
        addressResDTO.setId(UUID.randomUUID());
        addressResDTO.setHomeNumber("12");
        addressResDTO.setStreet("Oybek");
        addressResDTO.setDistrict("Yunusobod");

        ClinicResDTO clinicResDTO = new ClinicResDTO();
        clinicResDTO.setId(UUID.randomUUID());
        clinicResDTO.setName("AKFA");
        clinicResDTO.setClinicLegalCardNumber(123L);
        clinicResDTO.setPhoneNumber("+998981234567");
        clinicResDTO.setAddress(addressResDTO);
        clinicResDTO.setActive(true);

        JobTitleResDTO jobTitleResDTO = new JobTitleResDTO();
        jobTitleResDTO.setId(UUID.randomUUID());
        jobTitleResDTO.setName("DENTIST");

        RoleResDTO roleResDTO = new RoleResDTO();
        roleResDTO.setId(UUID.randomUUID());
        roleResDTO.setName("DOCTOR");
        roleResDTO.setPermissionEnumSet(new HashSet<>());

        UserResDTO userResDTO = new UserResDTO();
        userResDTO.setId(UUID.randomUUID());
        userResDTO.setFirstname("Jane");
        userResDTO.setLastname("Doe");
        userResDTO.setResidentCardNumber(1L);
        userResDTO.setWorkingExperienceInMonth(1);
        userResDTO.setQualificationCategory(QualificationCategory.SPECIALIST);
        userResDTO.setJobTitle(jobTitleResDTO);
        userResDTO.setClinic(clinicResDTO);
        userResDTO.setRole(roleResDTO);
        userResDTO.setActive(true);
        userResDTO.setAvatarImgUrl("This is path for avatar img");
        userResDTO.setAvatarImgId(UUID.randomUUID());

        AttestationResDTO attestationResDTO = new AttestationResDTO();
        attestationResDTO.setId(UUID.randomUUID());
        attestationResDTO.setStartDate(mock(Date.class));
        attestationResDTO.setEndDate(mock(Date.class));
        attestationResDTO.setDescription("The characteristics of someone or something");
        attestationResDTO.setClosed(true);

        certificateReqDTO.setToUserId(UUID.randomUUID());
        certificateReqDTO.setAttestationId(UUID.randomUUID());
        certificateReqDTO.setFromDate(mock(Date.class));
        certificateReqDTO.setToDate(mock(Date.class));

        certificateResDTO.setId(UUID.randomUUID());
        certificateResDTO.setSerialNumber(10);
        certificateResDTO.setToUser(userResDTO);
        certificateResDTO.setFromDate(mock(Date.class));
        certificateResDTO.setToDate(mock(Date.class));
        certificateResDTO.setQualificationCategory(QualificationCategory.SPECIALIST);
        certificateResDTO.setJobTitle(jobTitleResDTO);
        certificateResDTO.setAttestation(attestationResDTO);
        certificateResDTO.setCertificateTypeEnum(CertificateTypeEnum.CERTIFICATE_OF_REGISTRATION);
        certificateResDTO.setDescription("The characteristics of someone or something");
    }

    @Test
    void add() throws Exception {
        when(this.certificateService.add((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, certificateResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(certificateReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/certificate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.certificateController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getOne() throws Exception {
        when(this.certificateService.getOne((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, certificateResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/certificate/{id}", UUID.randomUUID());
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.certificateController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAll() throws Exception {
        List<CertificateResDTO> certificateResDTOList = new ArrayList<>(Collections.singletonList(certificateResDTO));

        when(this.certificateService.getAll()).thenReturn(new ApiResult<>(null, certificateResDTOList, null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/certificate");
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.certificateController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAllByUserId() throws Exception {
        List<CertificateResDTO> certificateResDTOList = new ArrayList<>(Collections.singletonList(certificateResDTO));
        when(this.certificateService.getAllByUserId((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, certificateResDTOList, null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/certificate/certificates-of-one/{userId}", UUID.randomUUID());
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.certificateController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }
}