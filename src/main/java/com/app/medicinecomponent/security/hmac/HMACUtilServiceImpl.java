package com.app.medicinecomponent.security.hmac;

import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.util.Base64;
import java.util.Date;

@Component
public class HMACUtilServiceImpl implements HMACUtilService {
    @Override
    public String calculateHASH(String keyId, String timestamp, String action, String secretKey) {
        String data = "keyId=" + keyId + ";timestamp=" + timestamp + ";action=" + action;
        try {
            SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            return new String(Base64.getEncoder().encode(rawHmac));
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    @Override
    public boolean hasAccess(String keyId, String timestamp, String action, String signature) {
        long now = new Date().getTime()+600000; // keyId = HOTEL, timestamp: 2222532252, action: get_resident, signature: gfdfg455464==
        String secretKey = "";
        switch (keyId) {
            case "CLINIC":
                secretKey+= "clinicKey";
                break;
            case "ACCOUNT_SERVICE":
                secretKey+= "accountServiceKey";
                break;
            case "CITY_MANAGEMENT":
                secretKey+= "cityManagementKey";
                break;
        }
        if(now>Long.parseLong(timestamp) && secretKey==null) {
            return false;
        }
        String testSignature = calculateHASH(keyId, timestamp, action, secretKey);
        return testSignature.equals(signature);
    }
}
