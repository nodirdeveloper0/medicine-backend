package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@ContextConfiguration(classes = {UserRepository.class, RoleRepository.class, JobTitleRepository.class, ClinicRepository.class, AddressRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class UserRepositoryTest {
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private ClinicRepository clinicRepository;
    @Autowired
    private JobTitleRepository jobTitleRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;

    User user = new User();
    Role role = new Role();

    @BeforeEach
    void setUp() {
        Address address = new Address();
        address.setDeleted(false);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setStreet("Street");
        addressRepository.save(address);

        Clinic clinic = new Clinic();
        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setDeleted(false);
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinicRepository.save(clinic);

        JobTitle jobTitle = new JobTitle();
        jobTitle.setDeleted(false);
        jobTitle.setName("Name");
        jobTitleRepository.save(jobTitle);


        role.setDeleted(false);
        role.setName("Name");
        role.setPermissionEnums(new HashSet<>());
        roleRepository.save(role);

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setActive(true);
        user.setClinic(clinic);
        user.setCredentialsNonExpired(true);
        user.setDeleted(false);
        user.setEnabled(true);
        user.setFirstname("Jane");
        user.setJobTitle(jobTitle);
        user.setLastname("Doe");
        user.setPassword("iloveyou");
        user.setQualificationCategory(QualificationCategory.SPECIALIST);
        user.setResidentCardNumber(1L);
        user.setRole(role);
        user.setWorkingExperienceInMonth(1);
        userRepository.save(user);
    }

    @AfterEach
    void tearDown() {
        addressRepository.deleteAll();
        clinicRepository.deleteAll();
        jobTitleRepository.deleteAll();
        roleRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void testFindByIdAndActiveTrue() {
        UUID userId = user.getId();
        Optional<User> userByIdAndActiveTrue = this.userRepository.findByIdAndActiveTrue(userId);
        assertNotNull(this.userRepository.findByIdAndActiveTrue(userId).get());
        userByIdAndActiveTrue.ifPresent(value -> assertEquals(value, user));
    }

    @Test
    void testFindByResidentCardNumberAndActiveTrue() {
        Optional<User> byResidentCardNumberAndActiveTrue = this.userRepository.findByResidentCardNumberAndActiveTrue(1L);
        assertNotNull(this.userRepository.findByResidentCardNumberAndActiveTrue(1L).get());
        byResidentCardNumberAndActiveTrue.ifPresent(value -> assertEquals(value, user));
    }

    @Test
    void testFindByResidentCardNumber() {
        Optional<User> userByResidentCardNumber = this.userRepository.findByResidentCardNumber(1L);
        assertNotNull(this.userRepository.findByResidentCardNumber(1L).get());
        userByResidentCardNumber.ifPresent(value -> assertEquals(value, user));
    }

    @Test
    void testFindAllByResidentCardNumberInAndActiveTrue() {
        List<User> allUserByCardNumberList = this.userRepository.findAllByResidentCardNumberInAndActiveTrue(new ArrayList<>(Arrays.asList(1L, 2L, 3L)));
        assertEquals(allUserByCardNumberList.size(),1);
        assertEquals(this.userRepository.findAllByResidentCardNumberInAndActiveTrue(new ArrayList<>(Arrays.asList(1L, 2L, 3L))).get(0),user);
    }

    @Test
    void testFindAllByClinicId() {
        UUID clinicId = user.getClinic().getId();
        assertEquals(this.userRepository.findAllByClinicId(clinicId).get(0), user);
        assertEquals(this.userRepository.findAllByClinicId(UUID.randomUUID()).size(), 0);
    }

    @Test
    void testFindAllByRole() {
        assertNotNull(this.userRepository.findAllByRole(role));
        assertEquals(this.userRepository.findAllByRole(role).size(), 1);
        assertEquals(this.userRepository.findAllByRole(role).get(0), user);
    }

    @Test
    void testExistsByResidentCardNumber() {
        assertTrue(this.userRepository.existsByResidentCardNumber(1L));
    }
}

