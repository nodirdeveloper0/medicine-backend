package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Registration;
import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RegisteredUsersResDTO;
import com.app.medicinecomponent.payload.RegistrationReqDTO;
import com.app.medicinecomponent.repository.AttestationRepository;
import com.app.medicinecomponent.repository.RegistrationRepository;
import com.app.medicinecomponent.repository.UserRepository;
import com.app.medicinecomponent.payload.RegistrationResDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    private final RegistrationRepository registrationRepository;
    private final AttestationRepository attestationRepository;
    private final UserRepository userRepository;

    public RegistrationServiceImpl(RegistrationRepository registrationRepository, AttestationRepository attestationRepository, UserRepository userRepository) {
        this.registrationRepository = registrationRepository;
        this.attestationRepository = attestationRepository;
        this.userRepository = userRepository;
    }


    @Override
    public ApiResult<RegistrationResDTO> add(RegistrationReqDTO registrationReqDTO, User currentUser) {
        //Here i checked requesting person already registered or not
        boolean exists = registrationRepository.existsByResidentCardNumber(registrationReqDTO.getResidentCardNumber());
        if (exists){
            throw new RestException(HttpStatus.CONFLICT,"You already registered");
        }

        //Here i checked  registering user exists or not in database
        User user = userRepository.findByResidentCardNumberAndActiveTrue(registrationReqDTO.getResidentCardNumber()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User is not found!"));

        //Strange people can't register one can only in system
        if (!currentUser.getResidentCardNumber().equals(user.getResidentCardNumber())) {
            throw new RestException(HttpStatus.CONFLICT,"You should sign up first");
        }

        //Here i divide userExperienceInMonth to 12 for getting experience in years, cause user can register in every four years
        double year = user.getWorkingExperienceInMonth() / 12d;
        if (user.getWorkingExperienceInMonth()==0)
            throw new RestException(HttpStatus.BAD_REQUEST,"Experience is not enough to participate in a attestation!");
        if (year % 4 != 0)
            throw new RestException(HttpStatus.BAD_REQUEST, "Experience is not enough to participate in a attestation!");

        Registration registration = new Registration(
                registrationReqDTO.getResidentCardNumber(),
                attestationRepository.findById(registrationReqDTO.getAttestationId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Attestation is not found!"))
        );
        registrationRepository.save(registration);
        return ApiResult.successResponse(CustomMapper.registrationToDTO(registration));
    }

    @Override
    public ApiResult<RegisteredUsersResDTO> getAllRegisteredUserByAttestationId(UUID attestationId) {
        List<Long> allResidentCardNumber = registrationRepository.findAllResidentCardNumber(attestationId);
        List<User> userList = userRepository.findAllByResidentCardNumberInAndActiveTrue(allResidentCardNumber);
        return ApiResult.successResponse(CustomMapper.registeredUsersToDTO(userList, attestationId));
    }
}
