package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.AttestationReqDTO;
import com.app.medicinecomponent.payload.AttestationResDTO;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.ATTESTATION_CONTROLLER)
@Tag(name = "Attestation operations",description = "Attestation")
public interface AttestationController {

    @PreAuthorize(value = "hasAuthority('ADD_ATTESTATION')")
    @Operation(summary = "Add attestation (admin)")
    @PostMapping
    ApiResult<AttestationResDTO> add(@RequestBody @Valid AttestationReqDTO attestationReqDTO);

    @PreAuthorize(value = "hasAuthority('EDIT_ATTESTATION')")
    @Operation(summary = "Edit attestation (admin)")
    @PutMapping("/{id}")
    ApiResult<AttestationResDTO> edit(@RequestBody @Valid AttestationReqDTO attestationReqDTO,
                                      @PathVariable UUID id);

    @Operation(summary = "Get one attestation (any user)")
    @GetMapping("/{id}")
    ApiResult<AttestationResDTO> getOne(@PathVariable UUID id);

    @PreAuthorize(value = "hasAuthority('ADD_ATTESTATION')")
    @Operation(summary = "Get all attestation for admin (admin)")
    @GetMapping
    ApiResult<List<AttestationResDTO>> getAll();

    @Operation(summary = "Get all attestations where closes are false for all (any user)")
    @GetMapping("/closed-false")
    ApiResult<List<AttestationResDTO>> getAllCloseFalse();
}
