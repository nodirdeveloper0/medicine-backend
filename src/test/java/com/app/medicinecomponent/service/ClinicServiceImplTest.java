package com.app.medicinecomponent.service;

import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.AddressResDTO;
import com.app.medicinecomponent.payload.ClinicReqDTO;
import com.app.medicinecomponent.payload.clinicResponse.ClinicInfoFromCityManagement;
import com.app.medicinecomponent.payload.clinicResponse.Result;
import com.app.medicinecomponent.entity.Address;
import com.app.medicinecomponent.entity.Clinic;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.ClinicResDTO;
import com.app.medicinecomponent.repository.AddressRepository;
import com.app.medicinecomponent.repository.ClinicRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {ClinicServiceImpl.class})
@ExtendWith(SpringExtension.class)
class ClinicServiceImplTest {
    @MockBean
    private AddressRepository addressRepository;

    @MockBean
    private ClinicRepository clinicRepository;

    @Autowired
    private ClinicServiceImpl clinicServiceImpl;

    @MockBean
    private ExternalAPIsServiceImpl externalAPIsServiceImpl;

    Address address = new Address();
    Clinic clinic = new Clinic();

    @BeforeEach
    void setUp() {
        address.setCreatedAt(mock(Timestamp.class));
        address.setCreatedBy(UUID.randomUUID());
        address.setDeleted(true);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setId(UUID.randomUUID());
        address.setStreet("Street");
        address.setUpdatedAt(mock(Timestamp.class));
        address.setUpdatedBy(UUID.randomUUID());

        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setCreatedAt(mock(Timestamp.class));
        clinic.setCreatedBy(UUID.randomUUID());
        clinic.setDeleted(true);
        clinic.setId(UUID.randomUUID());
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinic.setUpdatedAt(mock(Timestamp.class));
        clinic.setUpdatedBy(UUID.randomUUID());
    }

    @Test
        //one line
    void testAdd() {
        when(this.externalAPIsServiceImpl.getClinicInfoFromCityManagement((Long) any()))
                .thenReturn(new ClinicInfoFromCityManagement());
        when(this.clinicRepository.existsByClinicLegalCardNumber((Long) any())).thenReturn(true);
        assertThrows(RestException.class, () -> this.clinicServiceImpl.add(new ClinicReqDTO()));
        verify(this.clinicRepository).existsByClinicLegalCardNumber((Long) any());
    }

    @Test
        //one line
    void testAdd6() {
        ClinicInfoFromCityManagement clinicInfoFromCityManagement = new ClinicInfoFromCityManagement();
        clinicInfoFromCityManagement.setSuccess(false);
        when(this.externalAPIsServiceImpl.getClinicInfoFromCityManagement((Long) any()))
                .thenReturn(clinicInfoFromCityManagement);
        when(this.clinicRepository.existsByClinicLegalCardNumber((Long) any())).thenReturn(false);
        assertThrows(RestException.class, () -> this.clinicServiceImpl.add(new ClinicReqDTO()));
        verify(this.externalAPIsServiceImpl).getClinicInfoFromCityManagement((Long) any());
        verify(this.clinicRepository).existsByClinicLegalCardNumber((Long) any());
    }

    @Test
    void testAdd9() {
        UUID randomUUIDResult = UUID.randomUUID();
        address.setId(randomUUIDResult);

        Result result = new Result();
        result.setAddress(address);

        ClinicInfoFromCityManagement clinicInfoFromCityManagement = new ClinicInfoFromCityManagement(result, true);
        clinicInfoFromCityManagement.setSuccess(true);
        when(this.externalAPIsServiceImpl.getClinicInfoFromCityManagement((Long) any()))
                .thenReturn(clinicInfoFromCityManagement);

        when(this.clinicRepository.save((Clinic) any())).thenReturn(clinic);
        when(this.clinicRepository.existsByClinicLegalCardNumber((Long) any())).thenReturn(false);
        when(this.addressRepository.save((Address) any())).thenReturn(address);
        ApiResult<ClinicResDTO> actualAddResult = this.clinicServiceImpl.add(new ClinicReqDTO());
        assertTrue(actualAddResult.isSuccess());
        assertNull(actualAddResult.getErrors());
        assertNull(actualAddResult.getMessage());
        ClinicResDTO data = actualAddResult.getData();
        assertNull(data.getId());
        assertNull(data.getClinicLegalCardNumber());
        assertNull(data.getActive());
        assertNull(data.getPhoneNumber());
        assertNull(data.getName());
        AddressResDTO address3 = data.getAddress();
        assertEquals("42", address3.getHomeNumber());
        assertEquals("District", address3.getDistrict());
        assertSame(randomUUIDResult, address3.getId());
        assertEquals("Street", address3.getStreet());
        verify(this.externalAPIsServiceImpl).getClinicInfoFromCityManagement((Long) any());
        verify(this.clinicRepository).existsByClinicLegalCardNumber((Long) any());
        verify(this.clinicRepository).save((Clinic) any());
        verify(this.addressRepository).save((Address) any());
    }

    @Test
    void testGetOne() {
        UUID randomUUIDResult = UUID.randomUUID();
        address.setId(randomUUIDResult);
        UUID randomUUIDResult1 = UUID.randomUUID();
        clinic.setId(randomUUIDResult1);
        Optional<Clinic> ofResult = Optional.of(clinic);
        when(this.clinicRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<ClinicResDTO> actualOne = this.clinicServiceImpl.getOne(UUID.randomUUID());
        assertTrue(actualOne.isSuccess());
        assertNull(actualOne.getErrors());
        assertNull(actualOne.getMessage());
        ClinicResDTO data = actualOne.getData();
        assertSame(randomUUIDResult1, data.getId());
        assertEquals(1L, data.getClinicLegalCardNumber().longValue());
        assertEquals("4105551212", data.getPhoneNumber());
        assertTrue(data.getActive());
        assertEquals("Name", data.getName());
        AddressResDTO address1 = data.getAddress();
        assertEquals("Street", address1.getStreet());
        assertSame(randomUUIDResult, address1.getId());
        assertEquals("42", address1.getHomeNumber());
        assertEquals("District", address1.getDistrict());
        verify(this.clinicRepository).findById((UUID) any());
    }

    @Test
    void testGetOne3() {
        when(this.clinicRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.clinicServiceImpl.getOne(UUID.randomUUID()));
        verify(this.clinicRepository).findById((UUID) any());
    }


    @Test
    void testGetAll2() {
        when(this.clinicRepository.findAll()).thenReturn(new ArrayList<>());
        ApiResult<List<ClinicResDTO>> actualAll = this.clinicServiceImpl.getAll();
        assertTrue(actualAll.getData().isEmpty());
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getMessage());
        assertNull(actualAll.getErrors());
        verify(this.clinicRepository).findAll();
    }

    @Test
    void testGetAll3() {
        ArrayList<Clinic> clinicList = new ArrayList<>();
        clinicList.add(clinic);
        when(this.clinicRepository.findAll()).thenReturn(clinicList);
        ApiResult<List<ClinicResDTO>> actualAll = this.clinicServiceImpl.getAll();
        assertEquals(1, actualAll.getData().size());
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getMessage());
        assertNull(actualAll.getErrors());
        verify(this.clinicRepository).findAll();
    }

    @Test
    void testGetOneByLegalCardNumber() {
        UUID randomUUIDResult = UUID.randomUUID();
        address.setId(randomUUIDResult);
        UUID randomUUIDResult1 = UUID.randomUUID();
        clinic.setId(randomUUIDResult1);
        Optional<Clinic> ofResult = Optional.of(clinic);
        when(this.clinicRepository.findByClinicLegalCardNumber((Long) any())).thenReturn(ofResult);
        ApiResult<ClinicResDTO> actualOneByLegalCardNumber = this.clinicServiceImpl.getOneByLegalCardNumber(1L);
        assertTrue(actualOneByLegalCardNumber.isSuccess());
        assertNull(actualOneByLegalCardNumber.getErrors());
        assertNull(actualOneByLegalCardNumber.getMessage());
        ClinicResDTO data = actualOneByLegalCardNumber.getData();
        assertSame(randomUUIDResult1, data.getId());
        assertEquals(1L, data.getClinicLegalCardNumber().longValue());
        assertEquals("4105551212", data.getPhoneNumber());
        assertTrue(data.getActive());
        assertEquals("Name", data.getName());
        AddressResDTO address1 = data.getAddress();
        assertEquals("Street", address1.getStreet());
        assertSame(randomUUIDResult, address1.getId());
        assertEquals("42", address1.getHomeNumber());
        assertEquals("District", address1.getDistrict());
        verify(this.clinicRepository).findByClinicLegalCardNumber((Long) any());
    }

    @Test
    void testGetOneByLegalCardNumber4() {
        when(this.clinicRepository.findByClinicLegalCardNumber((Long) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.clinicServiceImpl.getOneByLegalCardNumber(1L));
        verify(this.clinicRepository).findByClinicLegalCardNumber((Long) any());
    }
}

