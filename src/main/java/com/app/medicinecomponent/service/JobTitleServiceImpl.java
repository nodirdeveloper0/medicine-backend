package com.app.medicinecomponent.service;

import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.JobTitleReqDTO;
import com.app.medicinecomponent.payload.JobTitleResDTO;
import com.app.medicinecomponent.repository.JobTitleRepository;
import com.app.medicinecomponent.entity.JobTitle;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * This is job title class
 */
@Service
public class JobTitleServiceImpl implements JobTitleService {
    private final JobTitleRepository jobTitleRepository;

    public JobTitleServiceImpl(JobTitleRepository jobTitleRepository) {
        this.jobTitleRepository = jobTitleRepository;
    }

    @Override
    public ApiResult<JobTitleResDTO> add(JobTitleReqDTO jobTitleReqDTO) {

        if (jobTitleRepository.existsByName(jobTitleReqDTO.getName()))
            throw new RestException(HttpStatus.CONFLICT, "Job title is already exist such name");

        JobTitle jobTitle = new JobTitle(
                jobTitleReqDTO.getName().toUpperCase());
        jobTitleRepository.save(jobTitle);
        return ApiResult.successResponse(CustomMapper.jobTitleToDTO(jobTitle));
    }

    @Override
    public ApiResult<JobTitleResDTO> getOne(UUID id) {
        JobTitle jobTitle = jobTitleRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Job title is not found!"));
        return ApiResult.successResponse(CustomMapper.jobTitleToDTO(jobTitle));
    }

    @Override
    public ApiResult<List<JobTitleResDTO>> getAll() {
        List<JobTitle> jobTitleList = jobTitleRepository.findAll();
        return ApiResult.successResponse(CustomMapper.jobTitleListToDTO(jobTitleList));
    }

    @Override
    public ApiResult<JobTitleResDTO> edit(JobTitleReqDTO jobTitleReqDTO, UUID id) {
        if (jobTitleRepository.existsByNameAndIdNot(jobTitleReqDTO.getName(), id))
            throw new RestException(HttpStatus.CONFLICT, "Job title already exist");
        JobTitle jobTitle = jobTitleRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Job title is not found!"));
        jobTitle.setName(jobTitleReqDTO.getName());
        jobTitleRepository.save(jobTitle);
        return ApiResult.successResponse(CustomMapper.jobTitleToDTO(jobTitle));
    }

}
