package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Clinic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface ClinicRepository extends JpaRepository<Clinic, UUID> {
    Optional<Clinic> findByClinicLegalCardNumber(Long clinicLegalCardNumber);

    boolean existsByClinicLegalCardNumber(Long clinicLegalCardNumber);

    @Query(value = "select cast(id as varchar) from clinic",
            nativeQuery = true)
    List<String> findAllIdsAsString();
}
