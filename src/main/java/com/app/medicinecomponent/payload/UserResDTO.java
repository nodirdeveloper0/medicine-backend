package com.app.medicinecomponent.payload;

import com.app.medicinecomponent.entity.enums.QualificationCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResDTO {
    private UUID id;

    private String firstname;

    private String lastname;

    private Long residentCardNumber;

    private Integer workingExperienceInMonth;

    private QualificationCategory qualificationCategory;

    private JobTitleResDTO jobTitle;

    private ClinicResDTO clinic;

    private RoleResDTO role;

    private boolean active;
    private String avatarImgUrl;
    private UUID avatarImgId;
}
