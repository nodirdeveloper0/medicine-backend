package com.app.medicinecomponent.entity;

import com.app.medicinecomponent.entity.template.AbcEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update clinic set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Clinic extends AbcEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false,unique = true)
    private Long clinicLegalCardNumber;

    @Column(nullable = false)
    private String phoneNumber;

    @OneToOne(fetch = FetchType.LAZY)
    private Address address;

    @Column(nullable = false)
    private Boolean active;
}
