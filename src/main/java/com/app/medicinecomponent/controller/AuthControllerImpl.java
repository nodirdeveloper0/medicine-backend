package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.service.AuthService;
import com.app.medicinecomponent.payload.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthControllerImpl implements AuthController {
    private final AuthService authService;

    public AuthControllerImpl(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public ApiResult<UserResDTO> signUp(SignUpReqDTO signUpReqDTO) {
        return authService.signUp(signUpReqDTO);
    }

    @Override
    public ApiResult<TokenResDto> signIn(SignInReqDTO signInReqDTO) {
        return authService.signIn(signInReqDTO);
    }

    @Override
    public ApiResult<?> analysisUser(AnalysisUserReqDTO analysisUserReqDTO) {
        return authService.analysisUser(analysisUserReqDTO);
    }
}
