package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RoleResDTO;
import com.app.medicinecomponent.service.RoleServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.UUID;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {RoleControllerImpl.class})
class RoleControllerImplTest {
    @Autowired
    private RoleControllerImpl roleController;

    @MockBean
    private RoleServiceImpl roleService;


    @Test
    void getOne() throws Exception {
        UUID uuid = UUID.randomUUID();
        RoleResDTO roleResDTO = new RoleResDTO(uuid,"DOCTOR",new HashSet<>());
        when(this.roleService.getOne(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null,roleResDTO,null,true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/role/{id}",uuid);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.roleController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAll() throws Exception {
        RoleResDTO roleResDTO = new RoleResDTO(UUID.randomUUID(),"DOCTOR",new HashSet<>());
        when(this.roleService.getOne(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null,roleResDTO,null,true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/role");
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.roleController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }
}