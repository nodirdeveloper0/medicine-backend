package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.AttestationReqDTO;
import com.app.medicinecomponent.payload.AttestationResDTO;

import java.util.List;
import java.util.UUID;

public interface AttestationService {
    ApiResult<AttestationResDTO> add(AttestationReqDTO attestationReqDTO);

    ApiResult<AttestationResDTO> getOne(UUID id);

    ApiResult<List<AttestationResDTO>> getAll();

    ApiResult<AttestationResDTO> edit(AttestationReqDTO attestationReqDTO, UUID id);

    ApiResult<List<AttestationResDTO>> getAllCloseFalse();

}
