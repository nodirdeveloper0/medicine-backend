package com.app.medicinecomponent.payload;

import com.app.medicinecomponent.entity.enums.QualificationCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResDTOForClinic {
    private UUID id;

    private String firstname;

    private String lastname;

    private Long residentCardNumber;

    private Integer workingExperienceWithMonth;

    private QualificationCategory qualificationCategory;

    private UUID jobTitleId;

    private UUID clinicId;

    private UUID roleId;

    private boolean active;
}
