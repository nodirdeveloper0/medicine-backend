package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.DiseaseInfoReqDTO;
import com.app.medicinecomponent.payload.DiseaseInfoResDto;
import com.app.medicinecomponent.security.hmac.AuthorizeRequest;
import com.app.medicinecomponent.service.DiseaseInfoService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class DiseaseInfoControllerImpl implements DiseaseInfoController{

    private final DiseaseInfoService diseaseInfoService;

    public DiseaseInfoControllerImpl(DiseaseInfoService diseaseInfoService) {
        this.diseaseInfoService = diseaseInfoService;
    }

    @AuthorizeRequest
    @Override
    public ApiResult<DiseaseInfoResDto> add(DiseaseInfoReqDTO diseaseInfoReqDTO) {
        return diseaseInfoService.add(diseaseInfoReqDTO);
    }

    @Override
    public ApiResult<DiseaseInfoResDto> getOne(UUID id) {
        return diseaseInfoService.getOne(id);
    }

    @Override
    public ApiResult<List<DiseaseInfoResDto>> getAll() {
        return diseaseInfoService.getAll();
    }
}
