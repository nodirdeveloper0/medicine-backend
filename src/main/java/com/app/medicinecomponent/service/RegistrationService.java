package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RegisteredUsersResDTO;
import com.app.medicinecomponent.payload.RegistrationReqDTO;
import com.app.medicinecomponent.payload.RegistrationResDTO;

import java.util.UUID;

public interface RegistrationService {
    ApiResult<RegistrationResDTO> add(RegistrationReqDTO registrationReqDTO, User user);

    ApiResult<RegisteredUsersResDTO> getAllRegisteredUserByAttestationId(UUID attestationId);
}
