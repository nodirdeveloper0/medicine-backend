package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.payload.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.UUID;

public interface AuthService extends UserDetailsService {

    UserDetails loadById(UUID id);

    ApiResult<UserResDTO> signUp(SignUpReqDTO signUpReqDTO);

    ApiResult<TokenResDto> signIn(SignInReqDTO signInReqDTO);

    ApiResult<?> analysisUser(AnalysisUserReqDTO analysisUserReqDTO);
}
