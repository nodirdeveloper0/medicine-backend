package com.app.medicinecomponent.security.hmac;

import com.app.medicinecomponent.exception.RestException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class AuthorizeRequestAspect {

    private final HMACUtilServiceImpl hmacUtilServiceImpl;

    public AuthorizeRequestAspect(HMACUtilServiceImpl hmacUtilServiceImpl) {
        this.hmacUtilServiceImpl = hmacUtilServiceImpl;
    }

    @Around("@annotation(com.app.medicinecomponent.security.hmac.AuthorizeRequest)")
    public Object authorizeRequest(ProceedingJoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String keyId = request.getHeader("sm-keyId"); // RECREATION
        String timestamp = request.getHeader("sm-timestamp"); // 26565956
        String action = request.getHeader("sm-action"); // get_resident
        String signature = request.getHeader("sm-signature"); // sdasfsgsd465463d5fsfes==
        if(keyId==null || timestamp==null || action==null || signature==null) {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ApiResponse.errorResponse("Headers are null!"));
            throw new RestException(HttpStatus.BAD_REQUEST,"Headers are null!");
        }
        boolean hasAccess = hmacUtilServiceImpl.hasAccess(keyId, timestamp, action, signature);
        if(!hasAccess) {
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ApiResponse.errorResponse("Unauthorized!"));
            throw new RestException(HttpStatus.UNAUTHORIZED,"Unauthorized");
        }
        return joinPoint.proceed();
    }
}
