package com.app.medicinecomponent.entity.enums;

public enum QualificationCategory {
    SPECIALIST("SPECIALIST"),
    CATEGORY_1("CATEGORY 1"),
    CATEGORY_2("CATEGORY 2"),
    CATEGORY_3("CATEGORY 3");

    private String name;

    QualificationCategory(String name) {
    }

    public String getName() {
        return name;
    }
}

