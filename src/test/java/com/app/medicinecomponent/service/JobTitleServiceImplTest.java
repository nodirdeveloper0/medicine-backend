package com.app.medicinecomponent.service;

import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.JobTitleReqDTO;
import com.app.medicinecomponent.payload.JobTitleResDTO;
import com.app.medicinecomponent.entity.JobTitle;
import com.app.medicinecomponent.repository.JobTitleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {JobTitleServiceImpl.class})
@ExtendWith(SpringExtension.class)
class JobTitleServiceImplTest {
    @MockBean
    private JobTitleRepository jobTitleRepository;

    @Autowired
    private JobTitleServiceImpl jobTitleServiceImpl;

    JobTitle jobTitle = new JobTitle();

    @BeforeEach
    void setUp() {
        jobTitle.setCreatedAt(mock(Timestamp.class));
        jobTitle.setCreatedBy(UUID.randomUUID());
        jobTitle.setDeleted(true);
        jobTitle.setId(UUID.randomUUID());
        jobTitle.setName("Name");
        jobTitle.setUpdatedAt(mock(Timestamp.class));
        jobTitle.setUpdatedBy(UUID.randomUUID());
    }

    @Test
    void testAdd() {
        when(this.jobTitleRepository.existsByName((String) any())).thenReturn(true);
        when(this.jobTitleRepository.save((JobTitle) any())).thenReturn(jobTitle);
        assertThrows(RestException.class, () -> this.jobTitleServiceImpl.add(new JobTitleReqDTO("Name")));
        verify(this.jobTitleRepository).existsByName((String) any());
    }

    @Test
    void testAdd2() {
        when(this.jobTitleRepository.existsByName((String) any())).thenReturn(false);
        when(this.jobTitleRepository.save((JobTitle) any())).thenReturn(jobTitle);
        ApiResult<JobTitleResDTO> actualAddResult = this.jobTitleServiceImpl.add(new JobTitleReqDTO("Name"));
        assertTrue(actualAddResult.isSuccess());
        assertNull(actualAddResult.getErrors());
        assertNull(actualAddResult.getMessage());
        JobTitleResDTO data = actualAddResult.getData();
        assertNull(data.getId());
        assertEquals("NAME", data.getName());
        verify(this.jobTitleRepository).existsByName((String) any());
        verify(this.jobTitleRepository).save((JobTitle) any());
    }

    @Test
    void testGetOne() {
        UUID randomUUIDResult = UUID.randomUUID();
        jobTitle.setId(randomUUIDResult);
        Optional<JobTitle> ofResult = Optional.of(jobTitle);
        when(this.jobTitleRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<JobTitleResDTO> actualOne = this.jobTitleServiceImpl.getOne(UUID.randomUUID());
        assertTrue(actualOne.isSuccess());
        assertNull(actualOne.getErrors());
        assertNull(actualOne.getMessage());
        JobTitleResDTO data = actualOne.getData();
        assertSame(randomUUIDResult, data.getId());
        assertEquals("Name", data.getName());
        verify(this.jobTitleRepository).findById((UUID) any());
    }

    @Test
    void testGetOne3() {
        when(this.jobTitleRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.jobTitleServiceImpl.getOne(UUID.randomUUID()));
        verify(this.jobTitleRepository).findById((UUID) any());
    }

    @Test
    void testGetAll2() {
        UUID randomUUIDResult = UUID.randomUUID();
        jobTitle.setId(randomUUIDResult);
        ArrayList<JobTitle> jobTitleList = new ArrayList<>();
        jobTitleList.add(jobTitle);
        when(this.jobTitleRepository.findAll()).thenReturn(jobTitleList);
        ApiResult<List<JobTitleResDTO>> actualAll = this.jobTitleServiceImpl.getAll();
        List<JobTitleResDTO> data = actualAll.getData();
        assertEquals(1, data.size());
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getErrors());
        assertNull(actualAll.getMessage());
        JobTitleResDTO getResult = data.get(0);
        assertSame(randomUUIDResult, getResult.getId());
        assertEquals("Name", getResult.getName());
        verify(this.jobTitleRepository).findAll();
    }

    @Test
    void testEdit() {
        Optional<JobTitle> ofResult = Optional.of(jobTitle);
        when(this.jobTitleRepository.existsByNameAndIdNot((String) any(), (UUID) any())).thenReturn(true);
        when(this.jobTitleRepository.findById((UUID) any())).thenReturn(ofResult);
        JobTitleReqDTO jobTitleReqDTO = new JobTitleReqDTO("Name");
        assertThrows(RestException.class, () -> this.jobTitleServiceImpl.edit(jobTitleReqDTO, UUID.randomUUID()));
        verify(this.jobTitleRepository).existsByNameAndIdNot((String) any(), (UUID) any());
    }

    @Test
    void testEdit2() {
        UUID randomUUIDResult = UUID.randomUUID();
        jobTitle.setId(randomUUIDResult);
        Optional<JobTitle> ofResult = Optional.of(jobTitle);
        when(this.jobTitleRepository.save((JobTitle) any())).thenReturn(jobTitle);
        when(this.jobTitleRepository.existsByNameAndIdNot((String) any(), (UUID) any())).thenReturn(false);
        when(this.jobTitleRepository.findById((UUID) any())).thenReturn(ofResult);
        JobTitleReqDTO jobTitleReqDTO = new JobTitleReqDTO("Name");
        ApiResult<JobTitleResDTO> actualEditResult = this.jobTitleServiceImpl.edit(jobTitleReqDTO, UUID.randomUUID());
        assertTrue(actualEditResult.isSuccess());
        assertNull(actualEditResult.getErrors());
        assertNull(actualEditResult.getMessage());
        JobTitleResDTO data = actualEditResult.getData();
        assertSame(randomUUIDResult, data.getId());
        assertEquals("Name", data.getName());
        verify(this.jobTitleRepository).existsByNameAndIdNot((String) any(), (UUID) any());
        verify(this.jobTitleRepository).save((JobTitle) any());
        verify(this.jobTitleRepository).findById((UUID) any());
    }

    @Test
    void testEdit5() {
        when(this.jobTitleRepository.save((JobTitle) any())).thenReturn(jobTitle);
        when(this.jobTitleRepository.existsByNameAndIdNot((String) any(), (UUID) any())).thenReturn(false);
        when(this.jobTitleRepository.findById((UUID) any())).thenReturn(Optional.empty());
        JobTitleReqDTO jobTitleReqDTO = new JobTitleReqDTO("Name");
        assertThrows(RestException.class, () -> this.jobTitleServiceImpl.edit(jobTitleReqDTO, UUID.randomUUID()));
        verify(this.jobTitleRepository).existsByNameAndIdNot((String) any(), (UUID) any());
        verify(this.jobTitleRepository).findById((UUID) any());
    }
}

//181 -> 138