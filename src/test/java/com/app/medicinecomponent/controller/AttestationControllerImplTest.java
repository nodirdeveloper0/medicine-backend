package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.AttestationReqDTO;
import com.app.medicinecomponent.payload.AttestationResDTO;
import com.app.medicinecomponent.service.AttestationServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AttestationControllerImpl.class})
class AttestationControllerImplTest {

    @Autowired
    private AttestationControllerImpl attestationController;

    @MockBean
    private AttestationServiceImpl attestationService;

    AttestationResDTO attestationResDTO = new AttestationResDTO();
    AttestationReqDTO attestationReqDTO = new AttestationReqDTO();

    @BeforeEach
    void setUp() {
        attestationResDTO.setId(UUID.randomUUID());
        attestationResDTO.setStartDate(mock(java.sql.Date.class));
        attestationResDTO.setEndDate(mock(java.sql.Date.class));
        attestationResDTO.setDescription("The characteristics of someone or something");
        attestationResDTO.setClosed(true);

        attestationReqDTO.setStartDate(mock(java.sql.Date.class));
        attestationReqDTO.setEndDate(mock(java.sql.Date.class));
        attestationReqDTO.setDescription("The characteristics of someone or something");



    }

    @Test
    void add() throws Exception {
        when(this.attestationService.add((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, attestationResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(attestationReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/attestation")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.attestationController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));

    }

    @Test
    void edit() throws Exception {
        when(this.attestationService.edit(ArgumentMatchers.any(),(ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, attestationResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(attestationReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.put("/api/attestation/{id}",UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.attestationController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getOne() throws Exception {
        when(this.attestationService.getOne((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, attestationResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/attestation/{id}",UUID.randomUUID());
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.attestationController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAll() throws Exception {
        List<AttestationResDTO> attestationResDTOList = new ArrayList<>(Arrays.asList(attestationResDTO));
        when(this.attestationService.getAll()).thenReturn(new ApiResult<>(null, attestationResDTOList, null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/attestation");
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.attestationController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAllCloseFalse() throws Exception {
        List<AttestationResDTO> attestationResDTOList = new ArrayList<>(Arrays.asList(attestationResDTO));
        when(this.attestationService.getAll()).thenReturn(new ApiResult<>(null, attestationResDTOList, null, true));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.get("/api/attestation/closed-false");
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.attestationController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }
}