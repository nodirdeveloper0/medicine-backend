package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Certificate;
import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.entity.enums.CertificateTypeEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.payload.accountResponse.CertificateReq;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeReq;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeResponseFromAccountService;
import com.app.medicinecomponent.payload.residentResponse.ResidentInfoFromCityManagement;
import com.app.medicinecomponent.repository.*;
import com.app.medicinecomponent.security.JwtProvider;
import com.app.medicinecomponent.utils.RestConstant;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.repository.*;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final ExternalAPIsService externalAPIsService;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final ClinicRepository clinicRepository;
    private final JobTitleRepository jobTitleRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;
    private final CertificateRepository certificateRepository;
    private final AttachmentRepository attachmentRepository;

    public AuthServiceImpl(@Lazy UserRepository userRepository, ExternalAPIsService externalAPIs, PasswordEncoder passwordEncoder, RoleRepository roleRepository, ClinicRepository clinicRepository, JobTitleRepository jobTitleRepository, AuthenticationManager authenticationManager, JwtProvider jwtProvider, CertificateRepository certificateRepository, AttachmentRepository attachmentRepository) {
        this.userRepository = userRepository;
        this.externalAPIsService = externalAPIs;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.clinicRepository = clinicRepository;
        this.jobTitleRepository = jobTitleRepository;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
        this.certificateRepository = certificateRepository;
        this.attachmentRepository = attachmentRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByResidentCardNumberAndActiveTrue(Long.parseLong(username)).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User not found"));
    }

    @Override
    public UserDetails loadById(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User not found!"));
    }

    @Override
    public ApiResult<UserResDTO> signUp(SignUpReqDTO signUpReqDTO) {
        if (userRepository.existsByResidentCardNumber(signUpReqDTO.getResidentCardNumber()))
            throw new RestException(HttpStatus.CONFLICT, "User already registered with card number!");

        ResidentInfoFromCityManagement residentInfo = externalAPIsService.getResidentInfoFromCityManagement(signUpReqDTO.getResidentCardNumber());
        if (!residentInfo.getSuccess())
            throw new RestException(HttpStatus.NOT_FOUND, "Error in receiving resident info from City Management!");
        if (!residentInfo.getResult().getActive()) {
            throw new RestException(HttpStatus.NOT_FOUND, "Resident Card Number is not valid!");
        }
        UUID role_not_found = roleRepository.findByName(RestConstant.DOCTOR).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Role not found")).getId();


        User user = new User(
                residentInfo.getResult().getFirstName(),
                residentInfo.getResult().getLastName(),
                residentInfo.getResult().getCardNumber(),
                passwordEncoder.encode(signUpReqDTO.getPassword()),
                0,
                QualificationCategory.SPECIALIST,
                jobTitleRepository.findById(signUpReqDTO.getJobTitleId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Job title is not found!")),
                residentInfo.getResult().getActive(),
                clinicRepository.findById(signUpReqDTO.getClinicId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Clinic is not found")),
                roleRepository.findByName(RestConstant.DOCTOR).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND,"ROle not found")),
                signUpReqDTO.getAttachmentId() != null ? attachmentRepository.findById(signUpReqDTO.getAttachmentId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Attachment not found")) : null
        );
        userRepository.save(user);

        /* Here incrementing serial number by plus 1 to all counted certificates number*/
        Integer count = certificateRepository.countAll();

        /* This defines expire date of certificate of registration */
        java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, 4);
        java.sql.Date addedYear = new java.sql.Date(calendar.getTimeInMillis());

        Certificate certificate = new Certificate(
                count + 1,
                user,
                new Date(System.currentTimeMillis()),
                addedYear,
                user.getQualificationCategory(),
                user.getJobTitle(),
                CertificateTypeEnum.CERTIFICATE_OF_REGISTRATION,
                "The certificate given to " + user.getFirstname() + "  " + user.getLastname() + " on the occasion of his registration as a " + user.getJobTitle().getName()
        );
        certificateRepository.save(certificate);

        /* At first certificateType will be created,send to account service and return certificate id*/
        String certificateTypeId = createCertificateType(user);
        if (certificateTypeId == null)
            throw new RestException(HttpStatus.SERVICE_UNAVAILABLE, "User signed up but , we couldn't create and send certificate(registration) to account service module");

        /* Second certificate will be created, send to account service */
        createCertificate(user, certificate, certificateTypeId);

        return ApiResult.successResponse(CustomMapper.userToResDTO(user));
    }

    @Override
    public ApiResult<TokenResDto> signIn(SignInReqDTO signInReqDTO) {
        try {
            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    String.valueOf(signInReqDTO.getResidentCardNumber()),
                    signInReqDTO.getPassword()
            ));
            User user = (User) authenticate.getPrincipal();
            if (!user.isActive())
                throw new RestException(HttpStatus.BAD_REQUEST, "This user is not active or can't enter to the system");

            String token = jwtProvider.generateJwtTokenFromId(user.getResidentCardNumber());
            return ApiResult.successResponse(new TokenResDto(token));
        } catch (Exception e) {
            throw new RestException(HttpStatus.NOT_FOUND, "Card number or password is incorrect!");
        }
    }

    @Override
    public ApiResult<?> analysisUser(AnalysisUserReqDTO analysisUserReqDTO) {
        String idFromToken = jwtProvider.getIdFromToken(analysisUserReqDTO.getToken().substring(7));
        User user = userRepository.findByResidentCardNumber(Long.valueOf(idFromToken)).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User is not found!"));
        return ApiResult.successResponse(user.getRole().getName());
    }

    public String createCertificateType(User user) {
        CertificateTypeResponseFromAccountService response = externalAPIsService.saveCertificateType(new CertificateTypeReq(
                CertificateTypeEnum.CERTIFICATE_OF_REGISTRATION.getName(),
                "The certificate given to " + user.getFirstname() + "  " + user.getLastname() + " on the occasion of his registration as a " + user.getJobTitle().getName(),
                "MEDICINE"
        ));
        System.out.println(response);
        System.out.println(response.getRoot());
        if (response.getRoot() == null)
            throw new RestException(HttpStatus.SERVICE_UNAVAILABLE, "WE can't send certificate to AccountService");

        int beginningOfCertificateTypeId = response.getRoot().get_links().getSelf().getHref().lastIndexOf("/");
        return response.getRoot().get_links().getSelf().getHref().substring(beginningOfCertificateTypeId + 1);
    }

    public void createCertificate(User user, Certificate certificate, String certificateTypeId) {
        externalAPIsService.saveCertificate(new CertificateReq(
                certificate.getDescription(),
                certificate.getToDate(),
                Math.toIntExact(user.getResidentCardNumber()),
                "certificateType/" + certificateTypeId,
                user.getFirstname(),
                user.getLastname()
        ));
    }
}
