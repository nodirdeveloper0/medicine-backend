package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.AboutUsReqDTO;
import com.app.medicinecomponent.payload.AboutUsResDTO;
import com.app.medicinecomponent.payload.ApiResult;

import java.util.List;
import java.util.UUID;

public interface AboutUsService {
    ApiResult<AboutUsResDTO> add(AboutUsReqDTO aboutUsReqDTO);

    ApiResult<AboutUsResDTO> getOne(UUID id);

    ApiResult<AboutUsResDTO> edit(AboutUsReqDTO aboutUsReqDTO, UUID id);

    ApiResult<?> delete(UUID id);

    ApiResult<List<AboutUsResDTO>> getAll();
}
