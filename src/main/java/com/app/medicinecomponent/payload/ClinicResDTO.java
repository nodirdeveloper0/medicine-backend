package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClinicResDTO {
    private UUID id;
    private String name;
    private Long clinicLegalCardNumber;
    private String phoneNumber;
    private AddressResDTO address;
    private Boolean active;
}
