package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CertificateReqDTO {

    @NotNull(message = "User shouldn't be empty!")
    private UUID toUserId;

    @NotNull(message = "Start date of certificate shouldn't be empty!")
    private Date fromDate;

    @NotNull(message = "Expire date of certificate shouldn't be empty!")
    private Date toDate;

//    @NotNull(message = "Qualification category of certificate shouldn't be empty!")
//    private QualificationCategory qualificationCategory;

//    private UUID jobTitleId;

    @NotNull(message = "Attestation shouldn't be empty")
    private UUID attestationId;
}
