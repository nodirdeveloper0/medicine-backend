package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.CertificateReqDTO;
import com.app.medicinecomponent.payload.CertificateResDTO;
import com.app.medicinecomponent.service.CertificateService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class CertificateControllerImpl implements CertificateController {
    private final CertificateService certificateService;

    public CertificateControllerImpl(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    @Override
    public ApiResult<CertificateResDTO> add(CertificateReqDTO certificateReqDTO) {
        return certificateService.add(certificateReqDTO);
    }

    @Override
    public ApiResult<CertificateResDTO> getOne(UUID id) {
        return certificateService.getOne(id);
    }

    @Override
    public ApiResult<List<CertificateResDTO>> getAll() {
        return certificateService.getAll();
    }

    @Override
    public ApiResult<List<CertificateResDTO>> getAllByUserId(UUID userId) {
        return certificateService.getAllByUserId(userId);
    }
}
