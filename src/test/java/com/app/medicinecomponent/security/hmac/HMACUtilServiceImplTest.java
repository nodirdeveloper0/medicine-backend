package com.app.medicinecomponent.security.hmac;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {HMACUtilServiceImpl.class})
@ExtendWith(SpringExtension.class)
class HMACUtilServiceImplTest {
    @Autowired
    private HMACUtilServiceImpl hMACUtilServiceImpl;

    @Test
    void testCalculateHASH() {
        assertEquals("Z6yLbpztDYKPzYjueFaOzBcIbuEq3GX8mzr4SZPHhRg=", this.hMACUtilServiceImpl.calculateHASH("42",
                "Timestamp", "Action", "EXAMPLEKEYwjalrXUtnFEMI/K7MDENG/bPxRfiCY"));
    }

    @Test
    void testHasAccess6() {
        assertFalse(this.hMACUtilServiceImpl.hasAccess("ACCOUNT_SERVICE", "42", "Action", "Signature"));
    }

    @Test
    void testHasAccess8() {
        assertTrue(this.hMACUtilServiceImpl.hasAccess("ACCOUNT_SERVICE", "42", "Action",
                "BBzKxE4UtFAKEXQt5Uj77hwU2ipTXqfHFJgD4/jbrgk="));
    }
}

