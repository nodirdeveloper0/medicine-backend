package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.JobTitleReqDTO;
import com.app.medicinecomponent.payload.JobTitleResDTO;
import com.app.medicinecomponent.service.JobTitleServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;

//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@WebMvcTest(JobTitleController.class)
//@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {JobTitleControllerImpl.class})
class JobTitleControllerImplTest {

    @Autowired
    private JobTitleControllerImpl jobTitleController;

//    @Autowired
//    private MockMvc mockMvc;

    @MockBean
    private JobTitleServiceImpl jobTitleService;

    @Test
    void add() throws Exception {
        JobTitleResDTO jobTitleResDTO = new JobTitleResDTO(UUID.randomUUID(), "DENTIST");
        JobTitleReqDTO jobTitleReqDTO = new JobTitleReqDTO("DENTIST");
        when(this.jobTitleService.add(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, jobTitleResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(jobTitleReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/job-title")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.jobTitleController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAll() throws Exception {
        JobTitleResDTO jobTitleResDTOFirst = new JobTitleResDTO(UUID.randomUUID(), "DENTIST");
        JobTitleResDTO jobTitleResDTOSecond = new JobTitleResDTO(UUID.randomUUID(), "LOR");
        JobTitleResDTO jobTitleResDTOThird = new JobTitleResDTO(UUID.randomUUID(), "OCULIST");
        List<JobTitleResDTO> jobTitleResDTOList = new ArrayList<>(Arrays.asList(jobTitleResDTOFirst, jobTitleResDTOSecond, jobTitleResDTOThird));

        when(this.jobTitleService.getAll()).thenReturn(new ApiResult<>(null, jobTitleResDTOList, null, true));

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/job-title");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.jobTitleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getOne() throws Exception {
        UUID uuid = UUID.randomUUID();
        JobTitleResDTO jobTitleResDTO = new JobTitleResDTO(uuid, "DENTIST");
        when(this.jobTitleService.getOne(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, jobTitleResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/job-title/{id}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.jobTitleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void edit() throws Exception {
        JobTitleResDTO jobTitleResDTO = new JobTitleResDTO(UUID.randomUUID(), "DENTIST");
        JobTitleReqDTO jobTitleReqDTO = new JobTitleReqDTO("DENTIST");

        when(this.jobTitleService.edit(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, jobTitleResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(jobTitleReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.put("/api/job-title/{id}", UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions =
                MockMvcBuilders.standaloneSetup(this.jobTitleController)
                        .build()
                        .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));

    }
}