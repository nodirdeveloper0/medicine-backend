package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.DiseaseInfoReqDTO;
import com.app.medicinecomponent.payload.DiseaseInfoResDto;

import java.util.List;
import java.util.UUID;

public interface DiseaseInfoService {

    ApiResult<DiseaseInfoResDto> add(DiseaseInfoReqDTO diseaseInfoReqDTO);

    ApiResult<DiseaseInfoResDto> getOne(UUID id);

    ApiResult<List<DiseaseInfoResDto>> getAll();
}
