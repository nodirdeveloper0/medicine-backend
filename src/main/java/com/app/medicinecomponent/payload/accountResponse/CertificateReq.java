package com.app.medicinecomponent.payload.accountResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CertificateReq {
    private String description;
    private Date expiredAt;
    private int cardNumber;
    private String certificateType;
    private String firstName;
    private String lastName;
}
