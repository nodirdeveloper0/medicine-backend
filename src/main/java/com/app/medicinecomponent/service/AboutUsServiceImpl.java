package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.AboutUs;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.AboutUsReqDTO;
import com.app.medicinecomponent.payload.AboutUsResDTO;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.repository.AboutUsRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AboutUsServiceImpl implements AboutUsService {
    private final AboutUsRepository aboutUsRepository;

    public AboutUsServiceImpl(AboutUsRepository aboutUsRepository) {
        this.aboutUsRepository = aboutUsRepository;
    }

    @Override
    public ApiResult<AboutUsResDTO> add(AboutUsReqDTO aboutUsReqDTO) {
        AboutUs aboutUs = new AboutUs(
                aboutUsReqDTO.getDescription()
        );
        aboutUsRepository.save(aboutUs);
        return ApiResult.successResponse(CustomMapper.aboutUsToDTO(aboutUs));
    }

    @Override
    public ApiResult<AboutUsResDTO> getOne(UUID id) {
        AboutUs aboutUs = aboutUsRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "About us is not found!"));
        return ApiResult.successResponse(CustomMapper.aboutUsToDTO(aboutUs));
    }

    @Override
    public ApiResult<List<AboutUsResDTO>> getAll() {
        List<AboutUs> aboutUsList = aboutUsRepository.findAll();
        return ApiResult.successResponse(CustomMapper.aboutUsListToDTO(aboutUsList));

    }

    @Override
    public ApiResult<AboutUsResDTO> edit(AboutUsReqDTO aboutUsReqDTO, UUID id) {
        AboutUs aboutUs = aboutUsRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "About us is not found!"));
        aboutUs.setDescription(aboutUsReqDTO.getDescription());
        aboutUsRepository.save(aboutUs);
        return ApiResult.successResponse(CustomMapper.aboutUsToDTO(aboutUs));
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        try {
            aboutUsRepository.deleteById(id);
            return ApiResult.successResponse("About us is successfully deleted");
        } catch (Exception e) {
            throw new RestException(HttpStatus.NOT_FOUND, "About us is not found!");
        }
    }
}
