package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Certificate;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface CertificateRepository extends JpaRepository<Certificate, UUID> {
    List<Certificate> findAllByToUserId(UUID toUser_id);

    @Query(value = "select count(*) from certificate",
            nativeQuery = true)
    Integer countAll();

    Optional<Certificate> findByToUserIdAndQualificationCategory(UUID toUser_id, QualificationCategory qualificationCategory);

    @Query(value = "select * from certificate where to_user_id=:userId order by from_date desc limit 1",nativeQuery = true)
    Optional<Certificate> findByFromDateAndToUserId(@Param("userId") UUID userId);
}
