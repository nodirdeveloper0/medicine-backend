package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpReqDTO {

    @NotNull(message = "Resident card number shouldn't be empty")
    private Long residentCardNumber;

//    private Integer workingExperienceInMonth;

    @NotBlank(message = "Password shouldn't be empty!")
    private String password;

//    @NotNull(message = "Qualification category shouldn't be empty!")
//    private QualificationCategory qualificationCategory;

    @NotNull(message = "Job title shouldn't be empty!")
    private UUID jobTitleId;

//    @NotNull(message = "Role shouldn't be empty!")
//    private UUID roleId;

    @NotNull(message = "Clinic shouldn't be empty")
    private UUID clinicId;

    private UUID attachmentId;
}
