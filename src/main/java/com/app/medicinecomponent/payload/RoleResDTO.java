package com.app.medicinecomponent.payload;

import com.app.medicinecomponent.entity.enums.PermissionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleResDTO {
    private UUID id;
    private String name;
    private Set<PermissionEnum> permissionEnumSet;

}
