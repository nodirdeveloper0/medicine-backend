package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Address;
import com.app.medicinecomponent.entity.Clinic;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration(classes = {ClinicRepository.class, AddressRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class ClinicRepositoryTest {
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private ClinicRepository clinicRepository;

    Clinic clinic = new Clinic();

    @BeforeEach
    void setUp() {
        Address address = new Address();
        address.setDeleted(false);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setStreet("Street");
        addressRepository.save(address);

        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setDeleted(false);
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinicRepository.save(clinic);
    }

    @AfterEach
    void tearDown() {
        addressRepository.deleteAll();
        clinicRepository.deleteAll();
    }

    @Test
    void testFindByClinicLegalCardNumber() {
        Optional<Clinic> optionalClinic = this.clinicRepository.findByClinicLegalCardNumber(1L);
        assertNotNull(this.clinicRepository.findByClinicLegalCardNumber(1L).get());
        optionalClinic.ifPresent(value -> assertEquals(value, clinic));
    }

    @Test
    void testExistByClinicCardNumber() {
        assertTrue(this.clinicRepository.existsByClinicLegalCardNumber(1L));
    }
}

