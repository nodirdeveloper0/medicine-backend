package com.app.medicinecomponent.utils;

public interface RestConstant {

    String BASE_PATH = "/api";
    String DOMAIN = "http://localhost";
    String AUTH_CONTROLLER = BASE_PATH + "/auth";
    String CLINIC_CONTROLLER = BASE_PATH + "/clinic";
    String JOB_TITLE_CONTROLLER = BASE_PATH + "/job-title";
    String ROLE_CONTROLLER = BASE_PATH + "/role";
    String ABOUT_US_CONTROLLER = BASE_PATH + "/about-us";
    String ATTESTATION_CONTROLLER = BASE_PATH + "/attestation";
    String REGISTRATION_CONTROLLER = BASE_PATH + "/registration";
    String CERTIFICATE_CONTROLLER = BASE_PATH + "/certificate";
    String USER_CONTROLLER = BASE_PATH + "/user";
    String DISEASE_INFO = BASE_PATH + "/disease-info";
    String ATTACHMENT_CONTROLLER = BASE_PATH + "/attachment";


    String DEFAULT_PAGE_NUMBER = "0";
    String DEFAULT_PAGE_SIZE = "20";

    String ADMIN = "ADMIN";
    String DOCTOR = "DOCTOR";
}
