package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.CertificateReqDTO;
import com.app.medicinecomponent.payload.CertificateResDTO;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.CERTIFICATE_CONTROLLER)
@Tag(name = "Certificate operations",description = "Certificate")
public interface CertificateController {

    @PreAuthorize(value = "hasAuthority('ADD_CERTIFICATE')")
    @Operation(summary = "Add certificate (admin)")
    @PostMapping
    ApiResult<CertificateResDTO> add(@RequestBody @Valid CertificateReqDTO certificateReqDTO);

    @Operation(summary = "Get one certificate (any user)")
    @GetMapping("/{id}")
    ApiResult<CertificateResDTO> getOne(@PathVariable UUID id);

    @Operation(summary = "Get all certificate (any user)")
    @GetMapping
    ApiResult<List<CertificateResDTO>> getAll();

    @Operation(summary = "Get all certificate by user id (any user)")
    @GetMapping("/certificates-of-one/{userId}")
    ApiResult<List<CertificateResDTO>> getAllByUserId(@PathVariable UUID userId);

}
