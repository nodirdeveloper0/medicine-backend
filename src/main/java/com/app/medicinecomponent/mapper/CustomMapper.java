package com.app.medicinecomponent.mapper;

import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.utils.CommonUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class CustomMapper {

    public static ClinicResDTO clinicToDTO(Clinic clinic) {
        return new ClinicResDTO(
                clinic.getId(),
                clinic.getName(),
                clinic.getClinicLegalCardNumber(),
                clinic.getPhoneNumber(),
                addressToDTO(clinic.getAddress()),
                clinic.getActive()
        );
    }

    public static AddressResDTO addressToDTO(Address address) {
        return new AddressResDTO(
                address.getId(),
                address.getHomeNumber(),
                address.getStreet(),
                address.getDistrict()
        );
    }

    public static JobTitleResDTO jobTitleToDTO(JobTitle jobTitle) {
        return new JobTitleResDTO(
                jobTitle.getId(),
                jobTitle.getName()
        );
    }

    public static RoleResDTO roleToDTO(Role role) {
        return new RoleResDTO(
                role.getId(),
                role.getName(),
                role.getPermissionEnums()
        );
    }

    public static List<RoleResDTO> roleListToDTO(List<Role> roleList) {
        List<RoleResDTO> roleResDTOS = new ArrayList<>();
        for (Role role : roleList) {
            roleResDTOS.add(roleToDTO(role));
        }
        return roleResDTOS;
    }

    public static AboutUsResDTO aboutUsToDTO(AboutUs aboutUs) {
        return new AboutUsResDTO(
                aboutUs.getId(),
                aboutUs.getDescription()
        );
    }

    public static AttestationResDTO attestationToDTO(Attestation attestation) {
        return new AttestationResDTO(
                attestation.getId(),
                attestation.getStartDate(),
                attestation.getEndDate(),
                attestation.getDescription(),
                attestation.getClosed()
        );
    }

    public static List<AttestationResDTO> attestationListToDTO(List<Attestation> attestationList) {
        List<AttestationResDTO> attestationResDTOS = new ArrayList<>();
        for (Attestation attestation : attestationList) {
            attestationResDTOS.add(attestationToDTO(attestation)
            );
        }
        return attestationResDTOS;
    }

    public static RegistrationResDTO registrationToDTO(Registration registration) {
        return new RegistrationResDTO(
                registration.getId(),
                registration.getResidentCardNumber(),
                attestationToDTO(registration.getAttestation())
        );
    }


    public static RegisteredUsersResDTO registeredUsersToDTO(List<User> userList, UUID id) {
        return new RegisteredUsersResDTO(
                userListToDTO(userList),
                id
        );
    }

    public static CertificateResDTO certificateToDTO(Certificate certificate) {
        return new CertificateResDTO(
                certificate.getId(),
                certificate.getSerialNumber(),
                userToResDTO(certificate.getToUser()),
                certificate.getFromDate(),
                certificate.getToDate(),
                certificate.getQualificationCategory(),
                jobTitleToDTO(certificate.getJobTitle()),
                attestationToDTO(certificate.getAttestation()),
                certificate.getCertificateTypeEnum(),
                certificate.getDescription()
        );
    }

    public static UserResDTO userToResDTO(User user) {
        return new UserResDTO(
                user.getId(),
                user.getFirstname(),
                user.getLastname(),
                user.getResidentCardNumber(),
                user.getWorkingExperienceInMonth(),
                user.getQualificationCategory(),
                jobTitleToDTO(user.getJobTitle()),
                user.getClinic() != null ? clinicToDTO(user.getClinic()) : null,
                roleToDTO(user.getRole()),
                user.isActive(),
                user.getAvatarImg()!=null? CommonUtils.buildPhotoUrl(user.getAvatarImg().getId()):null,
                user.getAvatarImg()!=null? user.getAvatarImg().getId():null
        );
    }

    public static List<CertificateResDTO> certificateListToDto(List<Certificate> certificateList) {
        List<CertificateResDTO> certificateResDTOS = new ArrayList<>();
        for (Certificate certificate : certificateList) {
            certificateResDTOS.add(new CertificateResDTO(
                    certificate.getId(),
                    certificate.getSerialNumber(),
                    userToResDTO(certificate.getToUser()),
                    certificate.getFromDate(),
                    certificate.getToDate(),
                    certificate.getQualificationCategory(),
                    jobTitleToDTO(certificate.getJobTitle()),
                    certificate.getAttestation()!=null?attestationToDTO(certificate.getAttestation()):null,
                    certificate.getCertificateTypeEnum(),
                    certificate.getDescription()
            ));
        }
        return certificateResDTOS;
    }


    public static List<JobTitleResDTO> jobTitleListToDTO(List<JobTitle> jobTitleList) {
        List<JobTitleResDTO> jobTitleResDTOS = new ArrayList<>();
        for (JobTitle jobTitle : jobTitleList) {
            jobTitleResDTOS.add(new JobTitleResDTO(
                    jobTitle.getId(),
                    jobTitle.getName()
            ));
        }
        return jobTitleResDTOS;
    }

    public static UserResDTOForClinic userToResDTOForClinic(User user) {
        return new UserResDTOForClinic(
                user.getId(),
                user.getFirstname(),
                user.getLastname(),
                user.getResidentCardNumber(),
                user.getWorkingExperienceInMonth(),
                user.getQualificationCategory(),
                user.getJobTitle().getId(),
                user.getClinic().getId(),
                user.getRole().getId(),
                user.isActive()
        );
    }

    public static List<ClinicResDTO> clinicListToDTO(List<Clinic> clinicList) {
        List<ClinicResDTO> clinicResDTOS = new ArrayList<>();
        for (Clinic clinic : clinicList) {
            clinicResDTOS.add(new ClinicResDTO(
                    clinic.getId(),
                    clinic.getName(),
                    clinic.getClinicLegalCardNumber(),
                    clinic.getPhoneNumber(),
                    addressToDTO(clinic.getAddress()),
                    clinic.getActive()
            ));
        }
        return clinicResDTOS;
    }

    public static List<AboutUsResDTO> aboutUsListToDTO(List<AboutUs> aboutUsList) {
        List<AboutUsResDTO> aboutUsResDTOS = new ArrayList<>();
        for (AboutUs aboutUs : aboutUsList) {
            aboutUsResDTOS.add(new AboutUsResDTO(
                    aboutUs.getId(),
                    aboutUs.getDescription()
            ));
        }
        return aboutUsResDTOS;
    }

    public static List<UserResDTO> userListToDTO(List<User> users) {
        List<UserResDTO> userResDTOS = new ArrayList<>();
        for (User user : users) {
            userResDTOS.add(userToResDTO(user));
        }
        return userResDTOS;
    }

    public static DiseaseInfoResDto diseaseInfoToResDTO(DiseaseInfo diseaseInfo) {
        return new DiseaseInfoResDto(
                diseaseInfo.getId(),
                diseaseInfo.getFirstName(),
                diseaseInfo.getLastName(),
                diseaseInfo.getCardNumber(),
                diseaseInfo.getDescription(),
                diseaseInfo.getStatus(),
                clinicToDTO(diseaseInfo.getClinic()),
                diseaseInfo.getReceivedTime(),
                diseaseInfo.getSentTime()
        );
    }

    public static List<DiseaseInfoResDto> diseaseInfoToResDTOList(List<DiseaseInfo> diseaseInfoList) {
        List<DiseaseInfoResDto> diseaseInfoResDtos = new ArrayList<>();
        for (DiseaseInfo diseaseInfo : diseaseInfoList) {
            diseaseInfoResDtos.add(diseaseInfoToResDTO(diseaseInfo));
        }
        return diseaseInfoResDtos;
    }
}
