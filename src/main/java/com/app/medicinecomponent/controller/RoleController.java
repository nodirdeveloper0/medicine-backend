package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RoleResDTO;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.ROLE_CONTROLLER)
@Tag(name = "Role operations", description = "Role")
public interface RoleController {

    @Operation(summary = "Get one role (any user)")
    @GetMapping("/{id}")
    ApiResult<RoleResDTO> getOne(@PathVariable UUID id);

    @Operation(summary = "Get all roles (any user)")
    @GetMapping
    ApiResult<List<RoleResDTO>> getAll();
}
