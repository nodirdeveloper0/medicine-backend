package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClinicReqDTO {

    @NotNull(message = "Clinic card number shouldn't be empty!")
    private Long clinicLegalCardNumber;

    @NotBlank(message = "PhoneNumber bo'sh bo'lmasligi kerak")
    @Pattern(regexp = "[+][9][9][8][0-9]{9}",message = "Telefon raqamining formatini xato kiritdingiz!")
    private String phoneNumber;
}
