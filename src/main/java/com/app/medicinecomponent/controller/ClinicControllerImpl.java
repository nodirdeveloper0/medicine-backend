package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.ClinicReqDTO;
import com.app.medicinecomponent.payload.ClinicResDTO;
import com.app.medicinecomponent.security.hmac.AuthorizeRequest;
import com.app.medicinecomponent.service.ClinicService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class ClinicControllerImpl implements ClinicController {

    private final ClinicService clinicService;

    public ClinicControllerImpl(ClinicService clinicService) {
        this.clinicService = clinicService;
    }

    @Override
    public ApiResult<ClinicResDTO> add(ClinicReqDTO clinicReqDTO) {
        return clinicService.add(clinicReqDTO);
    }

    @Override
    public ApiResult<ClinicResDTO> getOne(UUID id) {
        return clinicService.getOne(id);
    }

    @Override
    public ApiResult<List<ClinicResDTO>> getAll() {
        return clinicService.getAll();
    }

    @AuthorizeRequest
    @Override
    public ApiResult<ClinicResDTO> getOneByLegalCardNumber(Long cardNumber) {
        return clinicService.getOneByLegalCardNumber(cardNumber);
    }
}
