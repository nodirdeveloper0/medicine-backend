package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobTitleReqDTO {
    @NotBlank(message = "Name of Job title shouldn't be empty!")
    private String name;
}
