package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.AddressResDTO;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.ClinicReqDTO;
import com.app.medicinecomponent.payload.ClinicResDTO;
import com.app.medicinecomponent.service.ClinicServiceImpl;
import com.app.medicinecomponent.payload.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ClinicControllerImpl.class})
class ClinicControllerImplTest {

    @Autowired
    private ClinicControllerImpl clinicController;

    @MockBean
    private ClinicServiceImpl clinicService;

    ClinicResDTO clinicResDTO = new ClinicResDTO();
    ClinicReqDTO clinicReqDTO = new ClinicReqDTO();
    List<ClinicResDTO> clinicResDTOList =  new ArrayList<>();

    @BeforeEach
    void setUp() {
        AddressResDTO addressResDTO = new AddressResDTO();
        addressResDTO.setId(UUID.randomUUID());
        addressResDTO.setHomeNumber("12");
        addressResDTO.setStreet("Oybek");
        addressResDTO.setDistrict("Yunusobod");

        clinicResDTO.setId(UUID.randomUUID());
        clinicResDTO.setName("AKFA");
        clinicResDTO.setClinicLegalCardNumber(123L);
        clinicResDTO.setPhoneNumber("+998981234567");
        clinicResDTO.setAddress(addressResDTO);
        clinicResDTO.setActive(true);

        clinicResDTOList.add(clinicResDTO);

        clinicReqDTO.setClinicLegalCardNumber(123L);
        clinicReqDTO.setPhoneNumber("+998981234567");
    }

    @Test
    void add() throws Exception {
        when(this.clinicService.add(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, clinicResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(clinicReqDTO));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/clinic")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.clinicController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getOne() throws Exception {
        when(this.clinicService.getOne(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, clinicResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/clinic/{id}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.clinicController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAll() throws Exception {
        when(this.clinicService.getAll()).thenReturn(new ApiResult<>(null, clinicResDTOList, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/clinic");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.clinicController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getOneByLegalCardNumber() throws Exception {
        when(this.clinicService.getOneByLegalCardNumber(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, clinicResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/clinic/legal-card-number/{cardNumber}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.clinicController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));

    }
}