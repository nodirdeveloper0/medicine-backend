package com.app.medicinecomponent.payload.residentResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResidentInfoFromCityManagement {
    private Result result;
    private Boolean success;
}
