package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Attestation;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.AttestationReqDTO;
import com.app.medicinecomponent.payload.AttestationResDTO;
import com.app.medicinecomponent.repository.AttestationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {AttestationServiceImpl.class})
@ExtendWith(SpringExtension.class)
class AttestationServiceImplTest {
    @MockBean
    private AttestationRepository attestationRepository;

    @Autowired
    private AttestationServiceImpl attestationServiceImpl;

    Attestation attestation = new Attestation();

    @BeforeEach
    void setUp() {
        attestation.setClosed(true);
        attestation.setCreatedAt(mock(Timestamp.class));
        attestation.setCreatedBy(UUID.randomUUID());
        attestation.setDeleted(true);
        attestation.setDescription("The characteristics of someone or something");
        attestation.setEndDate(mock(Date.class));
        attestation.setId(UUID.randomUUID());
        attestation.setStartDate(mock(Date.class));
        attestation.setUpdatedAt(mock(Timestamp.class));
        attestation.setUpdatedBy(UUID.randomUUID());
    }

    @Test
    void testAdd() {
        Attestation attestation = new Attestation();
        attestation.setClosed(true);
        attestation.setCreatedAt(mock(Timestamp.class));
        attestation.setCreatedBy(UUID.randomUUID());
        attestation.setDeleted(true);
        attestation.setDescription("The characteristics of someone or something");
        attestation.setEndDate(mock(Date.class));
        attestation.setId(UUID.randomUUID());
        attestation.setStartDate(mock(Date.class));
        attestation.setUpdatedAt(mock(Timestamp.class));
        attestation.setUpdatedBy(UUID.randomUUID());

        when(this.attestationRepository.save((Attestation) any())).thenReturn(attestation);
        when(this.attestationRepository.findAllByExistsDate((Date) any(), (Date) any())).thenReturn(new ArrayList<>());
        ApiResult<AttestationResDTO> actualAddResult = this.attestationServiceImpl.add(new AttestationReqDTO());
        assertTrue(actualAddResult.isSuccess());
        assertNull(actualAddResult.getErrors());
        assertNull(actualAddResult.getMessage());
        AttestationResDTO data = actualAddResult.getData();
        assertNull(data.getEndDate());
        assertNull(data.getDescription());
        assertFalse(data.getClosed());
        assertNull(data.getId());
        assertNull(data.getStartDate());
        verify(this.attestationRepository).save((Attestation) any());
        verify(this.attestationRepository).findAllByExistsDate((Date) any(), (Date) any());
    }

    @Test
    void testEdit2() {
        Date date = mock(Date.class);
        when(date.after((Date) any())).thenReturn(true);
        Optional<Attestation> ofResult = Optional.of(attestation);
        when(this.attestationRepository.save((Attestation) any())).thenReturn(attestation);
        when(this.attestationRepository.findAllByExistsDateIdNot((UUID) any(), (Date) any(),
                (Date) any())).thenReturn(new ArrayList<>());
        when(this.attestationRepository.findByIdAndClosedFalse((UUID) any())).thenReturn(ofResult);
        Date date1 = mock(Date.class);
        when(date1.after((Date) any())).thenReturn(true);
        AttestationReqDTO attestationReqDTO = new AttestationReqDTO(date1, mock(Date.class),
                "The characteristics of someone or something");
        assertThrows(RestException.class, () -> this.attestationServiceImpl.edit(attestationReqDTO, UUID.randomUUID()));
        verify(date1).after((Date) any());
    }

    @Test
    void testEdit4() {
        Date date = mock(Date.class);
        when(date.after((Date) any())).thenReturn(true);
        UUID randomUUIDResult = UUID.randomUUID();
        attestation.setId(randomUUIDResult);
        Optional<Attestation> ofResult = Optional.of(attestation);

        when(this.attestationRepository.save((Attestation) any())).thenReturn(attestation);
        when(this.attestationRepository.findAllByExistsDateIdNot((UUID) any(), (Date) any(),
                (Date) any())).thenReturn(new ArrayList<>());
        when(this.attestationRepository.findByIdAndClosedFalse((UUID) any())).thenReturn(ofResult);
        Date date1 = mock(Date.class);
        when(date1.after((Date) any())).thenReturn(false);
        AttestationReqDTO attestationReqDTO = new AttestationReqDTO(date1, mock(Date.class),
                "The characteristics of someone or something");

        ApiResult<AttestationResDTO> actualEditResult = this.attestationServiceImpl.edit(attestationReqDTO,
                UUID.randomUUID());
        assertTrue(actualEditResult.isSuccess());
        assertNull(actualEditResult.getErrors());
        assertNull(actualEditResult.getMessage());
        AttestationResDTO data = actualEditResult.getData();
        assertEquals("The characteristics of someone or something", data.getDescription());
        assertTrue(data.getClosed());
        assertSame(randomUUIDResult, data.getId());
        verify(this.attestationRepository).save((Attestation) any());
        verify(this.attestationRepository).findAllByExistsDateIdNot((UUID) any(), (Date) any(),
                (Date) any());
        verify(this.attestationRepository).findByIdAndClosedFalse((UUID) any());
//        verify(date).after((java.sql.Date) any());
        verify(date1).after((Date) any());
    }

    @Test
    void testEdit01(){
        Date date1 = mock(Date.class);
        when(date1.after((Date) any())).thenReturn(false);

        when(this.attestationRepository.findByIdAndClosedFalse((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.attestationServiceImpl.edit(new AttestationReqDTO(date1, mock(Date.class),
                "The characteristics of someone or something"),UUID.randomUUID()));
        verify(this.attestationRepository).findByIdAndClosedFalse((UUID) any());
    }

    @Test
    void testGetOne() {
        UUID randomUUIDResult = UUID.randomUUID();
        attestation.setId(randomUUIDResult);
        Optional<Attestation> ofResult = Optional.of(attestation);
        when(this.attestationRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<AttestationResDTO> actualOne = this.attestationServiceImpl.getOne(UUID.randomUUID());
        assertTrue(actualOne.isSuccess());
        assertNull(actualOne.getErrors());
        assertNull(actualOne.getMessage());
        AttestationResDTO data = actualOne.getData();
        assertEquals("The characteristics of someone or something", data.getDescription());
        assertTrue(data.getClosed());
        assertSame(randomUUIDResult, data.getId());
        verify(this.attestationRepository).findById((UUID) any());
    }

    @Test
    void testGetOne3() {
        when(this.attestationRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.attestationServiceImpl.getOne(UUID.randomUUID()));
        verify(this.attestationRepository).findById((UUID) any());
    }

    @Test
    void testGetAll2() {
        UUID randomUUIDResult = UUID.randomUUID();
        attestation.setId(randomUUIDResult);
        ArrayList<Attestation> attestationList = new ArrayList<>();
        attestationList.add(attestation);
        when(this.attestationRepository.findAll()).thenReturn(attestationList);
        ApiResult<List<AttestationResDTO>> actualAll = this.attestationServiceImpl.getAll();
        List<AttestationResDTO> data = actualAll.getData();
        assertEquals(1, data.size());
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getErrors());
        assertNull(actualAll.getMessage());
        AttestationResDTO getResult = data.get(0);
        assertTrue(getResult.getClosed());
        assertSame(randomUUIDResult, getResult.getId());
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        verify(this.attestationRepository).findAll();
    }

    @Test
    void testGetAllCloseFalse2() {
        UUID randomUUIDResult = UUID.randomUUID();
        attestation.setId(randomUUIDResult);
        attestation.setClosed(false);
        ArrayList<Attestation> attestationList = new ArrayList<>();
        attestationList.add(attestation);
        when(this.attestationRepository.findAllByClosedFalse()).thenReturn(attestationList);
        ApiResult<List<AttestationResDTO>> actualAllCloseFalse = this.attestationServiceImpl.getAllCloseFalse();
        List<AttestationResDTO> data = actualAllCloseFalse.getData();
        assertEquals(1, data.size());
        assertTrue(actualAllCloseFalse.isSuccess());
        assertNull(actualAllCloseFalse.getErrors());
        assertNull(actualAllCloseFalse.getMessage());
        AttestationResDTO getResult = data.get(0);
        assertFalse(getResult.getClosed());
        assertSame(randomUUIDResult, getResult.getId());
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        verify(this.attestationRepository).findAllByClosedFalse();
    }

    @Test
    void testValidateDateOrThrow() {
        Date date = mock(Date.class);
        when(date.after((Date) any())).thenReturn(true);
        assertThrows(RestException.class, () -> AttestationServiceImpl.validateDateOrThrow((Date) date));
        verify(date).after((Date) any());
    }

    @Test
    void testValidateDateOrThrow2() {
        Date date = mock(Date.class);
        when(date.after((Date) any())).thenReturn(false);
        AttestationServiceImpl.validateDateOrThrow((Date) date);
        verify(date).after((Date) any());
    }

    @Test
    void testValidateConflictDatesOrThrowExistInAdd2() {
        ArrayList<Attestation> attestationList = new ArrayList<>();
        attestationList.add(attestation);
        when(this.attestationRepository.findAllByExistsDate((Date) any(), (Date) any())).thenReturn(attestationList);
        assertThrows(RestException.class,
                () -> this.attestationServiceImpl.validateConflictDatesOrThrowExistInAdd(mock(Date.class), mock(Date.class)));
        verify(this.attestationRepository).findAllByExistsDate((Date) any(), (Date) any());
    }

    @Test
    void testValidateConflictDatesOrThrowExistInAdd4() {
        ArrayList<Attestation> attestationList = new ArrayList<>();
        attestationList.add(attestation);
        when(this.attestationRepository.findAllByExistsDate((Date) any(), (Date) any())).thenReturn(attestationList);
        assertThrows(RestException.class,
                () -> this.attestationServiceImpl.validateConflictDatesOrThrowExistInAdd(mock(Date.class), mock(Date.class)));
        verify(this.attestationRepository).findAllByExistsDate((Date) any(), (Date) any());
    }

    @Test
    void testValidateConflictDatesOrThrowExistInEdit4() {
        ArrayList<Attestation> attestationList = new ArrayList<>();
        attestationList.add(attestation);
        when(this.attestationRepository.findAllByExistsDateIdNot((UUID) any(), (Date) any(), (Date) any()))
                .thenReturn(attestationList);
        assertThrows(RestException.class, () -> this.attestationServiceImpl
                .validateConflictDatesOrThrowExistInEdit(UUID.randomUUID(), mock(Date.class), mock(Date.class)));
        verify(this.attestationRepository).findAllByExistsDateIdNot((UUID) any(), (Date) any(), (Date) any());
    }
}

