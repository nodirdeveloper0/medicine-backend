package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisteredUsersResDTO {
    private List<UserResDTO> userList;
    private UUID attestationId;
}
