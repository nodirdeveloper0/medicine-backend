package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.AboutUsReqDTO;
import com.app.medicinecomponent.payload.AboutUsResDTO;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.ABOUT_US_CONTROLLER)
@Tag(name = "About us operations", description = "About Us")
public interface AboutUsController {

    @PreAuthorize(value = "hasAuthority('ADD_ABOUT_US')")
    @Operation(summary = "Add aboutUs (admin)")
    @PostMapping
    ApiResult<AboutUsResDTO> add(@RequestBody @Valid AboutUsReqDTO aboutUsReqDTO);

    @Operation(summary = "Get one aboutUs (any user)")
    @GetMapping("/{id}")
    ApiResult<AboutUsResDTO> getOne(@PathVariable UUID id);

    @Operation(summary = "Get all aboutUs (any user)")
    @GetMapping
    ApiResult<List<AboutUsResDTO>> getAll();

    @PreAuthorize(value = "hasAuthority('EDIT_ABOUT_US')")
    @Operation(summary = "Edit aboutUs (admin)")
    @PutMapping("/{id}")
    ApiResult<AboutUsResDTO> edit(@RequestBody @Valid AboutUsReqDTO aboutUsReqDTO,
                                  @PathVariable UUID id);

    @PreAuthorize(value = "hasAuthority('DELETE_ABOUT_US')")
    @Operation(summary = "Delete aboutUs (admin)")
    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);

}
