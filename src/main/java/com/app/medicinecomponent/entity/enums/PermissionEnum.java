package com.app.medicinecomponent.entity.enums;

public enum PermissionEnum {


    ADD_ABOUT_US,
    EDIT_ABOUT_US,
    DELETE_ABOUT_US,

    ADD_ATTESTATION,
    EDIT_ATTESTATION,

    ADD_CERTIFICATE,

    ADD_CLINIC,

    VIEW_DISEASE_INFO,

    ADD_JOB_TITLE,
    EDIT_JOB_TITLE,

    REQUEST_REGISTRATION,
    UPLOAD_ATTACHMENT,

    DELETE_USER
}
