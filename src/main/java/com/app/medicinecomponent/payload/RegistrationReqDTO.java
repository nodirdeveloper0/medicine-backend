package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationReqDTO {

    @NotNull(message = "Resident card number shouldn't be empty!")
    private Long residentCardNumber;

    @NotNull(message = "Attestation shouldn't be empty!")
    private UUID attestationId;
}
