package com.app.medicinecomponent.entity.enums;

public enum DiseaseInfoStatusEnums {
    NEW,
    PENDING,
    SENT
}
