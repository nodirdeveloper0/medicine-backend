package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.CustomPage;
import com.app.medicinecomponent.payload.UserResDTO;
import com.app.medicinecomponent.security.hmac.AuthorizeRequest;
import com.app.medicinecomponent.service.UserService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class UserControllerImpl implements UserController{
    private final UserService userService;

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ApiResult<UserResDTO> getOne(UUID id) {
        return userService.getOne(id);
    }

    @Override
    public ApiResult<CustomPage<UserResDTO>> getAll(int page, int size) {
        return userService.getAll(page,size);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return userService.delete(id);
    }

    @Override
    public ApiResult<List<UserResDTO>> getAllByClinicId(UUID clinicId) {
        return userService.getAllByClinicId(clinicId);
    }

    @AuthorizeRequest
    @Override
    public ApiResult<UserResDTO> getOneByCardNumber(Long cardNumber) {
        return userService.getOneByCardNumber(cardNumber);
    }

    @Override
    public ApiResult<List<UserResDTO>> getAllByClinicCardNumber(Long clinicCardNumber) {
        return userService.getAllByClinicCardNumber(clinicCardNumber);
    }

    @AuthorizeRequest
    @Override
    public ApiResult<?> activateUser(Long cardNumber) {
        return userService.activateUser(cardNumber);
    }

    @AuthorizeRequest
    @Override
    public ApiResult<?> deactivateUser(Long cardNumber) {
        return userService.deactivateUser(cardNumber);
    }
}
