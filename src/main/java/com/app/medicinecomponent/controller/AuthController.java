package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.utils.RestConstant;
import com.app.medicinecomponent.payload.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequestMapping(RestConstant.AUTH_CONTROLLER)
@Tag(name = "Auth operations", description = "Authentication")
public interface AuthController {

    @Operation(summary = "Sign up process (who want to be doctor)")
    @PostMapping("/sign-up")
    ApiResult<UserResDTO> signUp(@RequestBody @Valid SignUpReqDTO signUpReqDTO);

    @Operation(summary = "Sign in process (admin , doctor)")
    @PostMapping("/login")
    ApiResult<TokenResDto> signIn(@RequestBody @Valid SignInReqDTO signInReqDTO);

    @Operation(summary = "Analysis user")
    @PostMapping("/analysis-user")
    ApiResult<?> analysisUser(@RequestBody @Valid AnalysisUserReqDTO analysisUserReqDTO);

}
