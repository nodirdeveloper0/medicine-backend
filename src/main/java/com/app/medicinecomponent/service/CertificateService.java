package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.CertificateReqDTO;
import com.app.medicinecomponent.payload.CertificateResDTO;

import java.util.List;
import java.util.UUID;

public interface CertificateService {
    ApiResult<CertificateResDTO> add(CertificateReqDTO certificateReqDTO);

    ApiResult<CertificateResDTO> getOne(UUID id);

    ApiResult<List<CertificateResDTO>> getAll();

    ApiResult<List<CertificateResDTO>> getAllByUserId(UUID userId);
}
