package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Certificate;
import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.entity.enums.CertificateTypeEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.CertificateReqDTO;
import com.app.medicinecomponent.payload.CertificateResDTO;
import com.app.medicinecomponent.payload.accountResponse.CertificateReq;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeReq;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeResponseFromAccountService;
import com.app.medicinecomponent.repository.*;
import com.app.medicinecomponent.repository.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CertificateServiceImpl implements CertificateService {
    private final CertificateRepository certificateRepository;
    private final UserRepository userRepository;
    private final JobTitleRepository jobTitleRepository;
    private final AttestationRepository attestationRepository;
    private final RegistrationRepository registrationRepository;
    private final ExternalAPIsService externalAPIsService;

    public CertificateServiceImpl(CertificateRepository certificateRepository, UserRepository userRepository, JobTitleRepository jobTitleRepository, AttestationRepository attestationRepository, RegistrationRepository registrationRepository, ExternalAPIsService externalAPIsService) {
        this.certificateRepository = certificateRepository;
        this.userRepository = userRepository;
        this.jobTitleRepository = jobTitleRepository;
        this.attestationRepository = attestationRepository;
        this.registrationRepository = registrationRepository;
        this.externalAPIsService = externalAPIsService;
    }

    @Override
    public ApiResult<CertificateResDTO> add(CertificateReqDTO certificateReqDTO) {

        /* The main purpose of checking dates{ start date should be after now and there should be sometime in two dates*/
        if (!(certificateReqDTO.getFromDate().after(new Date()) && certificateReqDTO.getToDate().after(certificateReqDTO.getFromDate())))
            throw new RestException(HttpStatus.BAD_REQUEST, "Expire dates are incorrect entered");

        /* check if there is a user to whom the certificate is issued */
        User toUser = userRepository.findById(certificateReqDTO.getToUserId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "User is not found"));

        /* The main purpose of checking category is saving process of qualificationCategory should be saved automatically without any wrong selection from admin side*/
        QualificationCategory qualificationCategory = defineQualificationCategory(toUser);

        /* Description of certificate depends on user's qualificationCategory */
        String description = defineDescriptionByQualificationCategory(toUser);

        /* ? */
        Optional<Certificate> lastCertificate = certificateRepository.findByFromDateAndToUserId(toUser.getId());

        Certificate certificate2 = certificateRepository.findByFromDateAndToUserId(toUser.getId()).orElseThrow(() -> new RestException(HttpStatus.CONFLICT, "This user should be register first!"));
//        if (!lastCertificate.isPresent())
//            throw new RestException(HttpStatus.CONFLICT, "This user should be register first!");

        Certificate certificate1 = lastCertificate.get();
        java.sql.Date fromDate = certificate1.getFromDate();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(fromDate);
        calendar.add(Calendar.YEAR, 4);
        java.sql.Date resultDate = new java.sql.Date(calendar.getTimeInMillis());

//        if (!resultDate.equals(new java.sql.Date(System.currentTimeMillis())) || !resultDate.before(new Date(System.currentTimeMillis())))
//            throw new RestException(HttpStatus.BAD_REQUEST, "Certificate already given this user");

//        Optional<Certificate> existsCertificate = certificateRepository.findByToUserIdAndQualificationCategory(toUser.getId(), qualificationCategory);
//        if (!existsCertificate.isPresent())
//            throw new RestException(HttpStatus.BAD_REQUEST, "This type Certificate already given to this user");

        /* Here incrementing serial number by plus 1 to all counted certificates number*/
        Integer count = certificateRepository.countAll();

        Certificate certificate = new Certificate(
                count + 1,
                toUser,
                certificateReqDTO.getFromDate(),
                certificateReqDTO.getToDate(),
                qualificationCategory,
                jobTitleRepository.findById(toUser.getJobTitle().getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND,"Job title is not found")),
                attestationRepository.findById(certificateReqDTO.getAttestationId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Attestation is not found!")),
                CertificateTypeEnum.CERTIFICATE_OF_QUALIFICATION,
                description
        );
        certificateRepository.save(certificate);
        toUser.setQualificationCategory(qualificationCategory);
        userRepository.save(toUser);

        /* At first certificateType will be created,send to account service and return certificate id*/
        String certificateTypeId = createCertificateType(certificate);
        if (certificateTypeId==null)
            throw new RestException(HttpStatus.SERVICE_UNAVAILABLE,"Error from Account Service");

        /* Second certificate will be created, send to account service */
        createCertificate(certificate, toUser, certificateTypeId);
        return ApiResult.successResponse(CustomMapper.certificateToDTO(certificate));
    }

    @Override
    public ApiResult<CertificateResDTO> getOne(UUID id) {
        Certificate certificate = certificateRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Certificate is not found!"));
        return ApiResult.successResponse(CustomMapper.certificateToDTO(certificate));
    }

    @Override
    public ApiResult<List<CertificateResDTO>> getAll() {
        List<Certificate> certificateList = certificateRepository.findAll();
        return ApiResult.successResponse(CustomMapper.certificateListToDto(certificateList));
    }

    @Override
    public ApiResult<List<CertificateResDTO>> getAllByUserId(UUID userId) {
        List<Certificate> allByToUserId = certificateRepository.findAllByToUserId(userId);
        return ApiResult.successResponse(CustomMapper.certificateListToDto(allByToUserId));
    }

    public static QualificationCategory defineQualificationCategory(User user) {
        if (user.getQualificationCategory().equals(QualificationCategory.SPECIALIST)) {
            return QualificationCategory.CATEGORY_1;
        } else if (user.getQualificationCategory().equals(QualificationCategory.CATEGORY_1)) {
            return QualificationCategory.CATEGORY_2;
        } else {
            return QualificationCategory.CATEGORY_3;
        }
    }

    public static String defineDescriptionByQualificationCategory(User user) {
        if (user.getQualificationCategory().equals(QualificationCategory.SPECIALIST)) {
            return "This certificate was issued to confirm that " + user.getFirstname().toUpperCase() + " " + user.getLastname().toUpperCase() + "'s qualification level had risen to the " + "CATEGORY_1";
        } else if (user.getQualificationCategory().equals(QualificationCategory.CATEGORY_1)) {
            return "This certificate was issued to confirm that " + user.getFirstname().toUpperCase() + " " + user.getLastname().toUpperCase() + "'s qualification level had risen to the " + "CATEGORY_2";
        } else {
            return "This certificate was issued to confirm that " + user.getFirstname().toUpperCase() + " " + user.getLastname().toUpperCase() + "'s qualification level had risen to the " + "CATEGORY_3";
        }
    }

    public String createCertificateType(Certificate certificate) {
        CertificateTypeResponseFromAccountService response = externalAPIsService.saveCertificateType(new CertificateTypeReq(
                certificate.getCertificateTypeEnum().getName(),
                certificate.getDescription(),
                "MEDICINE"
        ));
        int beginningOfCertificateTypeId = response.getRoot().get_links().getSelf().getHref().lastIndexOf("/");
        return response.getRoot().get_links().getSelf().getHref().substring(beginningOfCertificateTypeId + 1);
    }

    public void createCertificate(Certificate certificate, User user, String certificateTypeId) {
        externalAPIsService.saveCertificate(new CertificateReq(
                certificate.getDescription(),
                certificate.getToDate(),
                Math.toIntExact(user.getResidentCardNumber()),
                "certificateType/" + certificateTypeId,
                user.getFirstname(),
                user.getLastname()
        ));
    }
}
