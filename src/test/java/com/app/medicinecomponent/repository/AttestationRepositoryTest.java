package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Attestation;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ContextConfiguration(classes = {AttestationRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class AttestationRepositoryTest {
    @Autowired
    private AttestationRepository attestationRepository;

    Attestation attestation = new Attestation();

    @BeforeEach
    void setUp() {

        attestation.setClosed(false);
        attestation.setDeleted(false);
        attestation.setDescription("The characteristics of someone or something");
        attestation.setEndDate(mock(Date.class));
        attestation.setStartDate(mock(Date.class));
        attestationRepository.save(attestation);
    }

    @AfterEach
    void tearDown() {
        attestationRepository.deleteAll();
    }

    @Test
    void testFindByIdAndClosedFalse() {
        Optional<Attestation> byIdAndClosedFalse = this.attestationRepository.findByIdAndClosedFalse(attestation.getId());
        byIdAndClosedFalse.ifPresent(value -> assertEquals(value, attestation));
    }

    @Test
    void testFindAllByClosedFalse() {
        List<Attestation> allByClosedFalse = this.attestationRepository.findAllByClosedFalse();
        assertNotNull(allByClosedFalse);
        assertEquals(allByClosedFalse.get(0), attestation);
    }


}

