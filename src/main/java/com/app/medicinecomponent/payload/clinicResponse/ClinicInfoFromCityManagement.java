package com.app.medicinecomponent.payload.clinicResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClinicInfoFromCityManagement {
    private Result result;
    private Boolean success;
}
