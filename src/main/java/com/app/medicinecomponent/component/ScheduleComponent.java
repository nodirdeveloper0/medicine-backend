package com.app.medicinecomponent.component;

import com.app.medicinecomponent.entity.Attestation;
import com.app.medicinecomponent.entity.DiseaseInfo;
import com.app.medicinecomponent.entity.Role;
import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.entity.enums.DiseaseInfoStatusEnums;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.repository.*;
import com.app.medicinecomponent.utils.RestConstant;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@EnableScheduling
@Component
public class ScheduleComponent {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AttestationRepository attestationRepository;
    private final DiseaseInfoRepository diseaseInfoRepository;
    private final ClinicRepository clinicRepository;

    public ScheduleComponent(UserRepository userRepository, RoleRepository roleRepository, AttestationRepository attestationRepository, DiseaseInfoRepository diseaseInfoRepository, ClinicRepository clinicRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.attestationRepository = attestationRepository;
        this.diseaseInfoRepository = diseaseInfoRepository;
        this.clinicRepository = clinicRepository;
    }

    //    @Value(value = "${clinicServiceDiseaseInfoPath}")
    private final String clinicServiceDiseaseInfoPath = null;

    /**
     * This schedule method for adding one month experience to doctor's working experience in every month
     */
//     @Scheduled(cron = "*/10*****")
//     @Scheduled(cron = "@monthly")
    @Scheduled(cron = "0 0 0 1 * *")
    private void addingExperience() {
        Role role = roleRepository.findByName(RestConstant.DOCTOR).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Role topilmadi"));
        List<User> users = userRepository.findAllByRole(role);
        for (User user : users) {
            user.setWorkingExperienceInMonth(user.getWorkingExperienceInMonth() + 1);
        }
        userRepository.saveAll(users);
    }

    /**
     * This schedule will change status of attestation automatically in given endDate
     */

//    @Scheduled(cron = "0 * * * * *")
//    @Scheduled(cron = "0 0 1 * * *")
    @Scheduled(cron = "@midnight")
    private void calculatingAttestationExpireTime() {
        List<Attestation> attestationList = attestationRepository.findAllByClosedFalse();
        for (Attestation attestation : attestationList) {
            if (attestation.getEndDate().getTime() < new Date().getTime()) {
                attestation.setClosed(true);
            }
        }
        attestationRepository.saveAll(attestationList);
    }

    @Scheduled(fixedRate = 180000) //3 minutes
    private void sendNewDiseaseInfoToClinic() {

        List<DiseaseInfo> allByStatusNew = diseaseInfoRepository.findAllByStatusNew();
        for (DiseaseInfo diseaseInfo : allByStatusNew) {
            if (clinicServiceDiseaseInfoPath != null) {
                /* here i should send to clinic module */
                diseaseInfo.setSentTime(new Timestamp(System.currentTimeMillis()));
                diseaseInfo.setStatus(DiseaseInfoStatusEnums.SENT);
            } else {
                diseaseInfo.setStatus(DiseaseInfoStatusEnums.PENDING);
            }
        }
        diseaseInfoRepository.saveAll(allByStatusNew);
    }
}
