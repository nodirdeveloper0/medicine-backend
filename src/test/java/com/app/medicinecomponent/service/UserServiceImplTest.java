package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.enums.PermissionEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.repository.ClinicRepository;
import com.app.medicinecomponent.repository.UserRepository;
import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.Address;
import com.app.medicinecomponent.entity.Attachment;
import com.app.medicinecomponent.entity.Clinic;
import com.app.medicinecomponent.entity.JobTitle;
import com.app.medicinecomponent.entity.Role;
import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.ClinicResDTO;
import com.app.medicinecomponent.payload.JobTitleResDTO;
import com.app.medicinecomponent.payload.RoleResDTO;
import com.app.medicinecomponent.payload.UserResDTO;

import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {UserServiceImpl.class})
@ExtendWith(SpringExtension.class)
class UserServiceImplTest {
    @MockBean
    private ClinicRepository clinicRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    Attachment attachment = new Attachment();
    Address address = new Address();
    Clinic clinic = new Clinic();
    JobTitle jobTitle = new JobTitle();
    Role role = new Role();
    User user = new User();


    @BeforeEach
    void setUp() {
        attachment.setContentType("text/plain");
        attachment.setCreatedAt(mock(Timestamp.class));
        attachment.setCreatedBy(UUID.randomUUID());
        attachment.setDeleted(true);
        attachment.setId(UUID.randomUUID());
        attachment.setName("Name");
        attachment.setSize(3L);
        attachment.setUpdatedAt(mock(Timestamp.class));
        attachment.setUpdatedBy(UUID.randomUUID());

        address.setCreatedAt(mock(Timestamp.class));
        address.setCreatedBy(UUID.randomUUID());
        address.setDeleted(true);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setId(UUID.randomUUID());
        address.setStreet("Street");
        address.setUpdatedAt(mock(Timestamp.class));
        address.setUpdatedBy(UUID.randomUUID());

        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setCreatedAt(mock(Timestamp.class));
        clinic.setCreatedBy(UUID.randomUUID());
        clinic.setDeleted(true);
        clinic.setId(UUID.randomUUID());
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinic.setUpdatedAt(mock(Timestamp.class));
        clinic.setUpdatedBy(UUID.randomUUID());

        jobTitle.setCreatedAt(mock(Timestamp.class));
        jobTitle.setCreatedBy(UUID.randomUUID());
        jobTitle.setDeleted(true);
        jobTitle.setId(UUID.randomUUID());
        jobTitle.setName("Name");
        jobTitle.setUpdatedAt(mock(Timestamp.class));
        jobTitle.setUpdatedBy(UUID.randomUUID());

        role.setCreatedAt(mock(Timestamp.class));
        role.setCreatedBy(UUID.randomUUID());
        role.setDeleted(true);
        role.setId(UUID.randomUUID());
        role.setName("Name");
        role.setPermissionEnums(new HashSet<>());
        role.setUpdatedAt(mock(Timestamp.class));
        role.setUpdatedBy(UUID.randomUUID());

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setActive(true);
        user.setAvatarImg(attachment);
        user.setClinic(clinic);
        user.setCreatedAt(mock(Timestamp.class));
        user.setCreatedBy(UUID.randomUUID());
        user.setCredentialsNonExpired(true);
        user.setDeleted(true);
        user.setEnabled(true);
        user.setFirstname("Jane");
        user.setId(UUID.randomUUID());
        user.setJobTitle(jobTitle);
        user.setLastname("Doe");
        user.setPassword("iloveyou");
        user.setQualificationCategory(QualificationCategory.SPECIALIST);
        user.setResidentCardNumber(1L);
        user.setRole(role);
        user.setUpdatedAt(mock(Timestamp.class));
        user.setUpdatedBy(UUID.randomUUID());
        user.setWorkingExperienceInMonth(1);
    }

    @Test
    void testGetOne() {
        UUID randomUUIDResult = UUID.randomUUID();
        attachment.setId(randomUUIDResult);
        UUID randomUUIDResult1 = UUID.randomUUID();
        clinic.setId(randomUUIDResult1);
        UUID randomUUIDResult2 = UUID.randomUUID();
        jobTitle.setId(randomUUIDResult2);
        UUID randomUUIDResult3 = UUID.randomUUID();
        role.setId(randomUUIDResult3);
        HashSet<PermissionEnum> permissionEnumSet = new HashSet<>();
        role.setPermissionEnums(permissionEnumSet);

        UUID randomUUIDResult4 = UUID.randomUUID();
        user.setId(randomUUIDResult4);

        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<UserResDTO> actualOne = this.userServiceImpl.getOne(UUID.randomUUID());
        assertTrue(actualOne.isSuccess());
        assertNull(actualOne.getErrors());
        assertNull(actualOne.getMessage());
        UserResDTO data = actualOne.getData();
        assertEquals(1L, data.getResidentCardNumber().longValue());
        assertTrue(data.isActive());
        assertSame(randomUUIDResult, data.getAvatarImgId());
        assertSame(randomUUIDResult4, data.getId());
        assertEquals("Doe", data.getLastname());
        Assertions.assertEquals(QualificationCategory.SPECIALIST, data.getQualificationCategory());
        assertEquals(1, data.getWorkingExperienceInMonth().intValue());
        assertEquals("Jane", data.getFirstname());
        ClinicResDTO clinic1 = data.getClinic();
        assertEquals("4105551212", clinic1.getPhoneNumber());
        assertEquals("Name", clinic1.getName());
        assertSame(randomUUIDResult1, clinic1.getId());
        assertEquals(1L, clinic1.getClinicLegalCardNumber().longValue());
        JobTitleResDTO jobTitle1 = data.getJobTitle();
        assertSame(randomUUIDResult2, jobTitle1.getId());
        RoleResDTO role1 = data.getRole();
        assertSame(randomUUIDResult3, role1.getId());
        assertSame(permissionEnumSet, role1.getPermissionEnumSet());
        assertEquals("Name", role1.getName());
        assertEquals("Name", jobTitle1.getName());
        assertTrue(clinic1.getActive());
        verify(this.userRepository).findById((UUID) any());
    }

    @Test
    void testGetOne01() {
        when(this.userRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.userServiceImpl.getOne(UUID.randomUUID()));
        verify(this.userRepository).findById((UUID) any());
    }

    @Test
    void testGetOneByCardNumber() {
        UUID randomUUIDResult = UUID.randomUUID();
        attachment.setId(randomUUIDResult);

        UUID randomUUIDResult1 = UUID.randomUUID();
        clinic.setId(randomUUIDResult1);

        UUID randomUUIDResult2 = UUID.randomUUID();
        jobTitle.setId(randomUUIDResult2);

        UUID randomUUIDResult3 = UUID.randomUUID();
        role.setId(randomUUIDResult3);
        HashSet<PermissionEnum> permissionEnumSet = new HashSet<>();
        role.setPermissionEnums(permissionEnumSet);

        UUID randomUUIDResult4 = UUID.randomUUID();
        user.setId(randomUUIDResult4);

        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findByResidentCardNumber((Long) any())).thenReturn(ofResult);
        ApiResult<UserResDTO> actualOneByCardNumber = this.userServiceImpl.getOneByCardNumber(1L);
        assertTrue(actualOneByCardNumber.isSuccess());
        assertNull(actualOneByCardNumber.getErrors());
        assertNull(actualOneByCardNumber.getMessage());
        UserResDTO data = actualOneByCardNumber.getData();
        assertEquals(1L, data.getResidentCardNumber().longValue());
        assertTrue(data.isActive());
        assertSame(randomUUIDResult, data.getAvatarImgId());
        assertSame(randomUUIDResult4, data.getId());
        assertEquals("Doe", data.getLastname());
        Assertions.assertEquals(QualificationCategory.SPECIALIST, data.getQualificationCategory());
        assertEquals(1, data.getWorkingExperienceInMonth().intValue());
        assertEquals("Jane", data.getFirstname());
        ClinicResDTO clinic1 = data.getClinic();
        assertEquals("4105551212", clinic1.getPhoneNumber());
        assertEquals("Name", clinic1.getName());
        assertSame(randomUUIDResult1, clinic1.getId());
        assertEquals(1L, clinic1.getClinicLegalCardNumber().longValue());
        JobTitleResDTO jobTitle1 = data.getJobTitle();
        assertSame(randomUUIDResult2, jobTitle1.getId());
        RoleResDTO role1 = data.getRole();
        assertSame(randomUUIDResult3, role1.getId());
        assertSame(permissionEnumSet, role1.getPermissionEnumSet());
        assertEquals("Name", role1.getName());
        assertEquals("Name", jobTitle1.getName());
        assertTrue(clinic1.getActive());
        verify(this.userRepository).findByResidentCardNumber((Long) any());
    }

    @Test
    void testGetOneByCardNumber01(){
        when(this.userRepository.findByResidentCardNumber((Long) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.userServiceImpl.getOneByCardNumber(123L));
        verify(this.userRepository).findByResidentCardNumber((Long) any());
    }

    @Test
    void testGetAll2() {
        ArrayList<User> userList = new ArrayList<>();
        userList.add(user);
        PageImpl<User> pageImpl = new PageImpl<>(userList);
        when(this.userRepository.findAll((org.springframework.data.domain.Pageable) any())).thenReturn(pageImpl);
        ApiResult<CustomPage<UserResDTO>> actualAll = this.userServiceImpl.getAll(1, 3);
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getErrors());
        assertNull(actualAll.getMessage());
        CustomPage<UserResDTO> data = actualAll.getData();
        assertEquals(0, data.getSize());
        assertEquals(0, data.getNumberOfElements());
        assertEquals(0, data.getNumber());
        assertTrue(data.getContent().isEmpty());
        assertEquals(0L, data.getTotalElements());
        assertEquals(1, data.getTotalPages());
        verify(this.userRepository).findAll((org.springframework.data.domain.Pageable) any());
    }

    @Test
//good two tests
    void testGetAll6() {


        Attachment attachment1 = new Attachment();
        attachment1.setContentType("text/plain");
        attachment1.setCreatedAt(mock(Timestamp.class));
        attachment1.setCreatedBy(UUID.randomUUID());
        attachment1.setDeleted(true);
        attachment1.setId(UUID.randomUUID());
        attachment1.setName("DOCTOR");
        attachment1.setSize(3L);
        attachment1.setUpdatedAt(mock(Timestamp.class));
        attachment1.setUpdatedBy(UUID.randomUUID());

        Address address1 = new Address();
        address1.setCreatedAt(mock(Timestamp.class));
        address1.setCreatedBy(UUID.randomUUID());
        address1.setDeleted(true);
        address1.setDistrict("DOCTOR");
        address1.setHomeNumber("42");
        address1.setId(UUID.randomUUID());
        address1.setStreet("DOCTOR");
        address1.setUpdatedAt(mock(Timestamp.class));
        address1.setUpdatedBy(UUID.randomUUID());

        Clinic clinic1 = new Clinic();
        clinic1.setActive(true);
        clinic1.setAddress(address1);
        clinic1.setClinicLegalCardNumber(1L);
        clinic1.setCreatedAt(mock(Timestamp.class));
        clinic1.setCreatedBy(UUID.randomUUID());
        clinic1.setDeleted(true);
        clinic1.setId(UUID.randomUUID());
        clinic1.setName("DOCTOR");
        clinic1.setPhoneNumber("4105551212");
        clinic1.setUpdatedAt(mock(Timestamp.class));
        clinic1.setUpdatedBy(UUID.randomUUID());

        JobTitle jobTitle1 = new JobTitle();
        jobTitle1.setCreatedAt(mock(Timestamp.class));
        jobTitle1.setCreatedBy(UUID.randomUUID());
        jobTitle1.setDeleted(true);
        jobTitle1.setId(UUID.randomUUID());
        jobTitle1.setName("DOCTOR");
        jobTitle1.setUpdatedAt(mock(Timestamp.class));
        jobTitle1.setUpdatedBy(UUID.randomUUID());

        Role role1 = new Role();
        role1.setCreatedAt(mock(Timestamp.class));
        role1.setCreatedBy(UUID.randomUUID());
        role1.setDeleted(true);
        role1.setId(UUID.randomUUID());
        role1.setName("DOCTOR");
        role1.setPermissionEnums(new HashSet<>());
        role1.setUpdatedAt(mock(Timestamp.class));
        role1.setUpdatedBy(UUID.randomUUID());

        User user1 = new User();
        user1.setAccountNonExpired(true);
        user1.setAccountNonLocked(true);
        user1.setActive(true);
        user1.setAvatarImg(attachment1);
        user1.setClinic(clinic1);
        user1.setCreatedAt(mock(Timestamp.class));
        user1.setCreatedBy(UUID.randomUUID());
        user1.setCredentialsNonExpired(true);
        user1.setDeleted(true);
        user1.setEnabled(true);
        user1.setFirstname("Jane");
        user1.setId(UUID.randomUUID());
        user1.setJobTitle(jobTitle1);
        user1.setLastname("Doe");
        user1.setPassword("iloveyou");
        user1.setQualificationCategory(QualificationCategory.SPECIALIST);
        user1.setResidentCardNumber(1L);
        user1.setRole(role1);
        user1.setUpdatedAt(mock(Timestamp.class));
        user1.setUpdatedBy(UUID.randomUUID());
        user1.setWorkingExperienceInMonth(1);

        ArrayList<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user);
        PageImpl<User> pageImpl = new PageImpl<>(userList);
        when(this.userRepository.findAll((org.springframework.data.domain.Pageable) any())).thenReturn(pageImpl);
        ApiResult<CustomPage<UserResDTO>> actualAll = this.userServiceImpl.getAll(1, 3);
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getErrors());
        assertNull(actualAll.getMessage());
        CustomPage<UserResDTO> data = actualAll.getData();
        assertEquals(1, data.getSize());
        assertEquals(1, data.getNumberOfElements());
        assertEquals(0, data.getNumber());
        assertEquals(1, data.getContent().size());
        assertEquals(1L, data.getTotalElements());
        assertEquals(1, data.getTotalPages());
        verify(this.userRepository).findAll((org.springframework.data.domain.Pageable) any());
    }

    @Test
    void testDelete() {
        doNothing().when(this.userRepository).deleteById((UUID) any());
        ApiResult<?> actualDeleteResult = this.userServiceImpl.delete(UUID.randomUUID());
        assertNull(actualDeleteResult.getData());
        assertTrue(actualDeleteResult.isSuccess());
        assertEquals("User successfully deleted!", actualDeleteResult.getMessage());
        assertNull(actualDeleteResult.getErrors());
        verify(this.userRepository).deleteById((UUID) any());
    }

    @Test
    void testDelete2() {
        doThrow(new RestException(HttpStatus.CONTINUE, "An error occurred")).when(this.userRepository)
                .deleteById((UUID) any());
        assertThrows(RestException.class, () -> this.userServiceImpl.delete(UUID.randomUUID()));
        verify(this.userRepository).deleteById((UUID) any());
    }

    @Test
    void testGetAllByClinicId2() {
        ArrayList<User> userList = new ArrayList<>();
        userList.add(user);
        when(this.userRepository.findAllByClinicId((UUID) any())).thenReturn(userList);
        ApiResult<List<UserResDTO>> actualAllByClinicId = this.userServiceImpl.getAllByClinicId(UUID.randomUUID());
        assertEquals(1, actualAllByClinicId.getData().size());
        assertTrue(actualAllByClinicId.isSuccess());
        assertNull(actualAllByClinicId.getMessage());
        assertNull(actualAllByClinicId.getErrors());
        verify(this.userRepository).findAllByClinicId((UUID) any());
    }

    @Test
    void testGetAllByClinicCardNumber() {
        when(this.userRepository.findAllByClinicId((UUID) any())).thenReturn(new ArrayList<>());
        Optional<Clinic> ofResult = Optional.of(clinic);
        when(this.clinicRepository.findByClinicLegalCardNumber((Long) any())).thenReturn(ofResult);
        ApiResult<List<UserResDTO>> actualAllByClinicCardNumber = this.userServiceImpl.getAllByClinicCardNumber(1L);
        assertTrue(actualAllByClinicCardNumber.getData().isEmpty());
        assertTrue(actualAllByClinicCardNumber.isSuccess());
        assertNull(actualAllByClinicCardNumber.getMessage());
        assertNull(actualAllByClinicCardNumber.getErrors());
        verify(this.userRepository).findAllByClinicId((UUID) any());
        verify(this.clinicRepository).findByClinicLegalCardNumber((Long) any());
    }

    @Test
    void testGetAllByClinicCardNumber01(){
        when(this.clinicRepository.findByClinicLegalCardNumber((Long) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.userServiceImpl.getAllByClinicCardNumber(123L));
        verify(this.clinicRepository).findByClinicLegalCardNumber((Long) any());
    }

    @Test
    void testActivateUser() {
        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.save((User) any())).thenReturn(user);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        ApiResult<?> actualActivateUserResult = this.userServiceImpl.activateUser(1L);
        assertNull(actualActivateUserResult.getData());
        assertTrue(actualActivateUserResult.isSuccess());
        assertEquals("User is activated successfully saved", actualActivateUserResult.getMessage());
        assertNull(actualActivateUserResult.getErrors());
        verify(this.userRepository).save((User) any());
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
    }

    @Test
    void testActivateUser01(){
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.userServiceImpl.activateUser((123L)));
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
    }

    @Test
    void testDeactivateUser() {
        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.save((User) any())).thenReturn(user);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        ApiResult<?> actualDeactivateUserResult = this.userServiceImpl.deactivateUser(1L);
        assertNull(actualDeactivateUserResult.getData());
        assertTrue(actualDeactivateUserResult.isSuccess());
        assertEquals("User is deactivated successfully saved", actualDeactivateUserResult.getMessage());
        assertNull(actualDeactivateUserResult.getErrors());
        verify(this.userRepository).save((User) any());
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
    }

    @Test
    void testDeactivateUser01(){
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.userServiceImpl.deactivateUser((123L)));
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
    }
}

