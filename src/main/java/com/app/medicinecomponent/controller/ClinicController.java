package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.ClinicReqDTO;
import com.app.medicinecomponent.payload.ClinicResDTO;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.CLINIC_CONTROLLER)
@Tag(name = "Clinic operations", description = "Clinic")
public interface ClinicController {

    @PreAuthorize(value = "hasAuthority('ADD_CLINIC')")
    @Operation(summary = "Add clinic (admin)")
    @PostMapping
    ApiResult<ClinicResDTO> add(@RequestBody @Valid ClinicReqDTO clinicReqDTO);

    @Operation(summary = "Get one clinic (any user)")
    @GetMapping("/{id}")
    ApiResult<ClinicResDTO> getOne(@PathVariable UUID id);

    @Operation(summary = "Get all clinics (any user)")
    @GetMapping
    ApiResult<List<ClinicResDTO>> getAll();

    @Operation(summary = "Get clinic by card number for clinic module (clinic module)")
    @GetMapping("/legal-card-number/{cardNumber}")
    ApiResult<ClinicResDTO> getOneByLegalCardNumber(@PathVariable Long cardNumber);

}
