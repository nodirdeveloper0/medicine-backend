package com.app.medicinecomponent.entity.enums;

public enum CertificateTypeEnum {
    CERTIFICATE_OF_REGISTRATION("CERTIFICATE OF REGISTRATION"),
    CERTIFICATE_OF_QUALIFICATION("CERTIFICATE OF QUALIFICATION");

    private String name;

    CertificateTypeEnum(String name) {
    }

    public String getName() {
        return name;
    }
}
