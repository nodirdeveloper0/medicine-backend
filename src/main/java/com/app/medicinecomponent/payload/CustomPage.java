package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomPage<T> {
    private List<T> content; // Elements
    private int numberOfElements; // Number of elements in current page
    private int number; // Current page number
    private long totalElements; // Total elements
    private int totalPages; //Number of total pages
    private int size; // number of requested elements in one page
}
