package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiseaseInfoReqDTO {
    @NotNull(message = "Patient's firstName shouldn't be empty ")
    private String firstName;

    @NotNull(message = "Patient's lastName shouldn't be empty ")
    private String lastName;

    @NotNull(message = "Card number shouldn't be empty!")
    private Long cardNumber;

    @NotBlank(message = "Description shouldn't be empty")
    private String description;
}
