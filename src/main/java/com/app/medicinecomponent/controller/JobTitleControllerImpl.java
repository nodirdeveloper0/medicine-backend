package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.JobTitleReqDTO;
import com.app.medicinecomponent.payload.JobTitleResDTO;
import com.app.medicinecomponent.service.JobTitleService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class JobTitleControllerImpl implements JobTitleController {
    private final JobTitleService jobTitleService;

    public JobTitleControllerImpl(JobTitleService jobTitleService) {
        this.jobTitleService = jobTitleService;
    }


    @Override
    public ApiResult<JobTitleResDTO> add(JobTitleReqDTO jobTitleReqDTO) {
        return jobTitleService.add(jobTitleReqDTO);
    }

    @Override
    public ApiResult<JobTitleResDTO> getOne(UUID id) {
        return jobTitleService.getOne(id);
    }

    @Override
    public ApiResult<List<JobTitleResDTO>> getAll() {
        return jobTitleService.getAll();
    }

    @Override
    public ApiResult<JobTitleResDTO> edit(JobTitleReqDTO jobTitleReqDTO, UUID id) {
        return jobTitleService.edit(jobTitleReqDTO, id);
    }
}
