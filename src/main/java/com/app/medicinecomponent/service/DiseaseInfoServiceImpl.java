package com.app.medicinecomponent.service;

import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.entity.DiseaseInfo;
import com.app.medicinecomponent.entity.enums.DiseaseInfoStatusEnums;
import com.app.medicinecomponent.mapper.CustomMapper;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.DiseaseInfoReqDTO;
import com.app.medicinecomponent.payload.DiseaseInfoResDto;
import com.app.medicinecomponent.repository.ClinicRepository;
import com.app.medicinecomponent.repository.DiseaseInfoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DiseaseInfoServiceImpl implements DiseaseInfoService {
    private final DiseaseInfoRepository diseaseInfoRepository;
    private final ClinicRepository clinicRepository;


    public DiseaseInfoServiceImpl(DiseaseInfoRepository diseaseInfoRepository, ClinicRepository clinicRepository) {
        this.diseaseInfoRepository = diseaseInfoRepository;
        this.clinicRepository = clinicRepository;
    }

    @Override
    public ApiResult<DiseaseInfoResDto> add(DiseaseInfoReqDTO diseaseInfoReqDTO) {
        List<String> clinicIdsInString = clinicRepository.findAllIdsAsString();
        List<UUID> clinicIdsInUUID = clinicIdsInString.stream().map(UUID::fromString).collect(Collectors.toList());
        DiseaseInfo diseaseInfo = new DiseaseInfo(
                diseaseInfoReqDTO.getFirstName(),
                diseaseInfoReqDTO.getLastName(),
                diseaseInfoReqDTO.getCardNumber(),
                diseaseInfoReqDTO.getDescription(),
                DiseaseInfoStatusEnums.NEW,
                clinicRepository.findById(clinicIdsInUUID.get((int) (Math.random() * clinicIdsInUUID.size()))).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Clinic is not found")),
                new Timestamp(System.currentTimeMillis()),
                null
        );
        diseaseInfoRepository.save(diseaseInfo);
        return ApiResult.successResponse(CustomMapper.diseaseInfoToResDTO(diseaseInfo));
    }

    @Override
    public ApiResult<DiseaseInfoResDto> getOne(UUID id) {
        DiseaseInfo diseaseInfo = diseaseInfoRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Disease info not found"));
        return ApiResult.successResponse(CustomMapper.diseaseInfoToResDTO(diseaseInfo));
    }

    @Override
    public ApiResult<List<DiseaseInfoResDto>> getAll() {
        List<DiseaseInfo> diseaseInfoList = diseaseInfoRepository.findAll();
        return ApiResult.successResponse(CustomMapper.diseaseInfoToResDTOList(diseaseInfoList));
    }

}
