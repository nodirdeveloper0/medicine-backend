package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.AboutUsReqDTO;
import com.app.medicinecomponent.payload.AboutUsResDTO;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.service.AboutUsService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class AboutUsControllerImpl implements AboutUsController {
    private final AboutUsService aboutUsService;

    public AboutUsControllerImpl(AboutUsService aboutUsService) {
        this.aboutUsService = aboutUsService;
    }

    @Override
    public ApiResult<AboutUsResDTO> add(AboutUsReqDTO aboutUsReqDTO) {
        return aboutUsService.add(aboutUsReqDTO);
    }

    @Override
    public ApiResult<AboutUsResDTO> getOne(UUID id) {
        return aboutUsService.getOne(id);
    }

    @Override
    public ApiResult<List<AboutUsResDTO>> getAll() {
        return aboutUsService.getAll();
    }

    @Override
    public ApiResult<AboutUsResDTO> edit(AboutUsReqDTO aboutUsReqDTO, UUID id) {
        return aboutUsService.edit(aboutUsReqDTO, id);
    }

    @Override
    public ApiResult<?> delete(UUID id) {
        return aboutUsService.delete((id));
    }
}
