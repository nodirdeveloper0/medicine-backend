package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.CustomPage;
import com.app.medicinecomponent.payload.UserResDTO;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.USER_CONTROLLER)
@Tag(name = "User operations", description = "User")
public interface UserController {

    String DEFAULT_PAGE_NUMBER = "0";
    String DEFAULT_PAGE_SIZE = "10";

    @Operation(summary = "Get one user (any user)")
    @GetMapping("/{id}")
    ApiResult<UserResDTO> getOne(@PathVariable UUID id);

    @Operation(summary = "Get all user by pageable (any user)")
    @GetMapping
    ApiResult<CustomPage<UserResDTO>> getAll(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) int page,
                                             @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) int size);

    @PreAuthorize(value = "hasAuthority('DELETE_USER')")
    @Operation(summary = "Delete one user (admin)")
    @DeleteMapping("/{id}")
    ApiResult<?> delete(@PathVariable UUID id);

    @Operation(summary = "Get user list by clinic id ((any user))")
    @GetMapping("/all-users-by-clinic-id/{clinicId}")
    ApiResult<List<UserResDTO>> getAllByClinicId(@PathVariable UUID clinicId);

    @Operation(summary = "Get user by user-card-number (for clinic module)")
    @GetMapping("/resident-card-number/{cardNumber}")
    ApiResult<UserResDTO> getOneByCardNumber(@PathVariable Long cardNumber);

    @Operation(summary = "Get all user by clinic card number (for clinic module)")
    @GetMapping("/all-users-by-clinic-card-number/{clinicCardNumber}")
    ApiResult<List<UserResDTO>> getAllByClinicCardNumber(@PathVariable Long clinicCardNumber);

    @Operation(summary = "Activate user status by cardNumber (for City-management module)",description = "This is for City-Management module")
    @PutMapping("/activate/{cardNumber}")
    ApiResult<?> activateUser(@PathVariable Long cardNumber);

    @Operation(summary = "Deactivate user status by cardNumber (for City-management module)",description = "This is for City-Management module")
    @PutMapping("/deactivate/{cardNumber}")
    ApiResult<?> deactivateUser(@PathVariable Long cardNumber);
}
