package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RoleResDTO;
import com.app.medicinecomponent.service.RoleService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class RoleControllerImpl implements RoleController {
    private final RoleService roleService;

    public RoleControllerImpl(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public ApiResult<RoleResDTO> getOne(UUID id) {
        return roleService.getOne(id);
    }

    @Override
    public ApiResult<List<RoleResDTO>> getAll() {
        return roleService.getAll();
    }
}
