package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.service.AttachmentService;
import com.app.medicinecomponent.entity.Attachment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@RestController
public class AttachmentControllerImpl implements AttachmentController {

    private final AttachmentService attachmentService;

    public AttachmentControllerImpl(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @Override
    public ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException {
        return attachmentService.upload(request);
    }

    @Override
    public ApiResult<Attachment> getById(UUID id) {
        return attachmentService.getById(id);
    }

    @Override
    public ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException {
        return attachmentService.getFile(id,response);
    }
}