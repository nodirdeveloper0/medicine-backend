package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Role;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RoleResDTO;
import com.app.medicinecomponent.repository.RoleRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {RoleServiceImpl.class})
@ExtendWith(SpringExtension.class)
class RoleServiceImplTest {
    @MockBean
    private RoleRepository roleRepository;

    @Autowired
    private RoleServiceImpl roleServiceImpl;

    Role role = new Role();

    @BeforeEach
    void setUp() {
        role.setCreatedAt(mock(Timestamp.class));
        role.setCreatedBy(UUID.randomUUID());
        role.setDeleted(true);
        role.setId(UUID.randomUUID());
        role.setName("Name");
        role.setPermissionEnums(new HashSet<>());
        role.setUpdatedAt(mock(Timestamp.class));
        role.setUpdatedBy(UUID.randomUUID());
    }

    @Test
    void testGetOne() {
        UUID randomUUIDResult = UUID.randomUUID();
        role.setId(randomUUIDResult);
        Optional<Role> ofResult = Optional.of(role);
        when(this.roleRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<RoleResDTO> actualOne = this.roleServiceImpl.getOne(UUID.randomUUID());
        assertTrue(actualOne.isSuccess());
        assertNull(actualOne.getErrors());
        assertNull(actualOne.getMessage());
        RoleResDTO data = actualOne.getData();
        assertSame(randomUUIDResult, data.getId());
        assertEquals("Name", data.getName());
        assertTrue(data.getPermissionEnumSet().isEmpty());
        verify(this.roleRepository).findById((UUID) any());
    }

    @Test
    void testGetOne3() {
        when(this.roleRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.roleServiceImpl.getOne(UUID.randomUUID()));
        verify(this.roleRepository).findById((UUID) any());
    }


    @Test
    void testGetAll2() {
        UUID randomUUIDResult = UUID.randomUUID();
        role.setId(randomUUIDResult);
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(role);
        when(this.roleRepository.findAll()).thenReturn(roleList);
        ApiResult<List<RoleResDTO>> actualAll = this.roleServiceImpl.getAll();
        List<RoleResDTO> data = actualAll.getData();
        assertEquals(1, data.size());
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getErrors());
        assertNull(actualAll.getMessage());
        RoleResDTO getResult = data.get(0);
        assertSame(randomUUIDResult, getResult.getId());
        assertTrue(getResult.getPermissionEnumSet().isEmpty());
        assertEquals("Name", getResult.getName());
        verify(this.roleRepository).findAll();
    }
}

