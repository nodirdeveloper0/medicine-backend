package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.entity.Attachment;
import com.app.medicinecomponent.service.AttachmentServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AttachmentControllerImpl.class})
class AttachmentControllerImplTest {

    @Autowired
    private AttachmentControllerImpl attachmentController;

    @MockBean
    private AttachmentServiceImpl attachmentService;

    @Test
    void upload() throws Exception {
//        when(this.attachmentService.upload(ArgumentMatchers.any())).thenReturn(new ApiResult<>(UUID.randomUUID().toString(),null, null, true));
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );
        MockMvc mockMvc
                = MockMvcBuilders.standaloneSetup(this.attachmentController).build();
        mockMvc.perform(multipart("/api/attachment/upload").file(file))
                .andExpect(status().is(200));
    }

    @Test
    void getById() throws Exception {
        when(this.attachmentService.getById(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, new Attachment("Photo",".jpg",12345L), null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/attachment/info/{id}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.attachmentController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getFile() throws Exception {
        when(this.attachmentService.getFile(ArgumentMatchers.any(),ArgumentMatchers.any())).thenReturn(new ApiResult<>("Attachment successfully got",null, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/attachment/download/{id}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.attachmentController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }
}