package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Registration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
@Repository
public interface RegistrationRepository extends JpaRepository<Registration, UUID> {

//    boolean existsAllByResidentCardNumber(UUID residentCardNumber);
/*
    select resident_card_number from registration
    join attestation a on registration.attestation_id = a.id
    where attestation_id=:attestationId*/

    @Query(value = "select resident_card_number from registration\n" +
            "    join attestation a on registration.attestation_id = a.id\n" +
            "    where attestation_id=:attestationId",nativeQuery = true)

    List<Long> findAllResidentCardNumber(@Param("attestationId") UUID attestationId);


//    List<Registration> findAllByAttestationId(UUID attestation_id);

    boolean existsByResidentCardNumber(Long residentCardNumber);

}
