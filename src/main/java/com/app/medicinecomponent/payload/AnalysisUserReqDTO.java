package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnalysisUserReqDTO {

//    @NotBlank(message = "Token should not be empty!")
    private String token;
}
