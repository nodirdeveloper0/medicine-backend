package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.service.AuthServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.UUID;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AuthControllerImpl.class})
class AuthControllerImplTest {

    @Autowired
    private AuthControllerImpl authController;

    @MockBean
    private AuthServiceImpl authService;

    UserResDTO userResDTO = new UserResDTO();

    @BeforeEach
    void setUp() {
        AddressResDTO addressResDTO = new AddressResDTO();
        addressResDTO.setId(UUID.randomUUID());
        addressResDTO.setHomeNumber("12");
        addressResDTO.setStreet("Oybek");
        addressResDTO.setDistrict("Yunusobod");

        ClinicResDTO clinicResDTO = new ClinicResDTO();
        clinicResDTO.setId(UUID.randomUUID());
        clinicResDTO.setName("AKFA");
        clinicResDTO.setClinicLegalCardNumber(123L);
        clinicResDTO.setPhoneNumber("+998981234567");
        clinicResDTO.setAddress(addressResDTO);
        clinicResDTO.setActive(true);

        JobTitleResDTO jobTitleResDTO = new JobTitleResDTO();
        jobTitleResDTO.setId(UUID.randomUUID());
        jobTitleResDTO.setName("DENTIST");

        RoleResDTO roleResDTO = new RoleResDTO();
        roleResDTO.setId(UUID.randomUUID());
        roleResDTO.setName("DOCTOR");
        roleResDTO.setPermissionEnumSet(new HashSet<>());

        userResDTO.setId(UUID.randomUUID());
        userResDTO.setFirstname("Jane");
        userResDTO.setLastname("Doe");
        userResDTO.setResidentCardNumber(1L);
        userResDTO.setWorkingExperienceInMonth(1);
        userResDTO.setQualificationCategory(QualificationCategory.SPECIALIST);
        userResDTO.setJobTitle(jobTitleResDTO);
        userResDTO.setClinic(clinicResDTO);
        userResDTO.setRole(roleResDTO);
        userResDTO.setActive(true);
        userResDTO.setAvatarImgUrl("This is path for avatar img");
        userResDTO.setAvatarImgId(UUID.randomUUID());
    }

    @Test
    void signUp() throws Exception {

        SignUpReqDTO signUpReqDTO = new SignUpReqDTO(123L, "admin123", UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID());
        when(this.authService.signUp(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, userResDTO, null, true));
        String content = (new ObjectMapper().writeValueAsString(signUpReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.authController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void signIn() throws Exception {
        SignInReqDTO signInReqDTO = new SignInReqDTO();
        signInReqDTO.setResidentCardNumber("1");
        signInReqDTO.setPassword("admin");
        TokenResDto tokenResDto = new TokenResDto("This is token........");
        when(this.authService.signIn(ArgumentMatchers.any())).thenReturn(new ApiResult<>(null, tokenResDto, null, true));
        String content = (new ObjectMapper().writeValueAsString(signInReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.authController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));

    }

    @Test
    void analysisUser() throws Exception {
        AnalysisUserReqDTO analysisUserReqDTO = new AnalysisUserReqDTO("This is token........");
        when(this.authService.analysisUser(ArgumentMatchers.any())).thenReturn(new ApiResult<>("DOCTOR", null, null, true));
        String content = (new ObjectMapper().writeValueAsString(analysisUserReqDTO));
        MockHttpServletRequestBuilder requestBuilder =
                MockMvcRequestBuilders.post("/api/auth/analysis-user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content);
        ResultActions resultActions = MockMvcBuilders.standaloneSetup(this.authController)
                .build()
                .perform(requestBuilder);
        resultActions.andExpect(MockMvcResultMatchers.status().is(200));

    }
}