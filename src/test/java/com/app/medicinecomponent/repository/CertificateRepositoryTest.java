package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.enums.CertificateTypeEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;

import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

@ContextConfiguration(classes = {CertificateRepository.class,AttestationRepository.class,UserRepository.class, RoleRepository.class, JobTitleRepository.class, ClinicRepository.class, AddressRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class CertificateRepositoryTest {
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private ClinicRepository clinicRepository;
    @Autowired
    private JobTitleRepository jobTitleRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AttestationRepository attestationRepository;
    @Autowired
    private CertificateRepository certificateRepository;

    Certificate certificate = new Certificate();
    User user = new User();

    @BeforeEach
    void setUp() {
        Address address = new Address();
        address.setDeleted(false);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setStreet("Street");
        addressRepository.save(address);

        Clinic clinic = new Clinic();
        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setDeleted(false);
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinicRepository.save(clinic);

        JobTitle jobTitle = new JobTitle();
        jobTitle.setDeleted(false);
        jobTitle.setName("Name");
        jobTitleRepository.save(jobTitle);

        Role role = new Role();
        role.setDeleted(false);
        role.setName("Name");
        role.setPermissionEnums(new HashSet<>());
        roleRepository.save(role);

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setActive(true);
        user.setClinic(clinic);
        user.setCredentialsNonExpired(true);
        user.setDeleted(false);
        user.setEnabled(true);
        user.setFirstname("Jane");
        user.setJobTitle(jobTitle);
        user.setLastname("Doe");
        user.setPassword("iloveyou");
        user.setQualificationCategory(QualificationCategory.SPECIALIST);
        user.setResidentCardNumber(1L);
        user.setRole(role);
        user.setWorkingExperienceInMonth(1);
        userRepository.save(user);

        Attestation attestation = new Attestation();
        attestation.setClosed(true);
        attestation.setDeleted(false);
        attestation.setDescription("The characteristics of someone or something");
        attestation.setEndDate(mock(Date.class));
        attestation.setStartDate(mock(Date.class));
        attestationRepository.save(attestation);

        certificate.setSerialNumber(1);
        certificate.setToUser(user);
        certificate.setToDate(mock(Date.class));
        certificate.setFromDate(mock(Date.class));
        certificate.setQualificationCategory(QualificationCategory.SPECIALIST);
        certificate.setJobTitle(jobTitle);
        certificate.setAttestation(attestation);
        certificate.setCertificateTypeEnum(CertificateTypeEnum.CERTIFICATE_OF_REGISTRATION);
        certificate.setDescription("This is description of certificate");
        certificateRepository.save(certificate);
    }

    @AfterEach
    void tearDown() {
        addressRepository.deleteAll();
        clinicRepository.deleteAll();
        jobTitleRepository.deleteAll();
        roleRepository.deleteAll();
        userRepository.deleteAll();
        attestationRepository.deleteAll();
        certificateRepository.deleteAll();
    }

    @Test
    void testFindAllByToUserId() {
        UUID id = certificate.getToUser().getId();
        List<Certificate> allByToUserId = this.certificateRepository.findAllByToUserId(id);
        assertNotNull(allByToUserId);
        assertEquals(allByToUserId.get(0),certificate);
    }

    @Test
    void testCountAll() {
        assertEquals(this.certificateRepository.countAll(),1);
    }

    @Test
    void testFindByToUserIdAndQualificationCategory() {
        Optional<Certificate> optionalCertificate = this.certificateRepository.findByToUserIdAndQualificationCategory(certificate.getId(), certificate.getQualificationCategory());
        optionalCertificate.ifPresent(value -> assertEquals(value,certificate));
    }

    @Test
    void testFindByFromDateAndToUserId() {
        Optional<Certificate> byFromDateAndToUserId = this.certificateRepository.findByFromDateAndToUserId(certificate.getToUser().getId());
        byFromDateAndToUserId.ifPresent(value -> assertEquals(value,certificate));
    }
}

