package com.app.medicinecomponent.entity;

import com.app.medicinecomponent.entity.enums.CertificateTypeEnum;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.entity.template.AbcEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update certificate set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Certificate extends AbcEntity {

    @Column(nullable = false, unique = true)
    private Integer serialNumber;

    @OneToOne(fetch = FetchType.LAZY)
    private User toUser;

    @Column(nullable = false)
    private Date fromDate;

    @Column(nullable = false)
    private Date toDate;

    @Enumerated(EnumType.STRING)
    private QualificationCategory qualificationCategory;

    @OneToOne(fetch = FetchType.LAZY)
    private JobTitle jobTitle;

    @ManyToOne(fetch = FetchType.LAZY)
    private Attestation attestation;

    @Enumerated(EnumType.STRING)
    private CertificateTypeEnum certificateTypeEnum;

    private String description;

    public Certificate(Integer serialNumber, User toUser, Date fromDate, Date toDate, QualificationCategory qualificationCategory, JobTitle jobTitle, CertificateTypeEnum certificateTypeEnum, String description) {
        this.serialNumber = serialNumber;
        this.toUser = toUser;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.qualificationCategory = qualificationCategory;
        this.jobTitle = jobTitle;
        this.certificateTypeEnum = certificateTypeEnum;
        this.description = description;
    }
}
