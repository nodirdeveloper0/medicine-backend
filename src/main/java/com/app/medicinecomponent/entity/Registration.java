package com.app.medicinecomponent.entity;

import com.app.medicinecomponent.entity.template.AbcEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update registration set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Registration extends AbcEntity {

    @Column(nullable = false)
    private Long residentCardNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    private Attestation  attestation;

}
