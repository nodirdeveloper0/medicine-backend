package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.service.UserServiceImpl;
import com.app.medicinecomponent.payload.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UserControllerImpl.class})
class UserControllerImplTest {

    @Autowired
    private UserControllerImpl userController;

    @MockBean
    private UserServiceImpl userService;

    UserResDTO userResDTO = new UserResDTO();

    @BeforeEach
    void setUp() {
        AddressResDTO addressResDTO = new AddressResDTO();
        addressResDTO.setId(UUID.randomUUID());
        addressResDTO.setHomeNumber("12");
        addressResDTO.setStreet("Oybek");
        addressResDTO.setDistrict("Yunusobod");

        ClinicResDTO clinicResDTO = new ClinicResDTO();
        clinicResDTO.setId(UUID.randomUUID());
        clinicResDTO.setName("AKFA");
        clinicResDTO.setClinicLegalCardNumber(123L);
        clinicResDTO.setPhoneNumber("+998981234567");
        clinicResDTO.setAddress(addressResDTO);
        clinicResDTO.setActive(true);

        JobTitleResDTO jobTitleResDTO = new JobTitleResDTO();
        jobTitleResDTO.setId(UUID.randomUUID());
        jobTitleResDTO.setName("DENTIST");

        RoleResDTO roleResDTO = new RoleResDTO();
        roleResDTO.setId(UUID.randomUUID());
        roleResDTO.setName("DOCTOR");
        roleResDTO.setPermissionEnumSet(new HashSet<>());

        userResDTO.setId(UUID.randomUUID());
        userResDTO.setFirstname("Jane");
        userResDTO.setLastname("Doe");
        userResDTO.setResidentCardNumber(1L);
        userResDTO.setWorkingExperienceInMonth(1);
        userResDTO.setQualificationCategory(QualificationCategory.SPECIALIST);
        userResDTO.setJobTitle(jobTitleResDTO);
        userResDTO.setClinic(clinicResDTO);
        userResDTO.setRole(roleResDTO);
        userResDTO.setActive(true);
        userResDTO.setAvatarImgUrl("This is path for avatar img");
        userResDTO.setAvatarImgId(UUID.randomUUID());
    }

    @Test
    void getOne() throws Exception {
        when(this.userService.getOne((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, userResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/user/{id}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAll() throws Exception {
        List<UserResDTO> userResDTOList = new ArrayList<>();
        userResDTOList.add(userResDTO);
        CustomPage<UserResDTO> customPage = new CustomPage<>(userResDTOList,1,1,1,1,1);

        when(this.userService.getAll(anyInt(),anyInt())).thenReturn(new ApiResult<CustomPage<UserResDTO>>(null,  customPage, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/user");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void delete() throws Exception {
        when(this.userService.delete((ArgumentMatchers.any()))).thenReturn(new ApiResult<>("User successfully deleted!", null, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/user/{id}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getAllByClinicId() throws Exception {
        List<UserResDTO> userResDTOList = new ArrayList<>();
        userResDTOList.add(userResDTO);
        when(this.userService.getAllByClinicId((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null,userResDTOList , null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/user/all-users-by-clinic-id/{clinicId}", UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void getOneByCardNumber() throws Exception {
        when(this.userService.getOneByCardNumber((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null, userResDTO, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/user/resident-card-number/{cardNumber}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));

    }

    @Test
    void getAllByClinicCardNumber() throws Exception {
        List<UserResDTO> userResDTOList = new ArrayList<>();
        userResDTOList.add(userResDTO);
        when(this.userService.getAllByClinicCardNumber((ArgumentMatchers.any()))).thenReturn(new ApiResult<>(null,userResDTOList , null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/user/all-users-by-clinic-card-number/{clinicCardNumber}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void activateUser() throws Exception {
        when(this.userService.activateUser((ArgumentMatchers.any()))).thenReturn(new ApiResult<>("User is activated", null, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/user/activate/{cardNumber}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void deactivateUser() throws Exception {
        when(this.userService.deactivateUser((ArgumentMatchers.any()))).thenReturn(new ApiResult<>("User is deactivated", null, null, true));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/user/deactivate/{cardNumber}", 123L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.userController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(200));
    }
}