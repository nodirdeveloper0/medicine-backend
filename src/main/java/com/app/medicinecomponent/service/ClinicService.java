package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.ClinicReqDTO;
import com.app.medicinecomponent.payload.ClinicResDTO;

import java.util.List;
import java.util.UUID;

public interface ClinicService {

    ApiResult<ClinicResDTO> add(ClinicReqDTO clinicReqDTO);

    ApiResult<ClinicResDTO> getOne(UUID id);

    ApiResult<List<ClinicResDTO>> getAll();

    ApiResult<ClinicResDTO> getOneByLegalCardNumber(Long cardNumber);

}
