package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.entity.enums.QualificationCategory;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RegisteredUsersResDTO;
import com.app.medicinecomponent.payload.RegistrationReqDTO;
import com.app.medicinecomponent.repository.AttestationRepository;
import com.app.medicinecomponent.repository.RegistrationRepository;
import com.app.medicinecomponent.repository.UserRepository;
import com.app.medicinecomponent.entity.*;
import com.app.medicinecomponent.payload.RegistrationResDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RegistrationServiceImpl.class})
@ExtendWith(SpringExtension.class)
class RegistrationServiceImplTest {
    @MockBean
    private AttestationRepository attestationRepository;

    @MockBean
    private RegistrationRepository registrationRepository;

    @Autowired
    private RegistrationServiceImpl registrationServiceImpl;

    @MockBean
    private UserRepository userRepository;

    Attachment attachment = new Attachment();
    Address address = new Address();
    Clinic clinic = new Clinic();
    JobTitle jobTitle = new JobTitle();
    Role role = new Role();
    User user = new User();
    User user1 = new User();
    Attestation attestation = new Attestation();
    Registration registration = new Registration();

    @BeforeEach
    void setUp() {
        attachment.setContentType("text/plain");
        attachment.setCreatedAt(mock(Timestamp.class));
        attachment.setCreatedBy(UUID.randomUUID());
        attachment.setDeleted(true);
        attachment.setId(UUID.randomUUID());
        attachment.setName("Name");
        attachment.setSize(3L);
        attachment.setUpdatedAt(mock(Timestamp.class));
        attachment.setUpdatedBy(UUID.randomUUID());

        address.setCreatedAt(mock(Timestamp.class));
        address.setCreatedBy(UUID.randomUUID());
        address.setDeleted(true);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setId(UUID.randomUUID());
        address.setStreet("Street");
        address.setUpdatedAt(mock(Timestamp.class));
        address.setUpdatedBy(UUID.randomUUID());

        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setCreatedAt(mock(Timestamp.class));
        clinic.setCreatedBy(UUID.randomUUID());
        clinic.setDeleted(true);
        clinic.setId(UUID.randomUUID());
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinic.setUpdatedAt(mock(Timestamp.class));
        clinic.setUpdatedBy(UUID.randomUUID());

        jobTitle.setCreatedAt(mock(Timestamp.class));
        jobTitle.setCreatedBy(UUID.randomUUID());
        jobTitle.setDeleted(true);
        jobTitle.setId(UUID.randomUUID());
        jobTitle.setName("Name");
        jobTitle.setUpdatedAt(mock(Timestamp.class));
        jobTitle.setUpdatedBy(UUID.randomUUID());

        role.setCreatedAt(mock(Timestamp.class));
        role.setCreatedBy(UUID.randomUUID());
        role.setDeleted(true);
        role.setId(UUID.randomUUID());
        role.setName("Name");
        role.setPermissionEnums(new HashSet<>());
        role.setUpdatedAt(mock(Timestamp.class));
        role.setUpdatedBy(UUID.randomUUID());

        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setActive(true);
        user.setAvatarImg(attachment);
        user.setClinic(clinic);
        user.setCreatedAt(mock(Timestamp.class));
        user.setCreatedBy(UUID.randomUUID());
        user.setCredentialsNonExpired(true);
        user.setDeleted(true);
        user.setEnabled(true);
        user.setFirstname("Jane");
        user.setId(UUID.randomUUID());
        user.setJobTitle(jobTitle);
        user.setLastname("Doe");
        user.setPassword("iloveyou");
        user.setQualificationCategory(QualificationCategory.SPECIALIST);
        user.setResidentCardNumber(1L);
        user.setRole(role);
        user.setUpdatedAt(mock(Timestamp.class));
        user.setUpdatedBy(UUID.randomUUID());
        user.setWorkingExperienceInMonth(1);


        user1.setAccountNonExpired(true);
        user1.setAccountNonLocked(true);
        user1.setActive(true);
        user1.setAvatarImg(attachment);
        user1.setClinic(clinic);
        user1.setCreatedAt(mock(Timestamp.class));
        user1.setCreatedBy(UUID.randomUUID());
        user1.setCredentialsNonExpired(true);
        user1.setDeleted(true);
        user1.setEnabled(true);
        user1.setFirstname("Jane");
        user1.setId(UUID.randomUUID());
        user1.setJobTitle(jobTitle);
        user1.setLastname("Doe");
        user1.setPassword("iloveyou");
        user1.setQualificationCategory(QualificationCategory.SPECIALIST);
        user1.setResidentCardNumber(234L);
        user1.setRole(role);
        user1.setUpdatedAt(mock(Timestamp.class));
        user1.setUpdatedBy(UUID.randomUUID());
        user1.setWorkingExperienceInMonth(1);

        attestation.setClosed(false);
        attestation.setCreatedAt(mock(Timestamp.class));
        attestation.setCreatedBy(UUID.randomUUID());
        attestation.setDeleted(true);
        attestation.setDescription("The characteristics of someone or something");
        attestation.setEndDate(mock(java.sql.Date.class));
        attestation.setId(UUID.randomUUID());
        attestation.setStartDate(mock(java.sql.Date.class));
        attestation.setUpdatedAt(mock(Timestamp.class));
        attestation.setUpdatedBy(UUID.randomUUID());

        registration.setAttestation(attestation);
        registration.setResidentCardNumber(user.getResidentCardNumber());
        registration.setCreatedAt(mock(Timestamp.class));
        registration.setCreatedBy(UUID.randomUUID());
        registration.setUpdatedAt(mock(Timestamp.class));
        registration.setUpdatedBy(UUID.randomUUID());
    }

    @Test
    void testAdd() {
        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        when(this.registrationRepository.existsByResidentCardNumber((Long) any())).thenReturn(true);
        RegistrationReqDTO registrationReqDTO = new RegistrationReqDTO();
        assertThrows(RestException.class, () -> this.registrationServiceImpl.add(registrationReqDTO, user));
        verify(this.registrationRepository).existsByResidentCardNumber((Long) any());
    }

    @Test
    void testAdd1() {
        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        when(this.registrationRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);
        RegistrationReqDTO registrationReqDTO = new RegistrationReqDTO();
        assertThrows(RestException.class, () -> this.registrationServiceImpl.add(registrationReqDTO, user));
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
        verify(this.registrationRepository).existsByResidentCardNumber((Long) any());
    }

    @Test
    void testAdd2() {

        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(Optional.of(user));
        when(this.registrationRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);


        assertThrows(RestException.class, () -> this.registrationServiceImpl.add(new RegistrationReqDTO(1234L, UUID.randomUUID()), user));
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
        verify(this.registrationRepository).existsByResidentCardNumber((Long) any());
    }

    @Test
    void testAdd4() {
        user.setWorkingExperienceInMonth(48);
        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        when(this.registrationRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);
        when(this.attestationRepository.findById((UUID) any())).thenReturn(Optional.empty());

        assertThrows(RestException.class, () -> this.registrationServiceImpl.add(new RegistrationReqDTO(user.getResidentCardNumber(), UUID.randomUUID()), user));
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
        verify(this.registrationRepository).existsByResidentCardNumber((Long) any());
        verify(this.attestationRepository).findById((UUID) any());
    }

    @Test
    void testAdd5() {
        user.setWorkingExperienceInMonth(0);
        Optional<User> ofResult = Optional.of(user);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        when(this.registrationRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);
//        when(this.attestationRepository.findById((UUID) any())).thenReturn(Optional.empty());

        assertThrows(RestException.class, () -> this.registrationServiceImpl.add(new RegistrationReqDTO(user.getResidentCardNumber(), UUID.randomUUID()), user));
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
        verify(this.registrationRepository).existsByResidentCardNumber((Long) any());
//        verify(this.attestationRepository).findById((UUID) any());
    }

    @Test
    void testAdd6() {
        user.setWorkingExperienceInMonth(48);
        Optional<User> ofResult = Optional.of(user);
        when(this.registrationRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        when(this.attestationRepository.findById((UUID) any())).thenReturn(Optional.of(attestation));
        when(this.registrationRepository.save((Registration) any())).thenReturn(registration);

//        assertEquals(this.registrationRepository.save(registration),registration);
        ApiResult<RegistrationResDTO> resDTOApiResult = this.registrationServiceImpl.add(new RegistrationReqDTO(user.getResidentCardNumber(), attestation.getId()), user);
        System.out.println(resDTOApiResult + "=============================");
        assertEquals(resDTOApiResult.getData().getResidentCardNumber(), registration.getResidentCardNumber());

        verify(this.registrationRepository).existsByResidentCardNumber((Long) any());
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
        verify(this.attestationRepository).findById((UUID) any());
        verify(this.registrationRepository).save((Registration) any());
    }

    @Test
    void testAdd56() {
        user.setWorkingExperienceInMonth(48);
        Optional<User> ofResult = Optional.of(user);
        when(this.registrationRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(ofResult);
        assertThrows(RestException.class, () -> this.registrationServiceImpl.add(new RegistrationReqDTO(user.getResidentCardNumber(), attestation.getId()), user1));
        verify(this.registrationRepository).existsByResidentCardNumber((Long) any());
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
    }

 @Test
    void testAdd57() {
        user.setWorkingExperienceInMonth(48);
        Optional<User> ofResult = Optional.of(user);
        when(this.registrationRepository.existsByResidentCardNumber((Long) any())).thenReturn(false);
        when(this.userRepository.findByResidentCardNumberAndActiveTrue((Long) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.registrationServiceImpl.add(new RegistrationReqDTO(user.getResidentCardNumber(), attestation.getId()), user1));
        verify(this.registrationRepository).existsByResidentCardNumber((Long) any());
        verify(this.userRepository).findByResidentCardNumberAndActiveTrue((Long) any());
    }


    @Test
    void testGetAllRegisteredUserByAttestationId3() {
        ArrayList<User> userList = new ArrayList<>();
        userList.add(user);
        when(this.userRepository.findAllByResidentCardNumberInAndActiveTrue((java.util.Collection<Long>) any()))
                .thenReturn(userList);
        when(this.registrationRepository.findAllResidentCardNumber((UUID) any())).thenReturn(new ArrayList<>());
        UUID randomUUIDResult = UUID.randomUUID();
        ApiResult<RegisteredUsersResDTO> actualAllRegisteredUserByAttestationId = this.registrationServiceImpl
                .getAllRegisteredUserByAttestationId(randomUUIDResult);
        assertTrue(actualAllRegisteredUserByAttestationId.isSuccess());
        assertNull(actualAllRegisteredUserByAttestationId.getErrors());
        assertNull(actualAllRegisteredUserByAttestationId.getMessage());
        RegisteredUsersResDTO data = actualAllRegisteredUserByAttestationId.getData();
        assertSame(randomUUIDResult, data.getAttestationId());
        assertEquals(1, data.getUserList().size());
        verify(this.userRepository).findAllByResidentCardNumberInAndActiveTrue((java.util.Collection<Long>) any());
        verify(this.registrationRepository).findAllResidentCardNumber((UUID) any());
    }
}
