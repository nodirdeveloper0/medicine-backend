package com.app.medicinecomponent.service;

import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.entity.Attachment;
import com.app.medicinecomponent.entity.AttachmentContent;
import com.app.medicinecomponent.repository.AttachmentContentRepository;
import com.app.medicinecomponent.repository.AttachmentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;

    public AttachmentServiceImpl(AttachmentRepository attachmentRepository, AttachmentContentRepository attachmentContentRepository) {
        this.attachmentRepository = attachmentRepository;
        this.attachmentContentRepository = attachmentContentRepository;
    }

    @Override
    public ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());
        if (file!=null){
            Attachment attachment=new Attachment(
                    file.getOriginalFilename(),
                    file.getContentType(),
                    file.getSize()
            );
            attachmentRepository.save(attachment);
            AttachmentContent attachmentContent=new AttachmentContent(
                    file.getBytes(),
                    attachment
            );
            attachmentContentRepository.save(attachmentContent);
            return ApiResult.successResponse(attachment.getId());
        }
        return  ApiResult.successResponse(" Cannot uploaded the attachment");
    }

    @Override
    public ApiResult<Attachment> getById(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Attachment not found"));
        return ApiResult.successResponse(attachment);
    }

    @Override
    public ApiResult<?> getFile(UUID id, HttpServletResponse response) throws IOException {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Attachment not found"));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(attachment.getId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Attachment not found"));
        response.setHeader("Content-Disposition","attachment; filename=\""+attachment.getName()+"\"");
        response.setContentType(attachment.getContentType());
        FileCopyUtils.copy(attachmentContent.getBytes(),response.getOutputStream());
        return ApiResult.successResponse("Attachment successfully got");
    }

}
