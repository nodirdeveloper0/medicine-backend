package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.AboutUs;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.AboutUsReqDTO;
import com.app.medicinecomponent.payload.AboutUsResDTO;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.repository.AboutUsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {AboutUsServiceImpl.class})
@ExtendWith(SpringExtension.class)
class AboutUsServiceImplTest {
    @MockBean
    private AboutUsRepository aboutUsRepository;

    @Autowired
    private AboutUsServiceImpl aboutUsServiceImpl;

    AboutUs aboutUs = new AboutUs();

    @BeforeEach
    void setUp() {
        aboutUs.setCreatedAt(mock(Timestamp.class));
        aboutUs.setCreatedBy(UUID.randomUUID());
        aboutUs.setDeleted(true);
        aboutUs.setDescription("The characteristics of someone or something");
        aboutUs.setId(UUID.randomUUID());
        aboutUs.setUpdatedAt(mock(Timestamp.class));
        aboutUs.setUpdatedBy(UUID.randomUUID());
    }

    @Test
    void testAdd() {
        when(this.aboutUsRepository.save((AboutUs) any())).thenReturn(aboutUs);
        ApiResult<AboutUsResDTO> actualAddResult = this.aboutUsServiceImpl
                .add(new AboutUsReqDTO("The characteristics of someone or something"));
        assertTrue(actualAddResult.isSuccess());
        assertNull(actualAddResult.getErrors());
        assertNull(actualAddResult.getMessage());
        AboutUsResDTO data = actualAddResult.getData();
        assertEquals("The characteristics of someone or something", data.getDescription());
        assertNull(data.getId());
        verify(this.aboutUsRepository).save((AboutUs) any());
    }

    @Test
    void testGetOne() {
        UUID randomUUIDResult = UUID.randomUUID();
        aboutUs.setId(randomUUIDResult);
        Optional<AboutUs> ofResult = Optional.of(aboutUs);
        when(this.aboutUsRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<AboutUsResDTO> actualOne = this.aboutUsServiceImpl.getOne(UUID.randomUUID());
        assertTrue(actualOne.isSuccess());
        assertNull(actualOne.getErrors());
        assertNull(actualOne.getMessage());
        AboutUsResDTO data = actualOne.getData();
        assertEquals("The characteristics of someone or something", data.getDescription());
        assertSame(randomUUIDResult, data.getId());
        verify(this.aboutUsRepository).findById((UUID) any());
    }

    @Test
    void testGetOne2() {
        when(this.aboutUsRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.aboutUsServiceImpl.getOne(UUID.randomUUID()));
        verify(this.aboutUsRepository).findById((UUID) any());
    }

    @Test
    void testGetAll2() {
        UUID randomUUIDResult = UUID.randomUUID();
        aboutUs.setId(randomUUIDResult);
        ArrayList<AboutUs> aboutUsList = new ArrayList<>();
        aboutUsList.add(aboutUs);
        when(this.aboutUsRepository.findAll()).thenReturn(aboutUsList);
        ApiResult<List<AboutUsResDTO>> actualAll = this.aboutUsServiceImpl.getAll();
        List<AboutUsResDTO> data = actualAll.getData();
        assertEquals(1, data.size());
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getErrors());
        assertNull(actualAll.getMessage());
        AboutUsResDTO getResult = data.get(0);
        assertEquals("The characteristics of someone or something", getResult.getDescription());
        assertSame(randomUUIDResult, getResult.getId());
        verify(this.aboutUsRepository).findAll();
    }

    @Test
    void testEdit() {
        UUID randomUUIDResult = UUID.randomUUID();
        aboutUs.setId(randomUUIDResult);
        Optional<AboutUs> ofResult = Optional.of(aboutUs);
        when(this.aboutUsRepository.save((AboutUs) any())).thenReturn(aboutUs);
        when(this.aboutUsRepository.findById((UUID) any())).thenReturn(ofResult);
        AboutUsReqDTO aboutUsReqDTO = new AboutUsReqDTO("The characteristics of someone or something");
        ApiResult<AboutUsResDTO> actualEditResult = this.aboutUsServiceImpl.edit(aboutUsReqDTO, UUID.randomUUID());
        assertTrue(actualEditResult.isSuccess());
        assertNull(actualEditResult.getErrors());
        assertNull(actualEditResult.getMessage());
        AboutUsResDTO data = actualEditResult.getData();
        assertEquals("The characteristics of someone or something", data.getDescription());
        assertSame(randomUUIDResult, data.getId());
        verify(this.aboutUsRepository).save((AboutUs) any());
        verify(this.aboutUsRepository).findById((UUID) any());
    }

    @Test
    void testEdit3() {
        Optional<AboutUs> ofResult = Optional.of(aboutUs);
        when(this.aboutUsRepository.save((AboutUs) any()))
                .thenThrow(new RestException(HttpStatus.CONTINUE, "An error occurred"));
        when(this.aboutUsRepository.findById((UUID) any())).thenReturn(ofResult);
        AboutUsReqDTO aboutUsReqDTO = new AboutUsReqDTO("The characteristics of someone or something");
        assertThrows(RestException.class, () -> this.aboutUsServiceImpl.edit(aboutUsReqDTO, UUID.randomUUID()));
        verify(this.aboutUsRepository).save((AboutUs) any());
        verify(this.aboutUsRepository).findById((UUID) any());
    }

    @Test
    void testDelete() {
        doNothing().when(this.aboutUsRepository).deleteById((UUID) any());
        ApiResult<?> actualDeleteResult = this.aboutUsServiceImpl.delete(UUID.randomUUID());
        assertNull(actualDeleteResult.getData());
        assertTrue(actualDeleteResult.isSuccess());
        assertEquals("About us is successfully deleted", actualDeleteResult.getMessage());
        assertNull(actualDeleteResult.getErrors());
        verify(this.aboutUsRepository).deleteById((UUID) any());
    }

    @Test
    void testDelete2() {
        doThrow(new RestException(HttpStatus.CONTINUE, "An error occurred")).when(this.aboutUsRepository)
                .deleteById((UUID) any());
        assertThrows(RestException.class, () -> this.aboutUsServiceImpl.delete(UUID.randomUUID()));
        verify(this.aboutUsRepository).deleteById((UUID) any());
    }
}

