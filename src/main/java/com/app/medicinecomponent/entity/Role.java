package com.app.medicinecomponent.entity;

import com.app.medicinecomponent.entity.enums.PermissionEnum;
import com.app.medicinecomponent.entity.template.AbcEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SQLDelete(sql = "update role set deleted=true where id=?")
@Where(clause = "deleted=false")
public class Role extends AbcEntity {

    @Column(nullable = false,unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    private Set<PermissionEnum>  permissionEnums;
}
