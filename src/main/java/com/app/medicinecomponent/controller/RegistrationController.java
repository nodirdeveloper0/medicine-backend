package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RegisteredUsersResDTO;
import com.app.medicinecomponent.payload.RegistrationReqDTO;
import com.app.medicinecomponent.payload.RegistrationResDTO;
import com.app.medicinecomponent.security.CurrentUser;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(RestConstant.REGISTRATION_CONTROLLER)
@Tag(name = "Registration operations", description = "Registration")
public interface RegistrationController {


    /**
     * Main purpose of this controller is registering users to the attestation which is already announced before.
     *
     * @param registrationReqDTO { residentCardNumber:Long,attestationId:UUID }
     * @return RegistrationResDto
     */
    @PreAuthorize(value = "hasAuthority('REQUEST_REGISTRATION')")
    @Operation(summary = "Add registration (only doctor can request this path)")
    @PostMapping
    ApiResult<RegistrationResDTO> add(@RequestBody @Valid RegistrationReqDTO registrationReqDTO,
                                      @CurrentUser User user );


    /**
     * Main purpose of this controller is getting all registered users by active attestation_id(last one) for giving certificate
     *
     * @param attestationId{ attestationId:UUID}
     * @return RegistredUsersResDTO
     */

    @Operation(summary = "Get all registered users (any user)")
    @GetMapping("/attestation-id/{attestationId}")
    ApiResult<RegisteredUsersResDTO> getAllRegisteredUserByAttestationId(@PathVariable UUID attestationId);
}
