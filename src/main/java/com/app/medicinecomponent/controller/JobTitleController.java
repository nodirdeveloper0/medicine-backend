package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.JobTitleReqDTO;
import com.app.medicinecomponent.payload.JobTitleResDTO;
import com.app.medicinecomponent.utils.RestConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequestMapping(RestConstant.JOB_TITLE_CONTROLLER)
@Tag(name = "Job title operations", description = "Job title")
public interface JobTitleController {

    @PreAuthorize(value = "hasAuthority('ADD_JOB_TITLE')")
    @Operation(summary = "Add job title (admin) ")
    @PostMapping
    ApiResult<JobTitleResDTO> add(@RequestBody @Valid JobTitleReqDTO jobTitleReqDTO);

    @Operation(summary = "Get one job title (any user)")
    @GetMapping("/{id}")
    ApiResult<JobTitleResDTO> getOne(@PathVariable UUID id);

    @Operation(summary = "Get all job title (any user)")
    @GetMapping
    ApiResult<List<JobTitleResDTO>> getAll();

    @PreAuthorize(value = "hasAuthority('EDIT_JOB_TITLE')")
    @Operation(summary = "Edit job title  (any user)")
    @PutMapping("/{id}")
    ApiResult<JobTitleResDTO> edit(@RequestBody @Valid JobTitleReqDTO jobTitleReqDTO,
                                   @PathVariable UUID id);

}
