package com.app.medicinecomponent.repository;

import com.app.medicinecomponent.entity.Address;
import com.app.medicinecomponent.entity.Clinic;
import com.app.medicinecomponent.entity.DiseaseInfo;
import com.app.medicinecomponent.entity.enums.DiseaseInfoStatusEnums;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.mock;

@ContextConfiguration(classes = {DiseaseInfoRepository.class, AddressRepository.class, ClinicRepository.class})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.epam.medicinecomponent.entity"})
@DataJpaTest
class DiseaseInfoRepositoryTest {
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private ClinicRepository clinicRepository;
    @Autowired
    private DiseaseInfoRepository diseaseInfoRepository;

    DiseaseInfo diseaseInfo = new DiseaseInfo();

    @BeforeEach
    void setUp() {
        Address address = new Address();
        address.setDeleted(false);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setStreet("Street");
        addressRepository.save(address);

        Clinic clinic = new Clinic();
        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setDeleted(false);
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinicRepository.save(clinic);

        diseaseInfo.setCardNumber(1L);
        diseaseInfo.setClinic(clinic);
        diseaseInfo.setDeleted(false);
        diseaseInfo.setDescription("The characteristics of someone or something");
        diseaseInfo.setFirstName("Jane");
        diseaseInfo.setLastName("Doe");
        diseaseInfo.setId(UUID.randomUUID());
        diseaseInfo.setReceivedTime(mock(Timestamp.class));
        diseaseInfo.setSentTime(mock(Timestamp.class));
        diseaseInfo.setStatus(DiseaseInfoStatusEnums.NEW);
        diseaseInfoRepository.save(diseaseInfo);
    }

    @AfterEach
    void tearDown() {
        addressRepository.deleteAll();
        clinicRepository.deleteAll();
        diseaseInfoRepository.deleteAll();
    }

    @Test
    void testFindAllByStatusNew() {
        List<DiseaseInfo> allByStatusNew = this.diseaseInfoRepository.findAllByStatusNew();
        Assertions.assertNotNull(allByStatusNew.get(0));
        Assertions.assertEquals(allByStatusNew.get(0).getStatus(), DiseaseInfoStatusEnums.NEW);
    }
}

