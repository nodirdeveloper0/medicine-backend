package com.app.medicinecomponent.controller;

import com.app.medicinecomponent.entity.User;
import com.app.medicinecomponent.payload.ApiResult;
import com.app.medicinecomponent.payload.RegisteredUsersResDTO;
import com.app.medicinecomponent.payload.RegistrationReqDTO;
import com.app.medicinecomponent.payload.RegistrationResDTO;
import com.app.medicinecomponent.service.RegistrationService;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class RegistrationControllerImpl implements RegistrationController{
    private final RegistrationService registrationService;

    public RegistrationControllerImpl(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @Override
    public ApiResult<RegistrationResDTO> add(RegistrationReqDTO registrationReqDTO, User user) {
        return registrationService.add(registrationReqDTO,user );
    }

    @Override
    public ApiResult<RegisteredUsersResDTO> getAllRegisteredUserByAttestationId(UUID attestationId) {
        return registrationService.getAllRegisteredUserByAttestationId(attestationId);
    }
}
