package com.app.medicinecomponent.config;

import com.app.medicinecomponent.security.JwtFilter;
import com.app.medicinecomponent.service.AuthServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)

public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilter jwtFilter;
    private final AuthServiceImpl authServiceImpl;

    public SecurityConfig(@Lazy JwtFilter jwtFilter, @Lazy AuthServiceImpl authServiceImpl) {
        this.jwtFilter = jwtFilter;
        this.authServiceImpl = authServiceImpl;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/api/auth/**",
                        "/api/v1/docs/**",
                        "/api/v1/swagger-ui/**",
                        "/api/v1/swagger-ui.html",
                        "/api/about-us",
                        "/api/about-us/*",
                        "/api/attestation",
                        "/api/attestation/*",
                        "/api/attestation/closed-false",
                        "/api/certificate/",
                        "/api/certificate/certificates-of-one/**",
                        "/api/clinic/*",
                        "/api/clinic",
                        "/api/clinic/legal-card-number/*",
//                        "/api/clinic/legal-card-number/",
                        "/api/clinic/all-clinics",
                        "/api/disease-info",
                        "/api/job-title",
                        "/api/job-title/*",
                        "/api/registration/attestation-id/**",
                        "/api/role",
                        "/api/role/*",
                        "/api/user",
                        "/api/user/*",
                        "/api/user/resident-card-number/",
                        "/api/user/clinic-id/**",
                        "/api/user/all-users-by-clinic-id/**",
                        "/api/user/activate/",
                        "/api/user/deactivate/",
                        "/api/attachment/upload",
                        "/api/attachment/info/*",
                        "/api/attachment/download/*",
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js",
                        "/*",
                        "/**"
                        )
                .permitAll()
                .anyRequest()
                .authenticated();
        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authServiceImpl).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
