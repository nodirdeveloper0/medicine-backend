package com.app.medicinecomponent.service;

import com.app.medicinecomponent.payload.accountResponse.CertificateReq;
import com.app.medicinecomponent.payload.accountResponse.CertificateResponseFromAccountService;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeReq;
import com.app.medicinecomponent.payload.accountResponse.CertificateTypeResponseFromAccountService;
import com.app.medicinecomponent.payload.clinicResponse.ClinicInfoFromCityManagement;
import com.app.medicinecomponent.payload.residentResponse.ResidentInfoFromCityManagement;

public interface ExternalAPIsService {

    ResidentInfoFromCityManagement getResidentInfoFromCityManagement(Long residentCardNumber);

    ClinicInfoFromCityManagement getClinicInfoFromCityManagement(Long cardNumber);

    CertificateTypeResponseFromAccountService saveCertificateType(CertificateTypeReq certificateTypeReq);

    CertificateResponseFromAccountService saveCertificate(CertificateReq certificateReq);

    //    ResidentCertificateFromCitizenAccount checkResidentCertificateFromCitizenAccount(Long cardNumber);
}
