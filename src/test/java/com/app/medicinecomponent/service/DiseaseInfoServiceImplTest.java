package com.app.medicinecomponent.service;

import com.app.medicinecomponent.entity.Address;
import com.app.medicinecomponent.entity.Clinic;
import com.app.medicinecomponent.entity.DiseaseInfo;
import com.app.medicinecomponent.entity.enums.DiseaseInfoStatusEnums;
import com.app.medicinecomponent.exception.RestException;
import com.app.medicinecomponent.payload.*;
import com.app.medicinecomponent.repository.ClinicRepository;
import com.app.medicinecomponent.repository.DiseaseInfoRepository;
import com.app.medicinecomponent.payload.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {DiseaseInfoServiceImpl.class})
@ExtendWith(SpringExtension.class)
class DiseaseInfoServiceImplTest {
    @MockBean
    private ClinicRepository clinicRepository;

    @MockBean
    private DiseaseInfoRepository diseaseInfoRepository;

    @Autowired
    private DiseaseInfoServiceImpl diseaseInfoServiceImpl;

    Address address = new Address();
    Clinic clinic = new Clinic();
    DiseaseInfo diseaseInfo = new DiseaseInfo();
    
    @BeforeEach
    void setUp() {

        address.setCreatedAt(mock(Timestamp.class));
        address.setCreatedBy(UUID.randomUUID());
        address.setDeleted(true);
        address.setDistrict("District");
        address.setHomeNumber("42");
        address.setId(UUID.randomUUID());
        address.setStreet("Street");
        address.setUpdatedAt(mock(Timestamp.class));
        address.setUpdatedBy(UUID.randomUUID());

        clinic.setActive(true);
        clinic.setAddress(address);
        clinic.setClinicLegalCardNumber(1L);
        clinic.setCreatedAt(mock(Timestamp.class));
        clinic.setCreatedBy(UUID.randomUUID());
        clinic.setDeleted(true);
        clinic.setId(UUID.randomUUID());
        clinic.setName("Name");
        clinic.setPhoneNumber("4105551212");
        clinic.setUpdatedAt(mock(Timestamp.class));
        clinic.setUpdatedBy(UUID.randomUUID());

        diseaseInfo.setCardNumber(1L);
        diseaseInfo.setClinic(clinic);
        diseaseInfo.setCreatedAt(mock(Timestamp.class));
        diseaseInfo.setCreatedBy(UUID.randomUUID());
        diseaseInfo.setDeleted(true);
        diseaseInfo.setDescription("The characteristics of someone or something");
        diseaseInfo.setFirstName("Jane");
        diseaseInfo.setId(UUID.randomUUID());
        diseaseInfo.setLastName("Doe");
        diseaseInfo.setReceivedTime(mock(Timestamp.class));
        diseaseInfo.setSentTime(mock(Timestamp.class));
        diseaseInfo.setStatus(DiseaseInfoStatusEnums.NEW);
        diseaseInfo.setUpdatedAt(mock(Timestamp.class));
        diseaseInfo.setUpdatedBy(UUID.randomUUID());
    }

    @Test
    void testAdd4() {
        when(this.clinicRepository.findAllIdsAsString())
                .thenThrow(new RestException(HttpStatus.CONTINUE, "An error occurred"));
        assertThrows(RestException.class, () -> this.diseaseInfoServiceImpl.add(new DiseaseInfoReqDTO()));
        verify(this.clinicRepository).findAllIdsAsString();
    }

    @Test
    void testGetOne() {
        UUID randomUUIDResult = UUID.randomUUID();
        address.setId(randomUUIDResult);
        UUID randomUUIDResult1 = UUID.randomUUID();
        clinic.setId(randomUUIDResult1);
        UUID randomUUIDResult2 = UUID.randomUUID();
        diseaseInfo.setId(randomUUIDResult2);
        Optional<DiseaseInfo> ofResult = Optional.of(diseaseInfo);
        when(this.diseaseInfoRepository.findById((UUID) any())).thenReturn(ofResult);
        ApiResult<DiseaseInfoResDto> actualOne = this.diseaseInfoServiceImpl.getOne(UUID.randomUUID());
        assertTrue(actualOne.isSuccess());
        assertNull(actualOne.getErrors());
        assertNull(actualOne.getMessage());
        DiseaseInfoResDto data = actualOne.getData();
        assertEquals("Doe", data.getLastName());
        assertSame(randomUUIDResult2, data.getId());
        assertEquals("Jane", data.getFirstName());
        Assertions.assertEquals(DiseaseInfoStatusEnums.NEW, data.getStatus());
        assertEquals(1L, data.getCardNumber().longValue());
        assertEquals("The characteristics of someone or something", data.getDescription());
        ClinicResDTO clinicResDTO = data.getClinicResDTO();
        assertEquals("Name", clinicResDTO.getName());
        assertSame(randomUUIDResult1, clinicResDTO.getId());
        assertEquals(1L, clinicResDTO.getClinicLegalCardNumber().longValue());
        assertEquals("4105551212", clinicResDTO.getPhoneNumber());
        assertTrue(clinicResDTO.getActive());
        AddressResDTO address1 = clinicResDTO.getAddress();
        assertEquals("District", address1.getDistrict());
        assertSame(randomUUIDResult, address1.getId());
        assertEquals("Street", address1.getStreet());
        assertEquals("42", address1.getHomeNumber());
        verify(this.diseaseInfoRepository).findById((UUID) any());
    }

    @Test
    void testGetOne01(){
        when(this.diseaseInfoRepository.findById((UUID) any())).thenReturn(Optional.empty());
        assertThrows(RestException.class, () -> this.diseaseInfoServiceImpl.getOne(UUID.randomUUID()));
        verify(this.diseaseInfoRepository).findById((UUID) any());
    }

    @Test
    void testGetAll2() {
        ArrayList<DiseaseInfo> diseaseInfoList = new ArrayList<>();
        diseaseInfoList.add(diseaseInfo);
        when(this.diseaseInfoRepository.findAll()).thenReturn(diseaseInfoList);
        ApiResult<List<DiseaseInfoResDto>> actualAll = this.diseaseInfoServiceImpl.getAll();
        assertEquals(1, actualAll.getData().size());
        assertTrue(actualAll.isSuccess());
        assertNull(actualAll.getMessage());
        assertNull(actualAll.getErrors());
        verify(this.diseaseInfoRepository).findAll();
    }
}