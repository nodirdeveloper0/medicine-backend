package com.app.medicinecomponent.payload;

import com.app.medicinecomponent.entity.enums.DiseaseInfoStatusEnums;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiseaseInfoResDto {
    private UUID id;

    private String firstName;

    private String lastName;

    private Long cardNumber;

    private String description;

    private DiseaseInfoStatusEnums status;

    private ClinicResDTO clinicResDTO;

    private Timestamp receivedTime;

    private Timestamp sentTime;
}
