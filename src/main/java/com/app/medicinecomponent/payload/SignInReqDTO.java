package com.app.medicinecomponent.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignInReqDTO {

    @NotNull(message = "Resident card number shouldn't be empty!")
    private String residentCardNumber;

    @NotBlank(message = "Password shouldn't be empty!")
    private String password;
}
